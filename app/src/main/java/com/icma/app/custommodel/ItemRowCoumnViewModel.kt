package com.icma.app.custommodel

data class ItemRowCoumnViewModel( var column: MutableList<Column>) {
}
data class Column (
    var text: String,
    var row: MutableList<RowD>


){}

data class RowD (
    var Image: Int,
    var subText:String,
    var time:String
) {}
