package com.icma.app.adapters
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.DownloadUtil
import com.icma.app.activities.VideoActivity
import com.icma.app.activities.YoutubePlayerActivity
import com.icma.app.model.VideoItem
import java.util.regex.Pattern

class Home4Adapter(
    val context: Context?,
    private val mList: List<VideoItem?>?,
    val mShimmerViewContainer: ShimmerFrameLayout,
    val mainlly: LinearLayout,

    ) : RecyclerView.Adapter<Home4Adapter.ViewHolder>() {

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_video, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mShimmerViewContainer.visibility=View.GONE
        mainlly.visibility=View.VISIBLE
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_audio_thumb)
            .error(R.drawable.ic_audio_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
        context?.let { Glide.with(it).load(mList!!.get(position)!!.videoThumbnail).apply(options).into(holder.vidThumbnail) }
        holder.txPlayTime.visibility=View.GONE
        holder.txHeading.text = mList!!.get(position)!!.title
        var vidLink:String= mList!!.get(position)!!.hymnsVideo!!
        holder.itemView.setOnClickListener {
            if (!vidLink.equals(""))
            {
                val newVideoID = extractYTId(vidLink)
                Constants.VIDEOID= newVideoID.toString()
                Constants.VIDEONAME= mList!!.get(position)!!.title!!
                val intent = Intent(context, YoutubePlayerActivity::class.java)
                context!!.startActivity(intent)
            }
            else{
                Constants.showToast(context as Activity?,context!!.getString(R.string.check_in_sometime))
            }
        }
       // perform(position)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val vidThumbnail: ImageView = itemView.findViewById(R.id.vidThumbnail)
        val txPlayTime: TextView = itemView.findViewById(R.id.txPlayTime)
        val txHeading: TextView = itemView.findViewById(R.id.txVideoHeading)
    }


    fun extractYTId(ytUrl: String?): String? {
        var vId: String? = null
        val pattern = Pattern.compile(
            "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
            Pattern.CASE_INSENSITIVE
        )
        val matcher = pattern.matcher(ytUrl)
        if (matcher.matches()) {
            vId = matcher.group(1)
        }
        return vId
    }
}
