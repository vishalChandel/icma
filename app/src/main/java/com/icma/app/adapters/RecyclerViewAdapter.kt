//package com.icma.app.adapters
//
//import android.content.Context
//import android.content.Intent
//import android.util.Log
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.Filter
//import android.widget.Filter.FilterResults
//import android.widget.Filterable
//import android.widget.TextView
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import com.facebook.shimmer.ShimmerFrameLayout
//import java.util.ArrayList
//
//class RecyclerViewAdapter(
//    private val mContext: Context,
//    mArrayList: ArrayList<SearchModelget.Datan>,
//    text_no_search: TextView,
//    mShimmerViewContainer: ShimmerFrameLayout
//) :
//    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(), Filterable {
//    var video_tag_title: String? = null
//    var mArrayList: ArrayList<SearchModelget.Datan>? = ArrayList<SearchModelget.Datan>()
//    var tx_no_search: TextView
//    var mShimmerViewContainer: ShimmerFrameLayout
//    var FilteredmArrayList: ArrayList<SearchModelget.Datan> = ArrayList<SearchModelget.Datan>()
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val view: View =
//            LayoutInflater.from(parent.context).inflate(R.layout.search_item, parent, false)
//        return ViewHolder(view)
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        //  holder.tag.setImageResource(mImageUrls.get(position).getImgId());
//        val mModel: SearchModelget.Datan = mArrayList!![position]
//        var message: String = mModel.getType()
//        message = Character.toUpperCase(message[0]).toString() + message.substring(1)
//        holder.tx_tagType.text = message
//        val tagString: String = mModel.getTagTitle()
//        val result = tagString.replace("[-+.^:,#$]".toRegex(), "")
//        if (result != null) {
//            try {
//                var message2 = result
//                message2 = Character.toUpperCase(message2[0]).toString() + message2.substring(1)
//                holder.tx_tagtitle.text = message2
//            } catch (e: StringIndexOutOfBoundsException) {
//                e.printStackTrace()
//            }
//        }
//        holder.tx_tagtotalVideos.setText(mModel.getTotalVideosViews())
//        fixed_tag_title = mModel.getTagTitle()
//        Log.d("**RESPONSE", "" + mModel)
//        //  Toast.makeText(mContext, ""+mModel.getTagVideos(), Toast.LENGTH_SHORT).show();
//        val layoutManager = LinearLayoutManager(mContext)
//        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
//        holder.recyclerViewHorizontal.layoutManager = layoutManager
//        video_tag_title = mModel.getTagTitle()
//        val horizontalAdapter = RecyclerViewAdapter2(
//            mModel.getTagVideos() as ArrayList<SearchModelget.TagVideos?>,
//            mContext,
//            video_tag_title,
//            holder.tx_tagtotalVideos,
//            mModel.getTotalVideosViews(),
//            position
//        )
//        holder.recyclerViewHorizontal.adapter = horizontalAdapter
//        mShimmerViewContainer.visibility = View.GONE
//        holder.itemView.setOnClickListener {
//            val intent = Intent(mContext, HashtagItemView::class.java)
//            intent.putExtra("tagName", mModel.getTagTitle())
//            intent.putExtra("tagViews", mModel.getTotalVideosViews())
//            intent.putExtra("musicId", "")
//            intent.putExtra("tagD", mModel.getTagTitle())
//            intent.putExtra("tagDescription", mModel.getTagDesc())
//            intent.putExtra("isLiked", "2")
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//            mContext.startActivity(intent)
//        }
//    }
//
//    override fun getItemCount(): Int {
//        return if (mArrayList == null) 0 else mArrayList!!.size
//    }
//
//    //filter method
//    override fun getFilter(): Filter {
//        return object : Filter() {
//            override fun performFiltering(charSequence: CharSequence): FilterResults {
//                val charString = charSequence.toString()
//                mArrayList = if (charString.isEmpty()) {
//                    FilteredmArrayList
//                } else {
//                    val filteredList: ArrayList<SearchModelget.Datan> =
//                        ArrayList<SearchModelget.Datan>()
//                    for (row in FilteredmArrayList) {
//
//
//                        //change this to filter according to your case
//                        if (row.getTagTitle().toLowerCase().contains(charString.toLowerCase())) {
//                            filteredList.add(row)
//                        }
//                    }
//                    filteredList
//                }
//                val filterResults = FilterResults()
//                filterResults.values = mArrayList
//                return filterResults
//            }
//
//            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
//                mArrayList = filterResults.values as ArrayList<*>
//                if (mArrayList!!.size > 0) {
//                    tx_no_search.visibility = View.GONE
//                } else {
//                    tx_no_search.visibility = View.VISIBLE
//                    tx_no_search.text = "No Results found for '$charSequence'"
//                }
//                notifyDataSetChanged()
//            }
//        }
//    }
//
//    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        var tx_tagtitle: TextView
//        var tx_tagType: TextView
//        var tx_tagtotalVideos: TextView
//        var recyclerViewHorizontal: RecyclerView
//
//        init {
//            tx_tagtitle = itemView.findViewById(R.id.search_tagTitle)
//            tx_tagtotalVideos = itemView.findViewById(R.id.search_tagTotalVideos_)
//            tx_tagType = itemView.findViewById(R.id.search_tagType)
//            recyclerViewHorizontal = itemView.findViewById(R.id.search_item1_recycler)
//        }
//    }
//
//    companion object {
//        var fixed_tag_title = ""
//    }
//
//    init {
//        this.mArrayList = mArrayList
//        FilteredmArrayList = mArrayList
//        tx_no_search = text_no_search
//        this.mShimmerViewContainer = mShimmerViewContainer
//    }
//}
