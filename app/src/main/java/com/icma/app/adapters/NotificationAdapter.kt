package com.icma.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.icma.app.R
import com.icma.app.model.DataS
import java.text.SimpleDateFormat
import java.util.*

class NotificationAdapter(val dataList0: MutableList<DataS>) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

   var formattedDate:String=""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_notification, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: NotificationAdapter.ViewHolder, position: Int) {
        holder.txNotifHeadingTV.text =dataList0.get(position).title
        holder.txNotifTimeTV.text =dataList0.get(position).creationAt
        val coment_time: Long = dataList0.get(position).creationAt.toLong()
        timeChange(coment_time)
        holder.txNotifTimeTV.text=formattedDate
    }

    override fun getItemCount(): Int {
       return dataList0.size
    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txNotifHeadingTV: TextView = itemView.findViewById(R.id.txNotifHeadingTV)
        val txNotifTimeTV: TextView = itemView.findViewById(R.id.txNotifTimeTV)
        val imNotification:ImageView=itemView.findViewById(R.id.imNotification)
    }


    fun timeChange(coment_time: Long) {
        val date = Date(coment_time * 1000L)
        val sdf = SimpleDateFormat("MMM dd hh:mma")
        sdf.timeZone = TimeZone.getDefault()
        formattedDate = sdf.format(date)
    }
}
