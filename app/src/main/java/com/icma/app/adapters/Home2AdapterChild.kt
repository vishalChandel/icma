package com.icma.app.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.icma.app.R

import android.util.Log
import android.widget.ImageView

import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.icma.app.activities.AudioActivityNew
import com.icma.app.model.DataItem
import java.io.Serializable
import android.text.Spannable

import android.text.style.ImageSpan


import android.graphics.drawable.Drawable

import android.text.SpannableString
import android.widget.TextView.BufferType

import android.graphics.BitmapFactory

import android.graphics.Bitmap

import android.text.SpannableStringBuilder
import android.widget.LinearLayout


class Home2AdapterChild(var context: Context?, val data_list: MutableList<DataItem>?) : RecyclerView.Adapter<Home2AdapterChild.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Home2AdapterChild.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(com.icma.app.R.layout.item_audio, parent, false)
        return Home2AdapterChild.ViewHolder(v)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: Home2AdapterChild.ViewHolder, position: Int) {
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_audio_thumb)
            .error(R.drawable.ic_audio_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
        context?.let { Glide.with(it).load(data_list!!.get(position)!!.audioThumbnail).apply(options).into(holder.audioImage) }

        if (data_list!!.get(position)!!.title.trim().length>20) {
            holder.txAudioHeading.visibility=View.VISIBLE
            holder.llyAudioheadingSingle.visibility=View.GONE
            val ss = SpannableString("ggg " + data_list!!.get(position)!!.title)
            val d: Drawable = context!!.getDrawable(R.drawable.ic_musiic)!!
            d.setBounds(0, 0, d.intrinsicWidth - 20, d.intrinsicHeight - 20)
            val span = ImageSpan(d, ImageSpan.ALIGN_BASELINE)
            ss.setSpan(span, 0, 3, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
            holder.txAudioHeading.setText(ss)
        }
        else
        {
            holder.txAudioHeading.visibility=View.GONE
            holder.llyAudioheadingSingle.visibility=View.VISIBLE
            holder.txAudioHeadingS.setText(data_list!!.get(position)!!.title)
        }

//        holder.txAudoTiming.text=context!!.getString(R.string.new_aud_time)
        holder.txAudoTiming.text=data_list!!.get(position)!!.end_time
        holder.itemView.setOnClickListener {
            val audio_list = arrayListOf<MutableList<DataItem>>()
            audio_list.add(data_list!!)
            val intent = Intent(context, AudioActivityNew::class.java)
            intent.putExtra("audioList", audio_list as Serializable?)
          //  intent.putExtra("videoURL",data_list.get(position).audio)
            intent.putExtra("audioUrl",data_list.get(position).audio)
            intent.putExtra("position", position)
            intent.putExtra("audioThumbnail",data_list.get(position).audioThumbnail)
            intent.putExtra("title",data_list.get(position).title)
            context!!.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
    return  data_list!!.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txAudioHeading: TextView = itemView.findViewById(R.id.txAudioHeading)
        val txAudoTiming: TextView = itemView.findViewById(R.id.txAudoTiming)
        val audioImage:ImageView=itemView.findViewById(R.id.audioImage)
        val llyAudioheadingSingle: LinearLayout=itemView.findViewById(R.id.llyAudioheadingSingle)
        val txAudioHeadingS: TextView = itemView.findViewById(R.id.txAudioHeadingS)
    }

}
