package com.icma.app.adapters
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.DownloadUtil
import com.icma.app.activities.VideoActivity
import com.icma.app.model.DataItem
import com.icma.app.model.VideoHome

class Home1Adapter(
    val context: Context?,
    private val mList: List<VideoHome>,
    val mShimmerViewContainer: ShimmerFrameLayout,
    val mainlly: LinearLayout,

    ) : RecyclerView.Adapter<Home1Adapter.ViewHolder>() {

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_video, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mShimmerViewContainer.visibility=View.GONE
        mainlly.visibility=View.VISIBLE
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_audio_thumb)
            .error(R.drawable.ic_audio_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
        context?.let { Glide.with(it).load(mList.get(position).videoThumbnail).apply(options).into(holder.vidThumbnail) }
        holder.txPlayTime.text= mList.get(position)!!.endTime
        holder.txHeading.text = mList!!.get(position)!!.title
        var vidLink:String=mList!!.get(position)!!.video
        holder.itemView.setOnClickListener {
            if (!vidLink.equals(""))
            {
                val intent = Intent(context, VideoActivity::class.java)
                intent.putExtra("videoURL", vidLink)
                context!!.startActivity(intent)
            }
            else{
                Constants.showToast(context as Activity?,context!!.getString(R.string.check_in_sometime))
            }
        }
       // perform(position)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val vidThumbnail: ImageView = itemView.findViewById(R.id.vidThumbnail)
        val txPlayTime: TextView = itemView.findViewById(R.id.txPlayTime)
        val txHeading: TextView = itemView.findViewById(R.id.txVideoHeading)
    }



    //TERRISTAL CHECK
    private fun perform(position: Int) {
        Log.e("Vishal","1")
        for (i in 0..mList.size-1) {
            val player: SimpleExoPlayer
            @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode =
                DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
            val loadControl = DefaultLoadControl.Builder()
                .setBufferDurationsMs(25000, 50000, 1500, 100).createDefaultLoadControl()
            val renderersFactory =
                context?.let { DefaultRenderersFactory(it).setExtensionRendererMode(extensionRendererMode) }
            player =
                context?.let { renderersFactory?.let { it1 -> SimpleExoPlayer.Builder(it, it1).setLoadControl(loadControl).build() } }!!
            val dataSourceFactory = DefaultDataSourceFactory(
                context,
                Util.getUserAgent(context!!, "ICMA")
            )
            val cacheDataSourceFactory =
                CacheDataSourceFactory(DownloadUtil.getCache(context), dataSourceFactory)
            val videoSource: MediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory)
                .createMediaSource(Uri.parse(mList.get(position).video))

            player.prepare(videoSource,true,true)
            player.addListener(object : Player.EventListener {
                override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                    when (playbackState) {
                        Player.STATE_READY -> player.release()
                        else -> {
                        }
                    }
                }
            })
        }
    }
}
