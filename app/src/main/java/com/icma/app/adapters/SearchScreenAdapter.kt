package com.icma.app.adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.icma.app.R
import com.icma.app.app.MyApplication
import com.icma.app.utils.Constants.Companion.AUDIO_VIEW_HOLDER
import com.icma.app.utils.Constants.Companion.VIDEO_VIEW_HOLDER
import com.icma.app.model.DataS
import com.icma.app.viewholder.SearchAudioViewHiolder
import com.icma.app.viewholder.SearchVideoViewHolder

class SearchScreenAdapter(
    val context: Activity?, dataList: MutableList<DataS>, val mShimmerViewContainer: ShimmerFrameLayout, val txNoData:TextView
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    var dataList = mutableListOf<DataS>()
    var filtered_dataList = mutableListOf<DataS>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == AUDIO_VIEW_HOLDER) {
            var mView: View = LayoutInflater.from(context).inflate(R.layout.item_audio_new, parent, false)
            return SearchAudioViewHiolder(mView)
        } else if (viewType == VIDEO_VIEW_HOLDER) {
            var mView: View = LayoutInflater.from(context).inflate(R.layout.item_video, parent, false)
            return SearchVideoViewHolder(mView)
        }
        return null!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        mShimmerViewContainer!!.visibility=View.GONE
        if (holder.itemViewType == AUDIO_VIEW_HOLDER) {
            (holder as SearchAudioViewHiolder).bindData(
                context, position,
                dataList!![position] as DataS,
                this
            )
        } else if (holder.itemViewType == VIDEO_VIEW_HOLDER) {
            (holder as SearchVideoViewHolder).bindData(
                context, position,
                dataList!![position] as DataS,
                this
            )
        }

    }

    //filter method
    override fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    dataList = filtered_dataList
                } else {
                    var filteredList = mutableListOf<DataS>()
                    for (row in filtered_dataList) {
                        //change this to filter according to your case
                        if (row.title.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }
                    dataList = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = dataList
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                dataList = filterResults.values as MutableList<DataS>
                if (dataList.size > 0) {
                    txNoData.setVisibility(View.GONE)
                } else {
                    txNoData.setVisibility(View.VISIBLE)
                    txNoData.setText("No Results found for '$charSequence'")
                }
                notifyDataSetChanged()
            }
        }
    }


    override fun getItemViewType(position: Int): Int {
        if (dataList!![position]!!.viewType == 1) return VIDEO_VIEW_HOLDER
        else if (dataList!![position]!!.viewType == 0) return AUDIO_VIEW_HOLDER
       return -1
    }

    override fun getItemCount(): Int {
        return dataList!!.size
    }

    init {
        this.dataList = dataList
        this.filtered_dataList = dataList
    }


    //TERRISTAL CHECK
    private fun perform() {
        Log.e("Vishal","1")
        for (i in dataList) {
            val dataSourceFactory = DefaultDataSourceFactory(context!!, Util.getUserAgent(context!!, context!!.getString(R.string.app_name)))
            val cachedDataSourceFactory = CacheDataSourceFactory((context.application as MyApplication).appContainer.downloadCache, dataSourceFactory)
             ProgressiveMediaSource.Factory(cachedDataSourceFactory).createMediaSource(
                Uri.parse(i.link))

        }
    }

}