package com.icma.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.icma.app.R
import com.icma.app.utils.StringFormatter
import com.icma.app.model.DataPrayer
import java.text.SimpleDateFormat
import java.util.*

class Home5Adapter(
    val dataList0: MutableList<DataPrayer>,
   val mShimmerViewContainer: ShimmerFrameLayout
) : RecyclerView.Adapter<Home5Adapter.ViewHolder>() {

    private val viewPool = RecyclerView.RecycledViewPool()
    var formattedDate:String=""


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Home5Adapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_prayer_wall, parent, false)
        return ViewHolder(v)    }

    override fun onBindViewHolder(holder: Home5Adapter.ViewHolder, position: Int) {
        mShimmerViewContainer.visibility=View.GONE
        holder.txAudioHeadingMain.text =StringFormatter.capitalizeWord(dataList0.get(position).title)
        holder.txSubHeadingTV.text=dataList0.get(position).detail
        val coment_time: Long = dataList0.get(position).creationAt.toLong()
        timeChange(coment_time)
        holder.txTimeTv.text=formattedDate
    }

    override fun getItemCount(): Int {
       return dataList0.size
    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txAudioHeadingMain: TextView = itemView.findViewById(R.id.txHeadingTV)
        val txTimeTv: TextView = itemView.findViewById(R.id.txTimeTv)
        val txSubHeadingTV:TextView=itemView.findViewById(R.id.txSubHeadingTV)
    }

    fun timeChange(coment_time: Long) {
        val date = Date(coment_time * 1000L)
        val sdf = SimpleDateFormat("MMM dd hh:mma")
        sdf.timeZone = TimeZone.getDefault()
        formattedDate = sdf.format(date)
    }
}
