package com.icma.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.icma.app.R
import com.icma.app.model.AudioItem
import android.content.Context
import android.net.Uri
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.icma.app.utils.DownloadUtil
import com.icma.app.model.DataItem


class Home2Adapter(
    val context: Context?,
    val dataList0: MutableList<AudioItem>,
    val mShimmerViewContainer: ShimmerFrameLayout?,
    val mainlly: LinearLayout
) : RecyclerView.Adapter<Home2Adapter.ViewHolder>() {

    private val viewPool = RecyclerView.RecycledViewPool()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Home2Adapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_home2, parent, false)
        return ViewHolder(v)    }

    override fun onBindViewHolder(holder: Home2Adapter.ViewHolder, position: Int) {
        mShimmerViewContainer!!.visibility=View.GONE
        mainlly.visibility=View.VISIBLE
        holder.txAudioHeadingMain.text =dataList0.get(position).subcategoryName
        val childLayoutManager = LinearLayoutManager(holder.recylclerAudio.context, LinearLayoutManager.VERTICAL, false)
        holder.recylclerAudio.apply {
            layoutManager = childLayoutManager
            adapter = Home2AdapterChild(context,dataList0.get(position).data)
        }

        //pvtcheck
        perform(position,dataList0.get(position).data)

    }



    override fun getItemCount(): Int {
       return dataList0.size
    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txAudioHeadingMain: TextView = itemView.findViewById(R.id.txAudioHeadingMain)
        val recylclerAudio: RecyclerView = itemView.findViewById(R.id.recylclerAudio)
    }

    private fun perform(position: Int, data: MutableList<DataItem>) {
        for (i in 0..dataList0.get(position).data.size-1) {
            val player: SimpleExoPlayer
            @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode =
                DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
            val loadControl = DefaultLoadControl.Builder()
                .setBufferDurationsMs(25000, 50000, 1500, 2000).createDefaultLoadControl()
            val renderersFactory =
                context?.let { DefaultRenderersFactory(it).setExtensionRendererMode(extensionRendererMode) }
            player =
                context?.let { renderersFactory?.let { it1 -> SimpleExoPlayer.Builder(it, it1).setLoadControl(loadControl).build() } }!!
            val dataSourceFactory = DefaultDataSourceFactory(
                context,
                Util.getUserAgent(context!!, "ICMA")
            )
            val cacheDataSourceFactory =
                CacheDataSourceFactory(DownloadUtil.getCache(context), dataSourceFactory)
            val videoSource: MediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory)
                .createMediaSource(Uri.parse(dataList0.get(position).data.get(i).audio))

            player.prepare(videoSource,true,true)
            player.addListener(object : Player.EventListener {
                override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                    when (playbackState) {
                        Player.STATE_READY -> player.release()
                        else -> {
                        }
                    }
                }
            })
        }
    }
}
