package com.icma.app.viewholder

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ImageSpan
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.icma.app.R
import com.icma.app.activities.AudioActivityNew
import com.icma.app.adapters.SearchScreenAdapter
import com.icma.app.model.DataItem
import com.icma.app.model.DataS
import java.io.Serializable

class SearchAudioViewHiolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val txAudioHeading: TextView = itemView.findViewById(R.id.txAudioHeading)
    val txAudoTiming: TextView = itemView.findViewById(R.id.txAudoTiming)
    val audioImage:ImageView=itemView.findViewById(R.id.audioImage)
    val llyAudioheadingSingle: LinearLayout =itemView.findViewById(R.id.llyAudioheadingSingle)
    val txAudioHeadingS: TextView = itemView.findViewById(R.id.txAudioHeadingS)

    fun bindData(context: Context?, position: Int, dataS: DataS?, searchScreenAdapter: SearchScreenAdapter) {

        val ss = SpannableString("ggg "+dataS!!.title)
        val d: Drawable = context!!.getDrawable(R.drawable.ic_musiic)!!
        d.setBounds(0, 0, d.intrinsicWidth-20, d.intrinsicHeight-28)
        val span = ImageSpan(d, ImageSpan.ALIGN_BASELINE)
        ss.setSpan(span, 0, 3, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
        txAudioHeading.setText(ss)
        if (dataS!!.title.trim().length>20) {
            txAudioHeading.visibility=View.VISIBLE
            llyAudioheadingSingle.visibility=View.GONE
            val ss = SpannableString("ggg " + dataS!!.title)
            val d: Drawable = context!!.getDrawable(R.drawable.ic_musiic)!!
            d.setBounds(0, 0, d.intrinsicWidth - 32, d.intrinsicHeight - 32)
            val span = ImageSpan(d, ImageSpan.ALIGN_BASELINE)
            ss.setSpan(span, 0, 3, Spannable.SPAN_EXCLUSIVE_INCLUSIVE)
            txAudioHeading.setText(ss)
        }
        else
        {
            txAudioHeading.visibility=View.GONE
            llyAudioheadingSingle.visibility=View.VISIBLE
            txAudioHeadingS.setText(dataS!!.title)
        }


        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_audio_thumb)
            .error(R.drawable.ic_audio_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
        context?.let { Glide.with(it).load(dataS.audioThumbnail).apply(options).into(audioImage) }
        //txAudoTiming.text=context!!.getString(R.string.new_aud_time)
        txAudoTiming.text=dataS!!.endTime
        itemView.setOnClickListener {
            val intent = Intent(context, AudioActivityNew::class.java)
            intent.putExtra("audioUrl",dataS!!.link)
            intent.putExtra("position", position)
            intent.putExtra("audioThumbnail",dataS.audioThumbnail)
            intent.putExtra("title",dataS.title)
            context!!.startActivity(intent)
        }
    }
}