package com.icma.app.viewholder

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.activities.VideoActivity
import com.icma.app.adapters.SearchScreenAdapter
import com.icma.app.model.DataItem
import com.icma.app.model.DataS
import java.util.*
import kotlin.collections.ArrayList

class SearchVideoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val vidThumbnail: ImageView = itemView.findViewById(R.id.vidThumbnail)
    val txPlayTime: TextView = itemView.findViewById(R.id.txPlayTime)
    val txHeading: TextView = itemView.findViewById(R.id.txVideoHeading)

    fun bindData(context: Context?, position: Int, mList: DataS, searchScreenAdapter: SearchScreenAdapter) {
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_audio_thumb)
            .error(R.drawable.ic_audio_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
        context?.let { Glide.with(it).load(mList.videoThumbnail).apply(options).into(vidThumbnail) }
        txPlayTime.text= mList.endTime
        txHeading.text = mList!!.title.trim()
        var vidLink:String=mList!!.link
        itemView.setOnClickListener {
            if (!vidLink.equals(""))
            {
                val intent = Intent(context, VideoActivity::class.java)
                intent.putExtra("videoURL", vidLink)
                context!!.startActivity(intent)
            }
            else{
                Constants.showToast(context as Activity?,context!!.getString(R.string.check_in_sometime))
            }
        }
    }
}

