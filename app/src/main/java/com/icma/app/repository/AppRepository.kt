package com.hadi.retrofitmvvm.repository
import com.icma.app.dataobject.RequestBodies
import com.icma.app.retrofit.RetrofitInstance
import okhttp3.MultipartBody
import okhttp3.RequestBody

class AppRepository {

    suspend fun loginUser(body: RequestBodies.LoginBody) = RetrofitInstance.appApi.loginUser(body)

    suspend fun signUpUser(body: RequestBodies.SignUpBody) = RetrofitInstance.appApi.signUpUser(body)

    suspend fun forgotPassword(body: RequestBodies.ForgotPswdBody) = RetrofitInstance.appApi.forgotPswd(body)

    suspend fun home2Data(authToken: String, body: RequestBodies.Home2Body) = RetrofitInstance.appApi.homeVideoAudioData(authToken,body)

    suspend fun home1Data(body: RequestBodies.Home5Body, authToken: String) = RetrofitInstance.appApi.home1Data(authToken,body)

    suspend fun home4Data(body: RequestBodies.Home5Body, authToken: String)=RetrofitInstance.appApi.home4Data(authToken,body)

    suspend fun home5Data(authToken: String, body: RequestBodies.Home5Body) = RetrofitInstance.appApi.homePrayers(authToken,body)

    suspend fun addPrayer(authToken: String, body: RequestBodies.PrayerBody) = RetrofitInstance.appApi.addPrayer(authToken,body)

    suspend fun contactUs(authToken: String, body: RequestBodies.ContactUsBody) = RetrofitInstance.appApi.contactUs(authToken,body)

    suspend fun logOut(authToken: String) = RetrofitInstance.appApi.logOut(authToken)

    suspend fun getNotifications(authToken: String, body: RequestBodies.Home5Body)= RetrofitInstance.appApi.getNotifications(authToken,body)

    suspend fun changePswd(authToken: String, body: RequestBodies.ChangePswdBody)= RetrofitInstance.appApi.changePswd(authToken,body)


    suspend fun searchData(authToken: String, body: RequestBodies.SearchBody)= RetrofitInstance.appApi.searchData(authToken,body)

    suspend fun setInAppPurchaseData( body: RequestBodies.InappPuchaseBody)=RetrofitInstance.appApi.inappPurchase(body)

    suspend fun getPaymentSatus(authToken: String)=RetrofitInstance.appApi.getPaymentStatus(authToken)

    suspend fun freemembershipCheck(authToken: String)=RetrofitInstance.appApi.freeMembershipCheck(authToken)


    fun profileSaveDataRequest(
        token: String,
        profileimage: MultipartBody.Part?,
        firstname: RequestBody,
        lastname: RequestBody,
        phone: RequestBody,
        password: RequestBody
    ) = RetrofitInstance.appApi.profileSaveDataRequest(token, profileimage,firstname,lastname,phone,password)



}