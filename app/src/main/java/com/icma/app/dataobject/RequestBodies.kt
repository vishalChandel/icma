package com.icma.app.dataobject

import android.graphics.Bitmap

object RequestBodies {
    data class LoginBody
        (
        val email: String,
        val password: String
        )

    data class  SignUpBody
        (
        val firstname:String,
        val lastname:String,
        val email:String,
        val password:String,
        val referral_by:String
        )
    data class ForgotPswdBody
        (
        val email: String
        )

    data class Home2Body
        (
        val categoryid:String
        )
    data class Home5Body
        (
        val perPage:String,
        val pageNo: String
    )

   data class PrayerBody(
       val name: String,
       val title: String,
       val detail: String
       )
    data class EditProfileBody(
        val firstName: String,
        val lastName: String,
        val email: String,
        val password: String,
        val phone: String,
        val mBitmap: Bitmap?
    )
    data class ContactUsBody(
        val name: String,
        val email: String,
        val message: String
    )

    data class ChangePswdBody (
        val  old_password:String,
        val  new_password:String
    )
    data class SearchBody(
        val title:String,
        val perPage:String,
        val pageNo:String,
    )

    data class InappPuchaseBody(
        val userid: String,
        val title: String,
        val endDate: String,
        val referralby: String,
        val devicetype: String,
        val devicemodel: String,
        val transactionid:String
        )

}