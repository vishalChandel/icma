package com.icma.app.utils

import android.content.Context
import android.content.SharedPreferences

object ICMAPrefrences {


    /***************
     * Define SharedPrefrances Keys & MODE
     */
    const val PREF_NAME = "App_PREF"
    const val MODE = Context.MODE_PRIVATE

    /*
     * Keys
     * */
    const val ID = "user_id"
    const val USERID = "user_id"
    const val ISLOGIN = "is_login"
    const val EMAIL = "email"
    const val PASSWORD = "password"
    const val NAME = "name"
    const val DEVICETYPE = "device_type"
    const val DEVICETOKEN = "device_token"
    const val PHOTO = "photo"
    const val REFERRALCODE = "referral_code"
    const val REFERRALCOUNT = "referral_count"
    const val AUTHTOKEN = "authtoken"
    const val STATUS = "status"
    const val FIRSTNAME="first_name"
    const val LASTNAME="last_name"
    const val REMEMBER_CREDENTIALS = "remember_credentials"
    const val REMEMBER_EMAIL = "remember_email"
    const val REMEMBER_PASSWORD = "remeber_password"
    const val START_DATE = "start_date"
    const val EXPIRATION_DATE = "end_date"
    const val FREE_MEMBERSHIP_TEXT = "free_membership_text"
    const val FREE_MEMBERSHIP_STATUS = "free_membership_status"


    /*
     * Write the Boolean Value
     * */
    fun writeBoolean(context: Context, key: String?, value: Boolean) {
        getEditor(context).putBoolean(key, value).apply()
    }

    /*
     * Read the Boolean Value
     * */
    fun readBoolean(context: Context, key: String?, defValue: Boolean): Boolean {
        return getPreferences(context).getBoolean(key, defValue)
    }

    /*
     * Write the Integer Value
     * */
    fun writeInteger(context: Context, key: String?, value: Int) {
        getEditor(context).putInt(key, value).apply()
    }

    /*
     * Read the Interger Value
     * */
    fun readInteger(context: Context, key: String?, defValue: Int): Int {
        return getPreferences(context).getInt(key, defValue)
    }

    /*
     * Write the String Value
     * */
    fun writeString(context: Context, key: String?, value: String?) {
        getEditor(context).putString(key, value).apply()
    }

    /*
     * Read the String Value
     * */
    fun readString(context: Context, key: String?, defValue: String?): String? {
        return getPreferences(context).getString(key, defValue)
    }

    /*
     * Write the Float Value
     * */
    fun writeFloat(context: Context, key: String?, value: Float) {
        getEditor(context).putFloat(key, value).apply()
    }

    /*
     * Read the Float Value
     * */
    fun readFloat(context: Context, key: String?, defValue: Float): Float {
        return getPreferences(context).getFloat(key, defValue)
    }

    /*
     * Write the Long Value
     * */
    fun writeLong(context: Context, key: String?, value: Long) {
        getEditor(context).putLong(key, value).apply()
    }

    /*
     * Read the Long Value
     * */
    fun readLong(context: Context, key: String?, defValue: Long): Long {
        return getPreferences(context).getLong(key, defValue)
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    fun getPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREF_NAME, MODE)
    }

    /*
     * Return the SharedPreferences Editor
     * */
    fun getEditor(context: Context): SharedPreferences.Editor {
        return getPreferences(context).edit()
    }
}
