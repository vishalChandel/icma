package com.icma.app.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.android.exoplayer2.offline.DownloadHelper
import com.google.android.exoplayer2.offline.DownloadService
import com.google.android.exoplayer2.util.Util
import com.icma.app.R
import com.icma.app.activities.LoginActivity
import com.icma.app.app.MyApplication
import com.icma.app.app.MyDownloadService
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.security.KeyStore
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import java.util.*
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManagerFactory


class Constants {
 companion object {
  val SUBSCRIPTION_KEY="com.diversitypop.app_montly_subscription_release_acxfqwseft123"



  /*
       *
       * Subscriptions Keys & Details
       * */
  const val MERCHANT_ID: String = "478777392370591989"
  const val LICENCE_KEY: String = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjm2aEumBqhAer/o5Bm3MSNku50DEnA3ApVu0IAFvkJqjjCZydXUgl3kH+LlqZ8fqrjCihhoKJSmyOpjd7AesrqoCrKFrnGrfG9J4RfxGm1WQ4LMWDPvw/ZwdUg8WnzecPYgF6Ikq8av/2rKDMrbpDI2H+ly6YomBs2lcLU8PE9PopgtAdYAJ/d8ERo4EhetkUxgpev0KwRTaJaG2qWbQ84XG0safzl23WX9DlxfC6MgwmT1IeQClgkkbfslL13sGRWXAGGoLuEhr//6erg0e0xXOTGdf4m8Sce1KiDQQE65gm/Xjbubaux7LJIyO6TOw9ckXEeqa2Nw9V0iJWafbiwIDAQAB"
  const val SUBSCRIPTION_PLAN_KEY: String = "icma.yearly_"

  //Splash Screen
  public var SPLASH_TIME_OUT = 500L
  private var progressDialog: Dialog? = null
  val TERMS_AND_CONDITIONS="https://www.dharmani.com/ICMA/webservices/Privacy.html"
  val ABOUT_US_LINK="https://icma.net/about-us/"
  val BLOGS_LINK="https://icma.net/blog/"
  val PRIVACY_POLICY_LINK="https://www.freeprivacypolicy.com/live/ca9d05c5-dbd9-45ca-89ad-5bb2d34f6677"
  val MODEL = "model"
  var VIDEOID = ""
  var VIDEONAME = ""

  //PayPal
  val CLIENT_ID="AemhRQaRuMAufUKGQdoDp3V6GUgEKgd2pymwBKrjsxeX69XkPb9WKRJ_J503kbr53gmjya3jW087M5ju"

  var AUDIO_VIEW_HOLDER = 0
  var VIDEO_VIEW_HOLDER = 1


  //Internet connection check
  fun hasInternetConnection(application: MyApplication): Boolean {
   val connectivityManager = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
    val activeNetwork = connectivityManager.activeNetwork ?: return false
    val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
    return when {
     capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
     capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
     capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
     else -> false
    }
   } else {
    connectivityManager.activeNetworkInfo?.run {
     return when (type) {
      ConnectivityManager.TYPE_WIFI -> true
      ConnectivityManager.TYPE_MOBILE -> true
      ConnectivityManager.TYPE_ETHERNET -> true
      else -> false
     }
    }
   }
   return false
  }


  /*
     * Show Progress Dialog
     * */
  @RequiresApi(api = Build.VERSION_CODES.KITKAT)
  @JvmStatic
  fun showProgressDialog(mActivity: Activity?) {
   progressDialog = Dialog(mActivity!!)
   progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
   progressDialog!!.setContentView(R.layout.dialog_progress)
   Objects.requireNonNull<Window>(progressDialog!!.getWindow())
    .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
   progressDialog!!.setCanceledOnTouchOutside(false)
   progressDialog!!.setCancelable(false)
   if (progressDialog != null&& !progressDialog!!.isShowing)
   {progressDialog!!.show()}
  }


  /*
   * Hide Progress Dialog
   * */
  @JvmStatic
  fun dismissProgressDialog() {
   if (progressDialog != null && progressDialog!!.isShowing()) {
    progressDialog!!.dismiss()
   }
  }


  //  Show Alert Dialog
  fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
   val alertDialog = Dialog(mActivity!!)
   alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
   alertDialog.setContentView(R.layout.dialog_alert)
   alertDialog.setCanceledOnTouchOutside(false)
   alertDialog.setCancelable(false)
   alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
   // set the custom dialog components - text, image and button
   val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
   val btnDismiss = alertDialog.findViewById<TextView>(R.id.txOk)
   txtMessageTV.text = strMessage
   btnDismiss.setOnClickListener { alertDialog.dismiss() }
   alertDialog.show()
  }

  //  Show Toast Message
  fun showToast(mActivity: Activity?, strMessage: String?) {
   val view: View = LayoutInflater.from(mActivity)
    .inflate(com.icma.app.R.layout.toast_layout, null)
   val tvMessage = view.findViewById<TextView>(com.icma.app.R.id.tvMessage)
   tvMessage.setText(strMessage)
   val toast = Toast(mActivity)
   toast.setView(view);
   toast.show();  }


  //Bitmap coversion methods
   fun convertBitmapToByteArrayUncompressed(bitmap: Bitmap): ByteArray? {
   val stream = ByteArrayOutputStream()
   bitmap.compress(Bitmap.CompressFormat.PNG, 40, stream)
   return stream.toByteArray()
  }

  open fun getAlphaNumericString(): String? {
   var n = 20
   val AlphaNumericString = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" /*+ "0123456789"*/
           + "abcdefghijklmnopqrstuvxyz")
   val sb = StringBuilder(n)

    val index = (AlphaNumericString.length
            * Math.random()).toInt()
    sb.append(
     AlphaNumericString[index]
    )

   return sb.toString()
  }


  //  Show Alert Dialog
  fun showAuthDismissDialog(mActivity: Activity?, strMessage: String?) {
   val alertDialog = Dialog(mActivity!!)
   alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
   alertDialog.setContentView(R.layout.dialog_alert)
   alertDialog.setCanceledOnTouchOutside(false)
   alertDialog.setCancelable(false)
   alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
   // set the custom dialog components - text, image and button
   val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
   val btnDismiss = alertDialog.findViewById<TextView>(R.id.txOk)
   txtMessageTV.text = strMessage
   btnDismiss.setOnClickListener { alertDialog.dismiss()
   performSwitchDel(mActivity)}
   alertDialog.show()
  }

  //AUTH TOKEN EXPIRE DIALOG
  private fun performSwitchDel(mActivity: Activity) {
   val preferences: SharedPreferences = ICMAPrefrences.getPreferences(
    Objects.requireNonNull(mActivity)
   )
   val editor = preferences.edit()
   editor.clear()
   editor.apply()
      val mIntent = Intent(mActivity, LoginActivity::class.java)
   mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
   mActivity.startActivity(mIntent)
  }


  fun downloadMedia(context: Context,uri:Uri){
   //the download request
   val helper = context?.let { DownloadHelper.forProgressive(it, uri) }
   helper?.prepare(object : DownloadHelper.Callback {
    override fun onPrepared(helper: DownloadHelper) {
     val json = JSONObject()
     //extra data about the download like title, artist e.tc below is an example
     json.put("uri" ,uri.toString())
     json.put("artist" ,"Promise chornma")
     var uniqId:String= (System.currentTimeMillis()/1000L).toString()
     val download =
      helper.getDownloadRequest(uniqId, Util.getUtf8Bytes(json.toString()))
     //sending the request to the download service
     context?.let {
      DownloadService.sendAddDownload(
       it,
       MyDownloadService::class.java,
       download,
       true
      )
     }
    }

    override fun onPrepareError(helper: DownloadHelper, e: IOException) {
     e.printStackTrace()
     Toast.makeText(context, e.localizedMessage, Toast.LENGTH_SHORT).show()
    }
   })

  }



 }





}