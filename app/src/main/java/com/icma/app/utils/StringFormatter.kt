package com.icma.app.utils

object StringFormatter {
    fun capitalizeWord(str: String): String {
        var words: Array<String>? = null
        words = str.split("\\s").toTypedArray()
        var capitalizeWord = ""
        if (words.size > 0) {
            for (w in words) {
                val first = w.substring(0, 1)
                val afterfirst = w.substring(1)
                capitalizeWord += first.toUpperCase() + afterfirst + " "
            }
        } else {
            capitalizeWord = str
        }
        return capitalizeWord.trim { it <= ' ' }
    }
}