package com.icma.app.utils

import android.content.Context
import com.google.android.exoplayer2.offline.DownloadManager
import com.google.android.exoplayer2.upstream.cache.Cache
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import java.io.File

object DownloadUtil {
    private var cache: Cache? = null
    private val downloadManager: DownloadManager? = null
    @Synchronized
    fun getCache(context: Context): Cache? {
        if (cache == null) {
            // LeastRecentlyUsedCacheEvictor evictor = new LeastRecentlyUsedCacheEvictor(maxCacheSize);
            val cacheDirectory = File(context.getExternalFilesDir(null), "downloads")
            cache = SimpleCache(cacheDirectory, NoOpCacheEvictor())
        }
        return cache
    }
}