package com.icma.app.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Event
import com.icma.app.utils.Resource
import com.icma.app.app.MyApplication
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.LoginModel
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class LoginViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {

    private val _loginResponse = MutableLiveData<Event<Resource<LoginModel>>>()
    val loginResponse:LiveData<Event<Resource<LoginModel>>> = _loginResponse


    fun loginUser(mActivity: Activity, body: RequestBodies.LoginBody) = viewModelScope.launch {
        login(mActivity,body)
    }

    private suspend fun login(mActivity: Activity, body: RequestBodies.LoginBody) {
        _loginResponse.postValue(Event(Resource.Loading()))
        try {
            if (Constants.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.loginUser(body)
                if (response.code()!=500)
                {
                    _loginResponse.postValue(handleResponse(response))
                }
                else
                {
                    _loginResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.server_error))))
                }
            } else {
                _loginResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.no_internet_connection))))
            }
        } catch (t: Throwable) {
            Log.e("ISSUE",""+t.message)
            when (t) {
                is IOException -> {
                    _loginResponse.postValue(
                        Event(Resource.Error(
                            ""+t.message
                        ))

                    )
                }
                else -> {
                    _loginResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<LoginModel>): Event<Resource<LoginModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}