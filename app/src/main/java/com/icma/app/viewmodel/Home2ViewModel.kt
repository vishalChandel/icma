package com.icma.app.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Event
import com.icma.app.utils.Resource
import com.icma.app.app.MyApplication
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.VideoAudioModel
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class Home2ViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _homeResponse = MutableLiveData<Event<Resource<VideoAudioModel>>>()
    val homeResponse:LiveData<Event<Resource<VideoAudioModel>>> = _homeResponse


    fun homeGetData(authToken: String, body: RequestBodies.Home2Body,mActivity:Activity) = viewModelScope.launch {
        homeData(authToken,body,mActivity)
    }

    private suspend fun homeData(
        authToken: String,
        body: RequestBodies.Home2Body,
        mActivity: Activity
    ) {
        _homeResponse.postValue(Event(Resource.Loading()))
        try {
            if (Constants.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.home2Data(authToken,body)
                if (response.code()==200)
                {
                    _homeResponse.postValue(handleResponse(response))}
                else
                {
                    Constants.showAuthDismissDialog(mActivity,getApplication<MyApplication>().getString(R.string.authorization_failure))
                }

            } else {
                _homeResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _homeResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _homeResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<VideoAudioModel>): Event<Resource<VideoAudioModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}