package com.icma.app.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Constants.Companion.convertBitmapToByteArrayUncompressed
import com.icma.app.utils.Constants.Companion.getAlphaNumericString
import com.icma.app.utils.Event
import com.icma.app.utils.Resource
import com.icma.app.app.MyApplication
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.EditProfileModel
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class EditProfileViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {

    private val _loginResponse = MutableLiveData<Event<Resource<EditProfileModel>>>()
    val loginResponse:LiveData<Event<Resource<EditProfileModel>>> = _loginResponse


    fun editUser(authToken: String, body: RequestBodies.EditProfileBody,mActivity: Activity) = viewModelScope.launch {
        edit(authToken,body,mActivity)
    }

    private suspend fun edit(
        authToken: String,
        body: RequestBodies.EditProfileBody,
        mActivity: Activity
    ) {
        _loginResponse.postValue(Event(Resource.Loading()))
        try {
            if (Constants.hasInternetConnection(getApplication<MyApplication>())) {
                val firstName: RequestBody = body.firstName.toRequestBody("multipart/form-data".toMediaTypeOrNull())
                val lastName: RequestBody = body.lastName.toRequestBody("multipart/form-data".toMediaTypeOrNull())
                val phone: RequestBody = body.phone.toRequestBody("multipart/form-data".toMediaTypeOrNull())
                val password: RequestBody = body.password.toRequestBody("multipart/form-data".toMediaTypeOrNull())
                var mMultipartBody: MultipartBody.Part? = null
                if (body.mBitmap != null) {
                    Log.d("EditProfile",">>>>>>value is :"+ body.mBitmap)
                    val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(body.mBitmap!!)?.let {
                        RequestBody.create(
                            "multipart/form-data".toMediaTypeOrNull(),
                            it
                        )
                    }
                    mMultipartBody = requestFile1?.let {
                        MultipartBody.Part.createFormData(
                            "profileimage", getAlphaNumericString()!!.toString() + ".jpeg",
                            it
                        )
                    }
                }
                val response = appRepository.profileSaveDataRequest(authToken,mMultipartBody,firstName,lastName,phone,password)
                response.enqueue(object : Callback<EditProfileModel> {
                    override fun onResponse(
                        call: Call<EditProfileModel>,
                        response: Response<EditProfileModel>
                    ) {
                        Log.e("Check",""+response.code())
                        Log.e("Check",":::"+response.body()!!.status)

                        if (response.code()==200 && response.body()!!.status!=301) {
                            if (response.isSuccessful) {
                                _loginResponse.postValue(handleResponse(response))
                            } else {
                                    _loginResponse.postValue(
                                        Event(
                                            Resource.Error(
                                                response.message()
                                            )
                                        )
                                    )
                            }
                        }
                        else
                        {
                            Constants.showAuthDismissDialog(mActivity,getApplication<MyApplication>().getString(R.string.authorization_failure))
                        }

                    }

                    override fun onFailure(call: Call<EditProfileModel>, t: Throwable) {
                        Log.e("Check","failure"+t.message.toString())

                        _loginResponse.postValue(
                            Event(Resource.Error(
                                t.message.toString()
                            ))
                        )
                    }
                })
            } else {
                _loginResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.no_internet_connection))))
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _loginResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _loginResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<EditProfileModel>): Event<Resource<EditProfileModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}