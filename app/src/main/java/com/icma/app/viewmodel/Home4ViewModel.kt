package com.icma.app.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Event
import com.icma.app.utils.Resource
import com.icma.app.app.MyApplication
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.Home1Model
import com.icma.app.model.HymnModel
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class Home4ViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _homeResponse = MutableLiveData<Event<Resource<HymnModel>>>()
    val homeResponse:LiveData<Event<Resource<HymnModel>>> = _homeResponse


    fun homeGetData(authToken: String, body: RequestBodies.Home5Body, activity: Activity) = viewModelScope.launch {
        homeData(authToken,body,activity)
    }

    private suspend fun homeData(
        authToken: String,
        body: RequestBodies.Home5Body,
        activity: Activity
    ) {
        _homeResponse.postValue(Event(Resource.Loading()))
        try {
            if (Constants.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.home4Data(body,authToken)
                Log.e("Check","iff1"+response.code())
                if (response.code()==200)
                {
                    _homeResponse.postValue(handleResponse(response))}
                else
                {
                    Constants.showAuthDismissDialog(activity,getApplication<MyApplication>().getString(R.string.authorization_failure))
                }

            } else {
                _homeResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _homeResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _homeResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<HymnModel>): Event<Resource<HymnModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}