package com.icma.app.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Event
import com.icma.app.utils.Resource
import com.icma.app.app.MyApplication
import com.icma.app.model.PaymentStatusModel
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class PaymentStatusViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _resultResponse = MutableLiveData<Event<Resource<PaymentStatusModel>>>()
    val resultResponse:LiveData<Event<Resource<PaymentStatusModel>>> = _resultResponse


    fun executePaymentStatus( authToken: String,mActivity: Activity) = viewModelScope.launch {
        getData(authToken,mActivity)
    }

    private suspend fun getData( authToken: String,mActivity: Activity) {
        _resultResponse.postValue(Event(Resource.Loading()))
        try {
            if (Constants.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.getPaymentSatus(authToken)
                if (response.code()==200)
                {
                    _resultResponse.postValue(handleResponse(response))
                }
                else
                {
                    _resultResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.server_error))))
                }

                Log.e("Check","iff"+response)
            } else {
                _resultResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _resultResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _resultResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<PaymentStatusModel>): Event<Resource<PaymentStatusModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}