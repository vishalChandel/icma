package com.icma.app.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Event
import com.icma.app.utils.Resource
import com.icma.app.app.MyApplication
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.ForgotPasswordModel
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class ForgotPasswordViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _forgotPswdResponse = MutableLiveData<Event<Resource<ForgotPasswordModel>>>()
    val forgotPswdResponse:LiveData<Event<Resource<ForgotPasswordModel>>> = _forgotPswdResponse


    fun forgotPswdUser(body: RequestBodies.ForgotPswdBody) = viewModelScope.launch {
        forgotPswd(body)
    }

    private suspend fun forgotPswd(body: RequestBodies.ForgotPswdBody) {
        _forgotPswdResponse.postValue(Event(Resource.Loading()))
        try {
            if (Constants.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.forgotPassword(body)
                _forgotPswdResponse.postValue(handleResponse(response))
                Log.e("Check","iff")
            } else {
                _forgotPswdResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _forgotPswdResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _forgotPswdResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<ForgotPasswordModel>): Event<Resource<ForgotPasswordModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}