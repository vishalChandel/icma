package com.icma.app.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Event
import com.icma.app.utils.Resource
import com.icma.app.app.MyApplication
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.InAppPurchaseModel
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class MemberShipViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _resultResponse = MutableLiveData<Event<Resource<InAppPurchaseModel>>>()
    val resultResponse:LiveData<Event<Resource<InAppPurchaseModel>>> = _resultResponse


    fun executeInapp( body: RequestBodies.InappPuchaseBody,mActivity: Activity) = viewModelScope.launch {
        getData(body,mActivity)
    }

    private suspend fun getData( body: RequestBodies.InappPuchaseBody,mActivity: Activity) {
        _resultResponse.postValue(Event(Resource.Loading()))
        try {
            if (Constants.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.setInAppPurchaseData(body)
                if (response.code()==200)
                {
                    _resultResponse.postValue(handleResponse(response))
                }
                else
                {
                    _resultResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.server_error))))
                }

                Log.e("Check","iff"+response)
            } else {
                _resultResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _resultResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _resultResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<InAppPurchaseModel>): Event<Resource<InAppPurchaseModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}