package com.icma.app.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Event
import com.icma.app.utils.Resource
import com.icma.app.app.MyApplication
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.AddPrayerModel
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class AddPrayerViewModel(
    app: Application,
    private val appRepository: AppRepository
) : AndroidViewModel(app) {


    private val _prayerAddResponse = MutableLiveData<Event<Resource<AddPrayerModel>>>()
    val prayerResponse:LiveData<Event<Resource<AddPrayerModel>>> = _prayerAddResponse


    fun prayerGetData(authToken: String, body: RequestBodies.PrayerBody, mActivity: Activity) = viewModelScope.launch {
        prayerData(authToken,body,mActivity)
    }

    private suspend fun prayerData(
        authToken: String,
        body: RequestBodies.PrayerBody,
        mActivity: Activity
    ) {
        _prayerAddResponse.postValue(Event(Resource.Loading()))
        try {
            if (Constants.hasInternetConnection(getApplication<MyApplication>())) {
                val response = appRepository.addPrayer(authToken,body)
              if (response.code()==200)
                {
                    _prayerAddResponse.postValue(handleResponse(response))}
                else
                {
                Constants.showAuthDismissDialog(mActivity,getApplication<MyApplication>().getString(R.string.authorization_failure))
                }
                Log.e("Check","iff"+response)
            } else {
                _prayerAddResponse.postValue(Event(Resource.Error(getApplication<MyApplication>().getString(R.string.no_internet_connection))))

                Log.e("Check","else")
            }
        } catch (t: Throwable) {
            when (t) {
                is IOException -> {
                    _prayerAddResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.network_failure
                            )
                        ))
                    )
                }
                else -> {
                    _prayerAddResponse.postValue(
                        Event(Resource.Error(
                            getApplication<MyApplication>().getString(
                                R.string.conversion_error
                            )
                        ))
                    )
                }
            }
        }
    }

    private fun handleResponse(response: Response<AddPrayerModel>): Event<Resource<AddPrayerModel>>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Event(Resource.Success(resultResponse))
            }
        }
        return Event(Resource.Error(response.message()))
    }
}