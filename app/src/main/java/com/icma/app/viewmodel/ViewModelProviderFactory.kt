package com.icma.app.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.hadi.retrofitmvvm.repository.AppRepository

class ViewModelProviderFactory(
    val app: Application,
    val appRepository: AppRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(SignUpViewModel::class.java)) {
            return SignUpViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(ForgotPasswordViewModel::class.java)) {
            return ForgotPasswordViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(Home2ViewModel::class.java)) {
            return Home2ViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(Home5ViewModel::class.java)) {
            return Home5ViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(AddPrayerViewModel::class.java)) {
            return AddPrayerViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(EditProfileViewModel::class.java)) {
            return EditProfileViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(Home1ViewModel::class.java)) {
            return Home1ViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(ContactUsViewModel::class.java)) {
            return ContactUsViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(LogOutViewModel::class.java)) {
            return LogOutViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(NotificationViewModel::class.java)) {
            return NotificationViewModel(app, appRepository) as T
        }

        if (modelClass.isAssignableFrom(ChangePasswordViewModel::class.java)) {
            return ChangePasswordViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            return SearchViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(MemberShipViewModel::class.java)) {
            return MemberShipViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(PaymentStatusViewModel::class.java)) {
            return PaymentStatusViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(MembershipFreeViewModel::class.java)) {
            return MembershipFreeViewModel(app, appRepository) as T
        }
        if (modelClass.isAssignableFrom(Home4ViewModel::class.java)) {
            return Home4ViewModel(app, appRepository) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}