package com.icma.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StatusModel(
    @field:SerializedName("status")
    val status: Int,

    @field:SerializedName("message")
    val message: String,


): Parcelable