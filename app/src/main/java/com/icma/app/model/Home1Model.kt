package com.icma.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Home1Model(

	@field:SerializedName("lastPage")
	val lastPage: String,

	@field:SerializedName("banner")
	val banner: Banner,

	@field:SerializedName("video")
	val video: List<VideoHome>,

	@field:SerializedName("message")
	val message: String,

	@field:SerializedName("status")
	val status: Int
) : Parcelable



@Parcelize
data class Banner(

	@field:SerializedName("creation_at")
	val creationAt: String,

	@field:SerializedName("description")
	val description: String,

	@field:SerializedName("id")
	val id: String,

	@field:SerializedName("banner_image")
	val bannerImage: String,

	@field:SerializedName("title")
	val title: String
) : Parcelable

@Parcelize
data class VideoHome(

	@field:SerializedName("video_thumbnail")
	val videoThumbnail: String,

	@field:SerializedName("start_time")
	val startTime: String,

	@field:SerializedName("thumbnail")
	val thumbnail: String,

	@field:SerializedName("creation_at")
	val creationAt: String,

	@field:SerializedName("name")
	val name: String,

	@field:SerializedName("end_time")
	val endTime: String,

	@field:SerializedName("video_width")
	val videoWidth: String? = null,

	@field:SerializedName("id")
	val id: String,

	@field:SerializedName("video")
	val video: String,

	@field:SerializedName("title")
	val title: String,

	@field:SerializedName("subcategory")
	val subcategory: String
) : Parcelable
