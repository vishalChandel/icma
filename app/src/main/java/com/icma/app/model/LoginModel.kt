package com.icma.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginModel(
    @field:SerializedName("status")
    val status: Int,

    @field:SerializedName("message")
    val message: String,

    @field:SerializedName("data")
    val data: DataProfile

): Parcelable

@Parcelize
data class DataProfile(
    @SerializedName("accessToken")
    val accessToken: String,

    @SerializedName("authtoken")
    val authtoken: String,

    @SerializedName("created")
    val created: String,

    @SerializedName("devicemodel")
    val devicemodel: String,

    @SerializedName("devicetoken")
    val devicetoken: String,

    @SerializedName("devicetype")
    val devicetype: String,

    @SerializedName("disable")
    val disable: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("emailverification")
    val emailverification: String,

    @SerializedName("end_date")
    val endDate: String,

    @SerializedName("firstname")
    val firstname: String,

    @SerializedName("lastname")
    val lastname: String,

    @SerializedName("password")
    val password: String,

    @SerializedName("payment_status")
    val paymentStatus: String,

    @SerializedName("paypalCity")
    val paypalCity: String,

    @SerializedName("paypalCountry")
    val paypalCountry: String,

    @SerializedName("paypalEmail")
    val paypalEmail: String,

    @SerializedName("paypalID")
    val paypalID: String,

    @SerializedName("paypalMiddleName")
    val paypalMiddleName: String,

    @SerializedName("paypalName")
    val paypalName: String,

    @SerializedName("paypalPostalCode")
    val paypalPostalCode: String,

    @SerializedName("paypalState")
    val paypalState: String,

    @SerializedName("paypalStreet1")
    val paypalStreet1: String,

    @SerializedName("paypalToken")
    val paypalToken: String,

    @SerializedName("paypalVerifiedAccount")
    val paypalVerifiedAccount: String,

    @SerializedName("phone")
    val phone: String,

    @SerializedName("profileimage")
    val profileimage: String,

    @SerializedName("referral_by")
    val referralBy: String,
    @SerializedName("referral_code")
    val referralCode: String,

    @SerializedName("referral_count")
    val referralCount: String,

    @SerializedName("refreshToken")
    val refreshToken: String,

    @SerializedName("start_date")
    val startDate: String,

    @SerializedName("userid")
    val userid: String,
    @SerializedName("verificationcode")
    val verificationcode: String
):Parcelable