package com.icma.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddPrayerModel(

	@field:SerializedName("status")
	val status: Int,

	@field:SerializedName("message")
	val message: String,

	@field:SerializedName("data")
	val data: DataPrayer,
	) : Parcelable

