package com.icma.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HymnModel(

	@field:SerializedName("lastPage")
	val lastPage: String? = null,

	@field:SerializedName("banner")
	val banner: BannerHymn? = null,

	@field:SerializedName("video")
	val video: List<VideoItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class BannerHymn(

	@field:SerializedName("creation_at")
	val creationAt: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("banner_image")
	val bannerImage: String? = null,

	@field:SerializedName("title")
	val title: String? = null
) : Parcelable

@Parcelize
data class VideoItem(

	@field:SerializedName("video_thumbnail")
	val videoThumbnail: String? = null,

	@field:SerializedName("hymns_video")
	val hymnsVideo: String? = null,

	@field:SerializedName("creation_at")
	val creationAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("title")
	val title: String? = null
) : Parcelable
