package com.icma.app.model

data class Media (val url: String, val artist: String,var title:String)