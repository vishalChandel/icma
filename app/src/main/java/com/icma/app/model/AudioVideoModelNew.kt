//package com.icma.app.model
//
//import android.os.Parcelable
//import com.google.gson.annotations.SerializedName
//import kotlinx.android.parcel.Parcelize
//
//@Parcelize
//data class AudioVideoModelNew(
//
//	@field:SerializedName("video")
//	val video: Video,
//
//	@field:SerializedName("audio")
//	val audio: List<AudioItem>,
//
//	@field:SerializedName("message")
//	val message: String,
//
//	@field:SerializedName("status")
//	val status: Int
//) : Parcelable
//
//@Parcelize
//data class Video(
//
//	@field:SerializedName("video_thumbnail")
//	val videoThumbnail: String,
//
//	@field:SerializedName("start_time")
//	val startTime: String,
//
//	@field:SerializedName("category_name")
//	val categoryName: String,
//
//	@field:SerializedName("video_title")
//	val videoTitle: String,
//
//	@field:SerializedName("category_id")
//	val categoryId: String,
//
//	@field:SerializedName("creation_at")
//	val creationAt: String,
//
//	@field:SerializedName("end_time")
//	val endTime: String,
//
//	@field:SerializedName("category_video")
//	val categoryVideo: String,
//
//	@field:SerializedName("id")
//	val id: String
//) : Parcelable
//
//@Parcelize
//data class DataItem(
//
//	@field:SerializedName("subcategory_id")
//	val subcategoryId: String,
//
//	@field:SerializedName("creation_at")
//	val creationAt: String,
//
//	@field:SerializedName("audio_thumbnail")
//	val audioThumbnail: String,
//
//	@field:SerializedName("id")
//	val id: String,
//
//	@field:SerializedName("audio")
//	val audio: String,
//
//	@field:SerializedName("title")
//	val title: String,
//
//	@field:SerializedName("categoryid")
//	val categoryid: String
//) : Parcelable
//
//@Parcelize
//data class AudioItem(
//
//	@field:SerializedName("data")
//	val data: List<DataItem>,
//
//	@field:SerializedName("subcategory_name")
//	val subcategoryName: String
//) : Parcelable
