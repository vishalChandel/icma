package com.icma.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EditProfileModel(

	@field:SerializedName("data")
	val data: Data1? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class Data1(

	@field:SerializedName("firstname")
	val firstname: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("authtoken")
	val authtoken: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null,

	@field:SerializedName("devicetoken")
	val devicetoken: String? = null,

	@field:SerializedName("userid")
	val userid: String? = null,

	@field:SerializedName("profileimage")
	val profileimage: String? = null,

	@field:SerializedName("devicetype")
	val devicetype: String? = null,

	@field:SerializedName("lastname")
	val lastname: String? = null,

	@field:SerializedName("emailverification")
	val emailverification: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("disable")
	val disable: String? = null,

	@field:SerializedName("verificationcode")
	val verificationcode: String? = null,

	@field:SerializedName("email")
	val email: String? = null
) : Parcelable
