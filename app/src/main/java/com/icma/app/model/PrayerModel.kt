package com.icma.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PrayerModel(

	@field:SerializedName("lastPage")
	val lastPage: String,

	@field:SerializedName("data")
	val data: List<DataPrayer>,

	@field:SerializedName("banner")
	val banner: PrayerBanner? = null,

	@field:SerializedName("message")
	val message: String,

	@field:SerializedName("status")
	val status: Int
) : Parcelable

@Parcelize
data class DataPrayer(

	@field:SerializedName("creation_at")
	val creationAt: String,

	@field:SerializedName("name")
	val name: String,

	@field:SerializedName("id")
	var id: String,

	@field:SerializedName("detail")
	val detail: String,

	@field:SerializedName("title")
	val title: String,

	@field:SerializedName("userid")
	val userid: String
) : Parcelable

@Parcelize
data class PrayerBanner(

	@field:SerializedName("creation_at")
	val creationAt: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("banner_image")
	val bannerImage: String? = null,

	@field:SerializedName("title")
	val title: String? = null
) : Parcelable