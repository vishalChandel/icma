package com.icma.app.model


import com.google.gson.annotations.SerializedName
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationModel(
    @SerializedName("data")
    val `data`: List<DataS>,
    @SerializedName("lastPage")
    val lastPage: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int
) : Parcelable

@Parcelize
data class DataX(
    @SerializedName("creation_at")
    val creationAt: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("notification_id")
    val notificationId: String,
    @SerializedName("notification_type")
    val notificationType: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("userid")
    val userid: String
) : Parcelable