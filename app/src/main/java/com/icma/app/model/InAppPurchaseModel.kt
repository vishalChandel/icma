package com.icma.app.model


import com.google.gson.annotations.SerializedName

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class InAppPurchaseModel(
    @SerializedName("data")
    val data : Data,
    @SerializedName("status")
    val status: Int
) : Parcelable