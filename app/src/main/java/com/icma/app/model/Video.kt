package com.icma.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Video(

    @field:SerializedName("video_thumbnail")
    val videoThumbnail: String,

    @field:SerializedName("start_time")
    val startTime: String,

    @field:SerializedName("category_name")
    val categoryName: String,

    @field:SerializedName("video_title")
    val videoTitle: String,

    @field:SerializedName("category_id")
    val categoryId: String,

    @field:SerializedName("creation_at")
    val creationAt: String,

    @field:SerializedName("end_time")
    val endTime: String,

    @field:SerializedName("category_video")
    val categoryVideo: String,

    @field:SerializedName("id")
    val id: String
) : Parcelable