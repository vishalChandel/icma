package com.icma.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VideoAudioModel(
	@field:SerializedName("video")
	val video: Video,

	@field:SerializedName("audio")
	val audio: List<AudioItem>,

	@field:SerializedName("message")
	val message: String,

	@field:SerializedName("status")
	val status: Int
) : Parcelable



@Parcelize
data class DataItem(

	@field:SerializedName("subcategory_id")
	val subcategoryId: String,

	@field:SerializedName("creation_at")
	val creationAt: String,

	@field:SerializedName("audio_thumbnail")
	val audioThumbnail: String="",

	@field:SerializedName("id")
	val id: String,

	@field:SerializedName("audio")
	val audio: String,

	@field:SerializedName("title")
	val title: String,

	@field:SerializedName("categoryid")
	val categoryid: String,

	@field:SerializedName("start_time")
	val start_time: String,

	@field:SerializedName("end_time")
	val end_time: String,

	) : Parcelable

@Parcelize
data class AudioItem(

	@field:SerializedName("data")
	val data: MutableList<DataItem>,

	@field:SerializedName("subcategory_name")
	val subcategoryName: String
) : Parcelable
