package com.icma.app.model


import com.google.gson.annotations.SerializedName
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataS(
    @SerializedName("audio")
    var audio: String,
    @SerializedName("audio_thumbnail")
    var audioThumbnail: String,
    @SerializedName("categoryid")
    var categoryid: String,
    @SerializedName("creation_at")
    var creationAt: String,
    @SerializedName("end_time")
    var endTime: String,
    @SerializedName("id")
    var id: String,
    @SerializedName("link")
    var link: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("start_time")
    var startTime: String,
    @SerializedName("subcategory")
    var subcategory: String,
    @SerializedName("subcategory_id")
    var subcategoryId: String,
    @SerializedName("title")
    var title: String,
    @SerializedName("type")
    var type: Int,
    @SerializedName("video")
    var video: String,
    @SerializedName("video_height")
    var videoHeight: String,
    @SerializedName("video_thumbnail")
    var videoThumbnail: String,
    @SerializedName("video_width")
    var videoWidth: String,

    var viewType: Int = -1


) : Parcelable {
    override fun toString(): String {
        return "DataS(audio='$audio', audioThumbnail='$audioThumbnail', categoryid='$categoryid', creationAt='$creationAt', endTime='$endTime', id='$id', link='$link', name='$name', startTime='$startTime', subcategory='$subcategory', subcategoryId='$subcategoryId', title='$title', type=$type, video='$video', videoHeight='$videoHeight', videoThumbnail='$videoThumbnail', videoWidth='$videoWidth', viewType=$viewType)"
    }
}