package com.icma.app.model


import com.google.gson.annotations.SerializedName
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchModel(
    @SerializedName("data")
    var `data`: List<DataS>,
    @SerializedName("message")
    var message: String,
    @SerializedName("status")
    var status: Int
) : Parcelable