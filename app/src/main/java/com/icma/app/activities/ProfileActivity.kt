package com.icma.app.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import coil.api.clear
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.ICMAPrefrences
import com.icma.app.utils.Resource
import com.icma.app.app.MyApplication
import com.icma.app.viewmodel.LogOutViewModel
import com.icma.app.viewmodel.MembershipFreeViewModel
import com.icma.app.viewmodel.PaymentStatusViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.*

class ProfileActivity : BaseActivity() {

    @BindView(R.id.llyBack)
    lateinit var llyBack: LinearLayout
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV: TextView
    @BindView(R.id.txUserName)
    lateinit var txUserName: TextView
    @BindView(R.id.txEmailAddress)
    lateinit var txEmailAddress: TextView
    @BindView(R.id.llyContact)
    lateinit var llyContact: LinearLayout
    @BindView(R.id.llyLogout)
    lateinit var llyLogout: LinearLayout
    @BindView(R.id.llyMemberShip)
    lateinit var llyMemberShip: LinearLayout
    @BindView(R.id.llyChangePassword)
    lateinit var llyChangePassword: LinearLayout
    @BindView(R.id.llyRefer)
    lateinit var llyRefer: LinearLayout
    @BindView(R.id.llyAboutUs)
    lateinit var llyAboutUs: LinearLayout
    @BindView(R.id.llyBlog)
    lateinit var llyBlog: LinearLayout
    @BindView(R.id.imageIV)
    lateinit var imageIV: ImageView
    @BindView(R.id.llyProfileDetail)
    lateinit var llyProfileDetail:LinearLayout

    lateinit var logOutViewModel: LogOutViewModel
    lateinit var membershipFreeViewModel: MembershipFreeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        ButterKnife.bind(this)
        setData()
        init()
        performPaymentStatusCheck()
    }

    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        membershipFreeViewModel = ViewModelProvider(this, factory).get(MembershipFreeViewModel::class.java)
        logOutViewModel = ViewModelProvider(this, factory).get(LogOutViewModel::class.java)

    }

    //Data set for profile
    private fun setData() {
        txHeadingTV.text = getString(R.string.profile)
        txEmailAddress.text = getEmail()
        txUserName.text = getUserName()
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_audio_thumb)
            .error(R.drawable.ic_audio_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
        Glide
            .with(this)
            .load(getUserPhoto())
            .centerCrop()
            .placeholder(R.drawable.ic_audio_thumb)
            .apply(options)
            .into(imageIV)
    }

    override fun onRestart() {
        super.onRestart()
        setData()
    }

    @OnClick(
        R.id.llyBack,
        R.id.llyContact,
        R.id.llyLogout,
        R.id.llyMemberShip,
        R.id.llyRefer,
        R.id.llyAboutUs,
        R.id.llyBlog,
        R.id.llyProfileDetail,
        R.id.llyChangePassword
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.llyBack -> finish()
            R.id.llyContact -> switchtoContactUs()
            R.id.llyLogout -> logOut()
            R.id.llyMemberShip -> switchToMembershipScreen()
            R.id.llyRefer -> switchToReferAfriend()
            R.id.llyAboutUs -> switchToWebView(txAboutUs.text.toString(), getString(R.string.type_about))
            R.id.llyBlog -> switchToWebView(txBlog.text.toString(), getString(R.string.type_blog))
            R.id.llyProfileDetail -> switchToEditProfile()
            R.id.llyChangePassword ->switchToChangePasswordScreen()
        }
    }



    private fun switchToWebView(headerText: String, valType: String) {
        val intent = Intent(mActivity, WebViewActivity::class.java)
        intent.putExtra("type", headerText)
        intent.putExtra("valType", valType)
        startActivity(intent)
    }

    private fun switchToReferAfriend() {
        var intent = Intent(mActivity, ReferAFriendActivity::class.java)
        startActivity(intent)
    }

    private fun switchToMembershipScreen() {
        var intent = Intent(mActivity, MemberShipCheckActivity::class.java)
        startActivity(intent)
    }

    private fun switchtoContactUs() {
        var intent = Intent(mActivity, ContactUsActivity::class.java)
        startActivity(intent)
    }

    private fun switchToEditProfile() {
        var intent = Intent(mActivity, ProfileDetailsScreen::class.java)
        startActivity(intent)
    }

    private fun switchToChangePasswordScreen() {
        var intent = Intent(mActivity, ChangePasswordActivity::class.java)
        startActivity(intent)
    }

    fun logOut() {
        if (Constants.hasInternetConnection(application as MyApplication)) {
            showLogoutDialog(mActivity)
        } else { showSNACK(llyLogout, getString(R.string.no_internet_connection))
        }
    }


    //  Show Alert Dialog
    fun showLogoutDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert_yes_no)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txNo = alertDialog.findViewById<TextView>(R.id.txNo)
        val txYes = alertDialog.findViewById<TextView>(R.id.txYes)
        txNo.setOnClickListener { alertDialog.dismiss() }
        txYes.setOnClickListener {
            alertDialog.dismiss()
            executeLogOutApi()

        }
        alertDialog.show()
    }


    //API execution
    private fun executeLogOutApi() {
        logOutViewModel.logOut(getAUthToken(),mActivity)
        logOutViewModel.resultResponse.observe(this, androidx.lifecycle.Observer {
            event -> event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { profileF ->
                            if (profileF.status == 1 || profileF.status == 200) {
                                switchTOLogOut()
                            }
                            else if (profileF.status == 301 || profileF.status == 401)
                            {
                                Constants.showAuthDismissDialog(mActivity,getString(R.string.authorization_failure))
                            }
                            else {
                                showSNACK(llyLogout, profileF.message)
                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showSNACK(llyLogout, message)

                        }
                    }

                    is Resource.Loading -> {
                        Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })

    }

    //Logout clear data and switching
    private fun switchTOLogOut() {
        val reEmail = getRemeberEmail()
        val rePassword = getRemeberPassword()
        val profileImage = getUserPhoto()
        val reRememberCre = IsRememberMe()
        val preferences: SharedPreferences = ICMAPrefrences.getPreferences(
            Objects.requireNonNull(mActivity)
        )
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
        ICMAPrefrences.writeString(mActivity, ICMAPrefrences.REMEMBER_EMAIL, reEmail)
        ICMAPrefrences.writeString(mActivity, ICMAPrefrences.REMEMBER_PASSWORD, rePassword)
        ICMAPrefrences.writeBoolean(mActivity, ICMAPrefrences.REMEMBER_CREDENTIALS, reRememberCre)
        ICMAPrefrences.writeString(mActivity, ICMAPrefrences.PHOTO, profileImage)
        val mIntent = Intent(mActivity, LoginActivity::class.java)
        mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(mIntent)
    }



    private fun performPaymentStatusCheck() {
        membershipFreeViewModel.getFreeMeberShipStatus(getAUthToken(),mActivity)
        membershipFreeViewModel.resultResponse.observe(this, androidx.lifecycle.Observer {
                event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { logResponse ->
                            ICMAPrefrences.writeString(mActivity, ICMAPrefrences.FREE_MEMBERSHIP_TEXT, logResponse.message)
                            ICMAPrefrences.writeString(mActivity, ICMAPrefrences.FREE_MEMBERSHIP_STATUS, logResponse.status.toString())
                        }
                    }

                    is Resource.Error -> {
                    }

                    is Resource.Loading -> {
                   }
                }
            }
        })

    }
}