package com.icma.app.activities

import android.net.Uri
import android.os.Bundle
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.icma.app.R
import com.icma.app.utils.DownloadUtil
import kotlinx.android.synthetic.main.activity_audio_new.*
import kotlinx.android.synthetic.main.custom_layout_audio_timebar.*

import com.bumptech.glide.Glide
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.DefaultRenderersFactory.ExtensionRendererMode
import com.google.android.exoplayer2.DefaultLoadControl
import com.icma.app.model.DataItem
import java.io.Serializable


class AudioActivityNew : BaseActivity(),Player.EventListener {


    @BindView(R.id.imVidClose)
    lateinit var imClose: ImageView

    var privious_player: SimpleExoPlayer? = null
   // var audioList = arrayListOf<MutableList<DataItem>>()
    var audioUrl :String=""
    var position: String=""
    var audioThumbnail:String=""
    var title:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTransparentStatusBarOnly(mActivity)
        setContentView(R.layout.activity_audio_new)
        ButterKnife.bind(this)
        getIntentData()
    }

    @OnClick(R.id.imVidClose)
    fun onClick(view:View)
    {
        when(view.id)
        {
            R.id.imVidClose ->ExitScreen()
        }
    }

    private fun ExitScreen() {
        try {
        if (privious_player != null) {
            privious_player!!.playWhenReady = false
        }
    }
    catch (e: Exception)
    {}
        finish()
    }

    private fun getIntentData() {
        if (intent!=null)
        {
            audioUrl = intent.getStringExtra("audioUrl") as String
            title = intent.getStringExtra("title") as String
            audioThumbnail = intent.getStringExtra("audioThumbnail").toString()
            var audio_listq:MutableList<DataItem>
           // audio_listq = intent.getStringArrayListExtra("audioThumbnail")!! as MutableList<DataItem>

        }
        //Set data
        if (!audioThumbnail.equals("")) {
            Glide.with(mActivity).load(audioThumbnail).into(exoaArtwork)
        }
        txHeadingTV.text=title
        setPlayer()
    }

    private fun setPlayer() {
        playerview.visibility= View.VISIBLE
        val player: SimpleExoPlayer
        @ExtensionRendererMode val extensionRendererMode =
            DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
        val loadControl = DefaultLoadControl.Builder()
            .setBufferDurationsMs(25000, 50000, 1500, 2000).createDefaultLoadControl()
        val renderersFactory =
            DefaultRenderersFactory(this).setExtensionRendererMode(extensionRendererMode)
        player =
            SimpleExoPlayer.Builder(this, renderersFactory).setLoadControl(loadControl).build()
        val dataSourceFactory = DefaultDataSourceFactory(
            applicationContext,
            Util.getUserAgent(applicationContext!!, "ICMA")
        )
        val cacheDataSourceFactory =
            CacheDataSourceFactory(DownloadUtil.getCache(applicationContext), dataSourceFactory)
        val videoSource: MediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory)
            .createMediaSource(Uri.parse(audioUrl))

        player.prepare(videoSource,true,true)
        player.repeatMode = Player.REPEAT_MODE_ALL
        player.addListener((this as Player.EventListener))
        val imageViewArtwork = playerview.findViewById<ImageView>(R.id.exoaArtwork)
        playerview.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
        imageViewArtwork.scaleType = ImageView.ScaleType.CENTER_CROP
       // imageViewArtwork.setImageResource(R.drawable.pic6)
        imageViewArtwork.visibility=View.VISIBLE

        playerview.player = player
        player.playWhenReady = true
        privious_player = player
        player.addListener(object : Player.EventListener {
            override fun onTimelineChanged(timeline: Timeline, reason: Int) {
            }

            override fun onTracksChanged(
                trackGroups: TrackGroupArray,
                trackSelections: TrackSelectionArray
            ) {
            }

            override fun onLoadingChanged(isLoading: Boolean) {}
            override fun onPlayerStateChanged(
                playWhenReady: Boolean,
                playbackState: Int
            ) {
                when (playbackState) {

                    Player.STATE_READY ->
                    {
                        progressBarAud.visibility= View.GONE
                    }
                    else -> {
                    }
                }
            }

            override fun onRepeatModeChanged(repeatMode: Int) {}
            override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
            override fun onPlayerError(error: ExoPlaybackException) {}
            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}
            override fun onSeekProcessed() {}
        })


         playerview.setOnTouchListener(object : View.OnTouchListener {
            private val gestureDetector =
                GestureDetector(applicationContext, object : GestureDetector.SimpleOnGestureListener() {
                    override fun onFling(
                        e1: MotionEvent,
                        e2: MotionEvent,
                        velocityX: Float,
                        velocityY: Float
                    ): Boolean {
                        super.onFling(e1, e2, velocityX, velocityY)
                        val deltaX = e1.x - e2.x
                        val deltaXAbs = Math.abs(deltaX)
                        // Only when swipe distance between minimal and maximal distance value then we treat it as effective swipe
                        if (deltaXAbs > 100 && deltaXAbs < 1000) {
                            if (deltaX > 0) {
                                //  OpenProfile(item,true);
                            }
                        }
                        return true
                    }

                    override fun onSingleTapUp(e: MotionEvent): Boolean {
                        super.onSingleTapUp(e)
                       playerview.visibility=View.VISIBLE
                        return true
                    }

                    override fun onLongPress(e: MotionEvent) {
                        super.onLongPress(e)
                    }

                    override fun onDoubleTap(e: MotionEvent): Boolean {
                        playerview.visibility=View.VISIBLE
                        return super.onDoubleTap(e)
                    }
                })

            override fun onTouch(v: View, event: MotionEvent): Boolean {
                gestureDetector.onTouchEvent(event)
                return true
            }
        })
    }


    override fun onBackPressed() {
        ExitScreen()
        super.onBackPressed()
    }
}