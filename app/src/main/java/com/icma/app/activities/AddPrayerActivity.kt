package com.icma.app.activities

import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.icma.app.R

import android.content.Intent
import android.util.Log
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.utils.Constants
import com.icma.app.utils.Resource
import com.icma.app.dataobject.RequestBodies
import com.icma.app.viewmodel.AddPrayerViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.fragment_prayer.*
import com.icma.app.fragments.Home5Fragment


class AddPrayerActivity : BaseActivity() {
    @BindView(R.id.llyBack)
    lateinit var llyBack:LinearLayout
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV:TextView
    @BindView(R.id.llyFirstName)
    lateinit var llyFirstName:LinearLayout
    @BindView(R.id.edFirstName)
    lateinit var edFirstName:EditText
    @BindView(R.id.llyPrayerDetail)
    lateinit var llyPrayerDetail:LinearLayout
    @BindView(R.id.llyPrayerTitle)
    lateinit var llyPrayerTitle:LinearLayout
    @BindView(R.id.edPrayerDetail)
    lateinit var edPrayerDetail:EditText
    @BindView(R.id.edPrayerTitle)
    lateinit var edPrayerTitle:EditText
    @BindView(R.id.txSubmitTV)
    lateinit var txSubmitTV:TextView

    lateinit var addPrayerViewModel: AddPrayerViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_prayer)
        ButterKnife.bind(this)
        setIntentData()
        performClicks()
        init()
    }

    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        addPrayerViewModel = ViewModelProvider(this, factory).get(AddPrayerViewModel::class.java)
    }

    //Set INtent data
    private fun setIntentData() {
        val ss = SpannableString(getString(R.string.note_prayer1) +
                getString(R.string.note_prayer2))
        var style:StyleSpan= StyleSpan(Typeface.BOLD)
        ss.setSpan(style, 0, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        txNoteTV.setText(ss)
    }

    //Click Listeners
    @OnClick(R.id.llyBack,R.id.txSubmitTV)
    fun onClick(view:View)
    {
        when(view.id)
        {
            R.id.llyBack   ->finish()
            R.id.txSubmitTV->executeApi()
        }
    }

    //API Execution
    private fun executeApi() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.no_internet_connection))
            } else {
                executePrayerApi()
            }
    }

    private fun executePrayerApi() {
        var name = edFirstName.text.toString().trim()
        var title = edPrayerTitle.text.toString().trim()
        var detail = edPrayerDetail.text.toString().trim()
        val body = RequestBodies.PrayerBody(
            name,
            title,
            detail
        )
        Log.e("Check",body.toString())
        addPrayerViewModel.prayerGetData(getAUthToken(),body,mActivity)
        addPrayerViewModel.prayerResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { prayerResponseF ->

                            if (prayerResponseF.status==1||prayerResponseF.status==200) {
                                showToast(mActivity,prayerResponseF.message)
                                val intent = Intent(this@AddPrayerActivity, Home5Fragment::class.java)
                                intent.putExtra(Constants.MODEL,prayerResponseF.data)
                                setResult(RESULT_OK, intent)
                                finish()
                            }
                            else{
                                showSNACK(txSubmitTV,prayerResponseF.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showSNACK(txSubmitTV,message)

                        }
                    }

                    is Resource.Loading -> {
                        Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })
    }

    //Clicks listeners
    private fun performClicks() {

        edFirstName.addTextChangedListener {
            firstNameSet()
        }
        edPrayerDetail.addTextChangedListener {
            prayerDetailSet()
        }
        edPrayerTitle.addTextChangedListener {
            prayerTitleSet()
        }
    }


    //functions for view  chnage
    private fun firstNameSet() {
        llyFirstName.setBackgroundResource(R.drawable.bg_et)
        llyPrayerDetail.setBackgroundResource(R.drawable.bg_et_grey)
        llyPrayerTitle.setBackgroundResource(R.drawable.bg_et_grey)
    }

    private fun prayerDetailSet() {
        llyFirstName.setBackgroundResource(R.drawable.bg_et_grey)
        llyPrayerDetail.setBackgroundResource(R.drawable.bg_et)
        llyPrayerTitle.setBackgroundResource(R.drawable.bg_et_grey)    }

    private fun prayerTitleSet() {
        llyFirstName.setBackgroundResource(R.drawable.bg_et_grey)
        llyPrayerDetail.setBackgroundResource(R.drawable.bg_et_grey)
        llyPrayerTitle.setBackgroundResource(R.drawable.bg_et)    }


    /*
* Set up validations
* */
    fun isValidate(): Boolean {
        var flag = true
        if (edFirstName.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edFirstName,getString(R.string.please_enter_firstname))
            flag = false
        } else if (edPrayerTitle.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edLastName,getString(R.string.enter_prayer_title))
            flag = false
        }  else if (edPrayerDetail.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edEmail,getString(R.string.enter_prayer_detail))
            flag = false
        }
        return flag
    }
}