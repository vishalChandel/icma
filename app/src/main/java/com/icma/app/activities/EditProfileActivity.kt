package com.icma.app.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.dhaval2404.imagepicker.ImagePicker
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.ICMAPrefrences
import com.icma.app.utils.Resource
import com.icma.app.dataobject.RequestBodies
import com.icma.app.viewmodel.EditProfileViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import com.makeramen.roundedimageview.RoundedImageView
import kotlinx.android.synthetic.main.activity_edit_profile.*
import java.io.ByteArrayOutputStream
import java.io.IOException

class EditProfileActivity : BaseActivity() {

    @BindView(R.id.txSaveTv)
    lateinit var txSaveTv: TextView
    @BindView(R.id.rlyImage)
    lateinit var rlyImage: RelativeLayout
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV:TextView
    @BindView(R.id.llyBack)
    lateinit var llyBack:LinearLayout
    @BindView(R.id.imageIV)
    lateinit var imageIV:RoundedImageView
    @BindView(R.id.password_toggle_si)
    lateinit var passwordToggle: ImageView

    var isChecked = true
    lateinit var editprofileViewModel: EditProfileViewModel


    /*
 * Initialize Menifest Permissions:
 * & Camera Gallery Request @params
 * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    var writeCamera = Manifest.permission.CAMERA
    var mBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        ButterKnife.bind(this)
        setHeaderText()
        performClicks()
        init()

    }




    //header Data set
    private fun setHeaderText() {
        txHeadingTV.setText(getString(R.string.edit_profile))
        edFirstName.setText(getFirstName())
        edLastName.setText(getLastName())
        txEmail.setText(getEmail())
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_audio_thumb)
            .error(R.drawable.ic_audio_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
           Glide
            .with(this)
            .load(getUserPhoto())
            .centerCrop()
            .placeholder(R.drawable.ic_audio_thumb)
            .apply(options)
            .into(imageIV)
             Log.e("Check::",getLoggedInUserID()+"::auth::"+getAUthToken()+"::shhd::"+getUserName())
    }

    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        editprofileViewModel = ViewModelProvider(this, factory).get(EditProfileViewModel::class.java)
    }

    //Clicks handeled
    @OnClick(R.id.rlyImage, R.id.txSaveTv, R.id.llyBack)
     fun onClick(view: View) {
        when (view.id) {
            R.id.rlyImage -> pickImage()
            R.id.txSaveTv -> performEditProfile()
            R.id.llyBack ->  finish()

        }
    }

    //Image Picker
    private fun pickImage() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    //Api hit
    private  fun performEditProfile() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.no_internet_connection))
            } else {
                var firstName = edFirstName.text.toString().trim()
                val lastName = edLastName.text.toString().trim()
                val email=txEmail.text.toString().trim()
                val password=getPassword()
                val phone=""
                val body=RequestBodies.EditProfileBody(firstName,lastName,email,password,phone,mBitmap)
                executeApi(body)
            }
    }


    private  fun executeApi(body: RequestBodies.EditProfileBody) {
        editprofileViewModel.editUser(getAUthToken(),body,mActivity)
        editprofileViewModel.loginResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { loginResponse ->
                            if (loginResponse.status==1||loginResponse.status==200) {
                                ICMAPrefrences.writeBoolean(mActivity, ICMAPrefrences.ISLOGIN, true)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.ID, loginResponse.data?.userid)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.FIRSTNAME, loginResponse.data?.firstname)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.LASTNAME, loginResponse.data?.lastname)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.NAME, loginResponse.data?.firstname+" "+loginResponse.data?.lastname)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.EMAIL, loginResponse.data?.email)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.AUTHTOKEN, loginResponse.data?.authtoken)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.PHOTO, loginResponse.data?.profileimage)
                                showToast(mActivity,getString(R.string.profile_updated))
                                loginResponse.data?.toString()?.let { Log.e("Checklopop", it) }
                                finish()
                            }else
                            {
                                loginResponse.message?.let { showSNACK(txSaveTv, it) }
                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showSNACK(txSaveTv,message)
                        }
                    }

                    is Resource.Loading -> {
                        Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })

    }


    /*
* Set up validations for Log In fields
* */
    fun isValidate(): Boolean {
        var flag = true
        if (edFirstName.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edFirstName,getString(R.string.please_enter_firstname))
            flag = false
        } else if (edLastName.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edLastName,getString(R.string.please_enter_lastname))
            flag = false
        }
        return flag
    }


    //Clicks listeners
    private fun performClicks() {

        edFirstName.addTextChangedListener {
            firstNameSet()
        }
        edLastName.addTextChangedListener {
            lastNameset()
        }


    }


    //IMAGE PICKER
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }


    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()
            //Crop image(Optional), Check Customization for more option
            .compress(80)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                600,
                600
            )
            .cropSquare()
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            try {
               val uri: Uri = data?.data!!
                val imageStream = contentResolver.openInputStream(uri)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
                val out = ByteArrayOutputStream()
                selectedImage.compress(Bitmap.CompressFormat.PNG, 100, out)
                mBitmap = selectedImage
                val options: RequestOptions = RequestOptions()
                    .placeholder(R.drawable.ic_audio_thumb)
                    .error(R.drawable.ic_audio_thumb)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform()
                Glide
                    .with(this)
                    .load(mBitmap)
                    .centerCrop()
                    .placeholder(R.drawable.ic_audio_thumb)
                    .apply(options)
                    .into(imageIV)


            } catch (e: IOException) {
                Log.e(TAG, "****Error****" + e.printStackTrace())
                e.printStackTrace()
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
             showToast(mActivity,ImagePicker.getError(data))
        } else {
            showToast(mActivity,getString(R.string.task_cancelled))
        }
    }

    //CLICK handled for views

    fun firstNameSet()
    {
        llyFirstName.setBackgroundResource(R.drawable.bg_et)
        llyLastName.setBackgroundResource(R.drawable.bg_et_grey)
        llyEmail.setBackgroundResource(R.drawable.bg_et_grey)
  }

    private fun lastNameset() {
        llyFirstName.setBackgroundResource(R.drawable.bg_et_grey)
        llyLastName.setBackgroundResource(R.drawable.bg_et)
        llyEmail.setBackgroundResource(R.drawable.bg_et_grey)
    }





}