package com.icma.app.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import coil.api.clear
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.ICMAPrefrences
import com.icma.app.utils.Resource
import com.icma.app.viewmodel.LoginViewModel
import com.icma.app.viewmodel.PaymentStatusViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : BaseActivity() {

    lateinit var paymentStatusViewModel: PaymentStatusViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        hideBottomNavigationbar()
        init()
        setData()
    }

    //Repository set
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        paymentStatusViewModel = ViewModelProvider(this, factory).get(PaymentStatusViewModel::class.java)
    }

    //Set Data
    private fun setData() {
        if (isNetworkAvailable(mActivity)&& isUserLogin()) {
            Constants.SPLASH_TIME_OUT=500L
            performPaymentStatusCheck(getAUthToken()) }
        else
        { showSplash()
        }
    }

    //Splash check
    private fun showSplash() {
        Constants.SPLASH_TIME_OUT=1500L
        try {
            imgLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bounce));

            val mThread = object : Thread() {
                override fun run() {
                    sleep(Constants.SPLASH_TIME_OUT)
                if (isUserLogin()) {
                    val i = Intent(mActivity, HomeActivity::class.java)
                    startActivity(i)
                    finish()

                }
                else {
                    imgLogo.clear()

                    val i = Intent(mActivity, LoginActivity::class.java)
                    startActivity(i)
                    finish()
                     }
                }
            }
            mThread.start()
        } catch (e: Exception)
        {
            e.printStackTrace()
        }

    }

    //Membership Check
    private fun performPaymentStatusCheck(authtoken: String) {
        imgLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bounce));
        paymentStatusViewModel.executePaymentStatus(authtoken,mActivity)
        paymentStatusViewModel.resultResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { loginResponse ->
                            if (loginResponse.status==1||loginResponse.status==2||loginResponse.status==200) {
                                Handler(Looper.getMainLooper()).postDelayed({
                                    Intent(this@SplashActivity, HomeActivity::class.java).also { startActivity(it) }
                                    finish()
                                },100)

                            }else
                            {
                                Handler(Looper.getMainLooper()).postDelayed({
                                    Intent(this@SplashActivity, LoginActivity::class.java).also { startActivity(it) }
                                    finish()
                                },100)


                            }
                        }
                    }

                    is Resource.Error -> {
                        imgLogo.clear()
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            //  showAlertDialog(mActivity,message)
                            showSplash()
                        }
                    }

                    is Resource.Loading -> {
                        // Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })
    }

}