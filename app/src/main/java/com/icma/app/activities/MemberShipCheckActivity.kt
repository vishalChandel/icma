package com.icma.app.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.icma.app.R
import kotlinx.android.synthetic.main.activity_profile.*

class MemberShipCheckActivity : BaseActivity() {
    @BindView(R.id.llyBack)
    lateinit var llyBack: LinearLayout
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV: TextView
    @BindView(R.id.txMembershipHead)
    lateinit var txMembershipHead: TextView
    @BindView(R.id.txStatusText)
    lateinit var txStatusText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_ship_check)
        ButterKnife.bind(this)
        setData()
    }

    private fun setData() {
        txHeadingTV.text = getString(R.string.membership)
        txMembershipHead.text = getFreeMembershipText()
        if (getMemberHipFreeStatus().equals("1"))
        {
            txStatusText.visibility=View.VISIBLE
        }
        else
        {
            txStatusText.visibility=View.GONE

        }
    }

    @OnClick(R.id.llyBack)
    fun onClick(view: View) {
        when (view.id) {
            R.id.llyBack -> finish()
        }
    }



}