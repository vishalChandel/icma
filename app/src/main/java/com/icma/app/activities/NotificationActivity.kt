package com.icma.app.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Resource
import com.icma.app.adapters.NotificationAdapter
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.DataS
import com.icma.app.model.NotificationModel
import com.icma.app.viewmodel.NotificationViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory

class NotificationActivity : BaseActivity() {
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV: TextView
    @BindView(R.id.llyBack)
    lateinit var llyBack: LinearLayout
    @BindView(R.id.txNoData)
    lateinit var txNoData: TextView
    @BindView(R.id.recyclerNotifications)
    lateinit var recyclerNotifications: RecyclerView

    lateinit var notificationViewModel: NotificationViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        ButterKnife.bind(this)
        setHeaderText()
        init()
        executeApi()

    }

    //header Data set
    private fun setHeaderText() {
        txHeadingTV.setText(getString(R.string.nottifications))
    }

    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        notificationViewModel = ViewModelProvider(this, factory).get(NotificationViewModel::class.java)
    }

    @OnClick(R.id.llyBack)
    fun onCLick(view: View)
    {
      when(view.id)
      {
          R.id.llyBack->finish()
      }
    }

    //Execute api
    private fun executeApi() {
        var perPage = "100"
        var pageNo="1"

        val body = RequestBodies.Home5Body(
            perPage,
            pageNo
        )
        notificationViewModel.getNotifications(getAUthToken(),body,mActivity)
        notificationViewModel.resultResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { homeResponse ->
                            if (homeResponse.status==1||homeResponse.status==200) {
                                recyclerNotifications.visibility=View.VISIBLE
                                txNoData.visibility=View.GONE
                                setAdapter(homeResponse)
                            }
                            else{
                                recyclerNotifications.visibility=View.GONE
                                txNoData.visibility=View.VISIBLE
                               //homeResponse.message?.let { showSNACK(recyclerNotifications, it) }
                            }
                        }
                    }

                    is Resource.Error -> {
                        response.message?.let { message -> showSNACK(recyclerNotifications,message)
                        }
                    }

                    is Resource.Loading -> {
                        //  mShimmerViewContainer!!.visibility=View.VISIBLE
                    }
                }
            }
        })
    }

    //adapter set
    private fun setAdapter(response: NotificationModel) {
        recyclerNotifications.layoutManager = LinearLayoutManager(mActivity)
        var data_list = mutableListOf<DataS>()
        data_list.addAll(response.data)
        val adapter = NotificationAdapter(data_list)
        Log.e("TEXT",data_list.toString())
        recyclerNotifications.adapter = adapter
        }
    }
