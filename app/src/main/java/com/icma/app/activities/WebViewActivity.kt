package com.icma.app.activities

import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.icma.app.R
import com.icma.app.utils.Constants
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : BaseActivity() {
    @BindView(R.id.llyBack)
    lateinit var llyBack:LinearLayout
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        ButterKnife.bind(this)
        setData()
    }

    //Clicks handled
    @OnClick(R.id.llyBack)
    fun onClick(view: View)
    {
        when(view.id)
        {
            R.id.llyBack->finish()
        }
    }

    private fun setData() {
        var typeSet=""
        if (intent!=null)
        {
            txHeadingTV.text=intent.getStringExtra("type")
            typeSet= intent.getStringExtra("valType")!!
       }
//        val webSettings: WebSettings = webView.getSettings()
//        webSettings.javaScriptEnabled = true
//        webView.setWebChromeClient(WebChromeClient())
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webView.webChromeClient = WebChromeClient()
        if (typeSet.equals(getString(R.string.terms_type))) {
            webView.loadUrl(Constants.TERMS_AND_CONDITIONS)
        }
        else if (typeSet.equals(getString(R.string.policy_type)))
        {
            webView.loadUrl(Constants.PRIVACY_POLICY_LINK)

        }
        else if (typeSet.equals(getString(R.string.type_blog)))
        {
            webView.loadUrl(Constants.BLOGS_LINK)

        } else if (typeSet.equals(getString(R.string.type_about)))
        {
            webView.loadUrl(Constants.ABOUT_US_LINK)

        }
        else if (typeSet.equals(getString(R.string.link_open)))
        {
            webView.loadUrl(getString(R.string.devotional_link))

        }


    }
    

}