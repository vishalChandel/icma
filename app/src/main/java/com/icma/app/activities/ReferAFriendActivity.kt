package com.icma.app.activities

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.icma.app.R
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap


import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.lang.Exception
import androidx.core.content.FileProvider
import com.facebook.share.model.ShareHashtag

import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog

import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.share.Sharer




class ReferAFriendActivity : BaseActivity() {
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV: TextView
    @BindView(R.id.llyBack)
    lateinit var llyBack: LinearLayout
    @BindView(R.id.txSendInvite)
    lateinit var txSendInvite:TextView
    @BindView(R.id.txReferralCode)
    lateinit var txReferralCode:TextView

    lateinit var callbackManager: CallbackManager
    lateinit var shareDialog: ShareDialog

    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_refer_afriend)
        ButterKnife.bind(this)
        setData()

        callbackManager = CallbackManager.Factory.create();
        shareDialog = ShareDialog(this);
//        // this part is optional
//        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {  });

//        var callbackManager: CallbackManager
//        callbackManager = CallbackManager.Factory.create();
//        var shareDialog: ShareDialog
        // this part is optional

    }

    private fun setData() {
        txHeadingTV.text=getString(R.string.refer_a_friend)
        txReferralCode.text=getReferralCode()
    }

    @OnClick(R.id.llyBack,R.id.txSendInvite)
    fun onClick(view: View)
    {
        when(view.id)
        {
            R.id.llyBack->finish()
            R.id.txSendInvite ->sharereferCodeDialog()
        }
    }

    private fun shareInvite() {
        showSNACK(txSendInvite,getString(R.string.coming_soon))

    }

    private fun sharereferCodeDialog()
    {
        val bottomSheetDialog = mActivity?.let { BottomSheetDialog(it) }

        val view: View = LayoutInflater.from(mActivity)
            .inflate(R.layout.dilaog_share, null)
        bottomSheetDialog!!.setContentView(view)
        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)

        bottomSheetDialog.show()
        //fetching ids
        val llyTwitter = view.findViewById<LinearLayout>(R.id.llyTwitter)
        val llyInsta = view.findViewById<LinearLayout>(R.id.llyInsta)
        val llyFb = view.findViewById<LinearLayout>(R.id.llyFb)
        val llyWhatsapp = view.findViewById<LinearLayout>(R.id.llyWhatsapp)

        //Click Listeners
        llyTwitter.setOnClickListener {
            bottomSheetDialog.dismiss()
            if (checkPermission()) {
                shareCode("Twitter", "com.twitter")
            }else
            {
                requestPermission()
            }
        }
        llyFb.setOnClickListener {
            bottomSheetDialog.dismiss()
            if (checkPermission()) {

                showSNACK(txSendInvite,getString(R.string.coming_soon))


//                val shareHashTag = ShareHashtag.Builder().setHashtag("#YOUR_HASHTAG").build()
//                val shareLinkContent = ShareLinkContent.Builder()
//                    .setShareHashtag(shareHashTag)
//                    .setQuote("Your Description")
//                    .setContentUrl(Uri.parse("image or logo [if playstore or app store url then no need of this image url]"))
//                    .build()
//                shareDialog.show(shareLinkContent)
            }else
            {
                requestPermission()
            }
        }
        llyWhatsapp.setOnClickListener {
            bottomSheetDialog.dismiss()
            if (checkPermission()) {
                shareCode("WhatsApp", "com.whatsapp")
            }else
            {
                requestPermission()
            }
        }
        llyInsta.setOnClickListener {
            bottomSheetDialog.dismiss()
            if (checkPermission()) {
                shareCode("Instagram", "com.instagram.android")
            }else
            {
                requestPermission()
            }
        }
    }

    fun shareCode(typeShare:String,packageName:String)
    {
        var shareIntent = Intent()
        val bitmap = BitmapFactory.decodeResource(resources, com.icma.app.R.drawable.ic_share_pic)
        var path: String =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                .toString() + "/Share.png"
        var out: OutputStream? = null
        val file = File(path)
        try {
            out = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val bmpUri = FileProvider.getUriForFile(
            mActivity,
            mActivity.getApplicationContext().getPackageName().toString() + ".provider",
            file
        )
        shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri)
        shareIntent.setPackage(packageName)
        var til:String;

        shareIntent.putExtra(Intent.EXTRA_TEXT, "Use code ${getReferralCode()} on signup for to get extra benefits of app \n" +
                "Regards by :${getUserName()}\nICMA")

        if (typeShare.equals("WhatsApp")) {
            shareIntent.type = "image/*"
        }
        else
        {
            shareIntent.type = "text/plain"
        }
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        try {
            startActivity(
                Intent
                    .createChooser(shareIntent, mActivity.getString(R.string.share_intent_title))
            )

        } catch (ex: ActivityNotFoundException) {
            showToast(mActivity,"$typeShare have not been installed.")
        }
    }

    //IMAGE PICKER
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage), 369
        )
    }

}