package com.icma.app.activities

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.SeekBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContentProviderCompat.requireContext
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.icma.app.R
import com.icma.app.model.DataItem
import java.lang.Exception

class AudioActivity : BaseActivity(), MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener {

    //Play Related
    @BindView(R.id.imBack)
    lateinit var imBack:ImageView
    @BindView(R.id.imForward)
    lateinit var imForward:ImageView
    @BindView(R.id.imPlay)
    lateinit var imPlay:ImageView
    @BindView(R.id.imPause)
    lateinit var imPause:ImageView
    @BindView(R.id.songProgressBar)
    lateinit var songProgressBar:SeekBar
    @BindView(R.id.txStartTimeTV)
    lateinit var txStartTimeTV:TextView
    @BindView(R.id.txEndTimeTV)
    lateinit var txEndTimeTV:TextView
    @BindView(R.id.progressBar)
    lateinit var progressBar:ProgressBar

    @BindView(R.id.imAudioThumbnail)
    lateinit var imAudioThumbnail:ImageView
    @BindView(R.id.imClose)
    lateinit var imClose:ImageView
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV:TextView



    var audioList = arrayListOf<MutableList<DataItem>>()
    var audioUrl :String=""
    var position: String=""
    var audioThumbnail:String=""
    var title:String=""



    //MediaPlayer
    private var mp: MediaPlayer?=null
    private val myhandler = Handler()
    var isPlayerPlaying=true
    var checkk=true
    private val seekForwardTime = 5000
    private val seekBackwardTime = 5000
    private var oldmp: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio)
        setTransparentStatusBarOnly(mActivity)
        ButterKnife.bind(this)
        pauseOtherAudio()
        mp = MediaPlayer()
        mp!!.setOnCompletionListener(this)
        songProgressBar!!.setOnSeekBarChangeListener(this)
        getIntentData()


    }



    //Intent Data fetch
    private fun getIntentData() {
        if (intent!=null) {
            audioList = intent.getSerializableExtra("audioList") as ArrayList<MutableList<DataItem>>
            audioUrl = intent.getStringExtra("audioUrl") as String
//            position = intent.getStringExtra("position") as String
            title = intent.getStringExtra("title") as String
            audioThumbnail = intent.getStringExtra("audioThumbnail") as String
        }
        //Set data

        mActivity?.let { Glide.with(it).load(audioThumbnail).into(imAudioThumbnail) }
        txHeadingTV.text=title
        playAudio()

    }


    //CLICK LISTENERS
    @OnClick(R.id.imPlay,R.id.imPause,R.id.imBack,R.id.imForward,R.id.imClose)
    fun onClick(view:View)
    {
        when(view.id)
        {
            R.id.imPlay ->playAudio()
            R.id.imPause ->PauseAudio()
            R.id.imBack ->BackwardAudio()
            R.id.imForward ->ForwardAudio()
            R.id.imClose ->exitScreen()
        }
    }

    override fun onBackPressed() {
        exitScreen()
        super.onBackPressed()
    }

    //AUDIO CLICKS HANDLED
    private fun playAudio() {
        progressBar.visibility = View.VISIBLE
        if (isPlayerPlaying) {
                mp!!.reset()
                mp!!.setDataSource(audioUrl)
                if (checkk) {
                    mp!!.prepareAsync()
                        mp!!.setOnPreparedListener {
                            it!!.start()
                            progressBar.visibility=View.GONE
                            if(songProgressBar!=null) {
                                songProgressBar!!.progress = 0
                                songProgressBar!!.max = 100
                            }
                            updateProgressBar()
                            checkk = false;
                        }
                }
                else
                {
                    mp!!.prepare()
                    mp!!.setOnPreparedListener {
                        it!!.start()
                        progressBar.visibility=View.GONE
                        songProgressBar!!.progress = 0
                        songProgressBar!!.max = 100
                        updateProgressBar()
                        checkk = false;
                    }
                }
                isPlayerPlaying = false

                imPause.visibility = View.VISIBLE
                imPlay.visibility = View.GONE
                oldmp = mp


        } else {

                if (mp!=null) {
                    progressBar.visibility=View.GONE
                    mp!!.start()
                    imPause.visibility = View.VISIBLE
                    imPlay.visibility = View.GONE
                }
        }
    }
    private fun PauseAudio() {
        if (mp != null&&mp!!.isPlaying)
        {
            mp!!.pause()
            imPause.visibility=View.GONE
            imPlay.visibility=View.VISIBLE
        }

    }


    private fun ForwardAudio() {

        val currentPosition = mp!!.currentPosition
        if (currentPosition + seekForwardTime <= mp!!.duration) {
            mp!!.seekTo(currentPosition + seekForwardTime)
        } else mp!!.seekTo(mp!!.duration)
    }


    private fun BackwardAudio() {
        val currentPosition = mp!!.currentPosition
        if (currentPosition + seekBackwardTime >= 0) {
            mp!!.seekTo(currentPosition - seekBackwardTime)
        } else mp!!.seekTo(mp!!.duration)

    }



    private fun updateProgressBar() {
        myhandler.postDelayed(UpdateTimeTask, 2000)
    }

    private val UpdateTimeTask: Runnable = object : Runnable {
        override fun run() {
            try {
                val totalduration = mp!!.duration.toLong()
                val currentduration = mp!!.currentPosition.toLong()
                Log.e("Checkuio", totalduration.toString())
                Log.e("Checkuio", currentduration.toString())
                txEndTimeTV!!.setText("" + milliSecondstoTimer(totalduration))
                txStartTimeTV!!.setText("" + milliSecondstoTimer(currentduration))
                songProgressBar!!.setProgress(
                    getProgressPercentage(
                        currentduration,
                        totalduration
                    )
                )
                myhandler.postDelayed(this, 100)
            }
            catch (e: Exception)
            {}

        }
    }
    override fun onCompletion(mp: MediaPlayer?) {
        Pause_Privious_Player()

    }



    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        myhandler.removeCallbacks(UpdateTimeTask)
        val totalduration = mp!!.duration
        val currentposition = progressToTimer(seekBar!!.progress, totalduration)
        mp!!.seekTo(currentposition)
        updateProgressBar()
    }

    fun milliSecondstoTimer(milliseconds: Long): String? {
        var finalTimerString = ""
        var secondsString = ""
        var minuteString = ""
        val hours = (milliseconds / (1000 * 60 * 60)).toInt()
        val minutes = (milliseconds % (1000 * 60 * 60)).toInt() / (1000 * 60)
        val seconds = (milliseconds % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()
        if (hours > 0) {
            finalTimerString =if (hours < 10) {
                "0$hours:"
            } else "" + hours
        }
        secondsString = if (seconds < 10) {
            "0$seconds"
        } else "" + seconds

        minuteString = if (minutes < 10) {
            "0$minutes"
        } else "" + minutes
        finalTimerString = "$finalTimerString$minuteString:$secondsString"
        return finalTimerString
    }
    fun getProgressPercentage(currentduartion: Long, totalduration: Long): Int {
        var percentage = 0.toDouble()
        val currentseconds = currentduartion.toInt() / 1000.toLong()
        val totalseconds = totalduration.toInt() / 1000.toLong()
        percentage = currentseconds.toDouble() / totalseconds * 100
        return percentage.toInt()
    }
    fun progressToTimer(progress: Int, totalduration: Int): Int {
        var totalduration = totalduration
        var currentduration = 0
        totalduration = (totalduration / 1000)
        currentduration = (progress.toDouble() / 100 * totalduration).toInt()
        return currentduration * 1000
    }


    fun Pause_Privious_Player() {
        if (mp != null) {
            imPlay.visibility=View.VISIBLE
            imPause.visibility=View.GONE
            oldmp!!.pause()
            mp!!.pause()
            mp!!.seekTo(0)
        }

        isPlayerPlaying=true
    }


    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {
    }




    //EXIT SCREEN
    fun exitScreen()
    {
        Pause_Privious_Player()
        mp!!.release()
        finish()
    }


    //Pause other audio while call or open player
    fun pauseOtherAudio()
    {
        val ama = mActivity.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        // Request audio focus for playback
        val result = ama.requestAudioFocus(
            focusChangeListener,  // Use the music stream.
            AudioManager.STREAM_MUSIC,  // Request permanent focus.
            AudioManager.AUDIOFOCUS_GAIN
        )
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            // other app had stopped playing song now , so u can do u stuff now .
        }

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m =
                    StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    private val focusChangeListener =
        AudioManager.OnAudioFocusChangeListener { focusChange ->
            try {
                val mediaPlayerBackground = MediaPlayer()
                val am = getSystemService(AUDIO_SERVICE) as AudioManager
                when (focusChange) {
                    AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK ->                                 // Lower the volume while ducking.
                    {
                        mediaPlayerBackground.setVolume(0.2f, 0.2f)
                        Pause_Privious_Player()
                    }
                    AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                        mediaPlayerBackground.stop()
                        Pause_Privious_Player()
                    }
                    AudioManager.AUDIOFOCUS_LOSS -> {
                        mediaPlayerBackground.stop()
                        Pause_Privious_Player()
                    }
                    AudioManager.AUDIOFOCUS_GAIN -> {
                        // Return the volume to normal and resume if paused.
                        mediaPlayerBackground.setVolume(1f, 1f)
                        mediaPlayerBackground.start()
                    }
                    else -> {
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
}