package com.icma.app.activities

import android.content.Intent
import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import com.icma.app.R
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up.imChecked
import kotlinx.android.synthetic.main.activity_sign_up.llyEmail
import kotlinx.android.synthetic.main.activity_sign_up.llyPassword
import android.text.Html
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.utils.Constants
import com.icma.app.utils.ICMAPrefrences
import com.icma.app.utils.Resource
import com.icma.app.viewmodel.SignUpViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import com.icma.app.dataobject.RequestBodies
import kotlinx.android.synthetic.main.activity_login.*


class SignUpActivity : BaseActivity() {
    var checkedRemember=false

    @BindView(R.id.txLoginTV)
    lateinit var txLoginTV:TextView
    @BindView(R.id.txSignUp)
    lateinit var txSignUp:TextView
    @BindView(R.id.password_toggle_si)
    lateinit var passwordToggle: ImageView
    @BindView(R.id.txTermsAndConditons)
    lateinit var txTermsAndConditons:TextView


    lateinit var signUpViewModel: SignUpViewModel
    var isChecked = true
    var deviceTokenUser:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        ButterKnife.bind(this)
        performClicks()
        setPolicyText()
        getDeviceTokenUser()
        init()
    }


    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        signUpViewModel = ViewModelProvider(this, factory).get(SignUpViewModel::class.java)
    }

    //Clicks handled
    @OnClick(R.id.txLoginTV,R.id.txSignUp, R.id.llyTermsConditions,R.id.password_toggle_si,R.id.txTermsAndConditons)
    fun onClick(view: View)
   {
    when(view.id)
    {
        R.id.txLoginTV -> finish()
        R.id.txSignUp ->performSignUp()
        R.id.llyTermsConditions ->performTermsClick()
        R.id.password_toggle_si ->performPasswordSeenFun()
        R.id.txTermsAndConditons ->performTermsCOnditonSwitch()
    }
}

    //Radio butoon click handled
    private fun performPasswordSeenFun() {
        if (!isChecked) {
            // show password
            edPassword.transformationMethod = PasswordTransformationMethod.getInstance()
            edPassword.setSelection(edPassword.text.length)
            passwordToggle!!.setImageResource(R.drawable.ic_eye_close)
            isChecked = true
        } else {
            // hide password
            edPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            edPassword.setSelection(edPassword.text.length)
            passwordToggle!!.setImageResource(R.drawable.ic_eye_open)
            isChecked = false
        }
    }



    //Check signup
    private fun performSignUp() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.no_internet_connection))
            } else {
                executeSignUpApi()
            }

    }



    //Set Terms & Policy Text
    private fun setPolicyText() {
        val text =
            "<font color=#7E000000>" + getString(R.string.agree_with) + " " + "</font> <font color=#9E32F5>" + getString(
                R.string.terms
            ) + "</font>"
        txTermsAndConditons.setText(Html.fromHtml(text))
    }

    //switchto Terms and Conditons
    fun performTermsCOnditonSwitch()
    {
        val intent=Intent(mActivity,WebViewActivity::class.java)
        intent.putExtra("type",getString(R.string.terms))
        intent.putExtra("valType",getString(R.string.terms_type))
        startActivity(intent)
    }

    //Clicks listeners
    private fun performClicks() {

        edFirstName.addTextChangedListener {
           firstNameSet()
        }
        edLastName.addTextChangedListener {
            lastNameset()
        }
        edEmail.addTextChangedListener {
            emailSet()
        }
        edPassword.addTextChangedListener {
            passwordSet()
        }
        edReferralCode.addTextChangedListener {
            referralCodeSet()
        }
    }

    //Api execution
    private fun executeSignUpApi() {
        var firstName = edFirstName.text.toString().trim()
        var lastName = edLastName.text.toString().trim()
        var email = edEmail.text.toString().trim()
        val password = edPassword.text.toString().trim()
        val body = RequestBodies.SignUpBody(firstName, lastName, email, password,edReferralCode.text.toString().trim() )
        signUpViewModel.signUpUser(body)
        signUpViewModel.signUpResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { signupResponse ->
                            if (signupResponse.status==1||signupResponse.status==200) {
                                showDismissDialog(mActivity,signupResponse.message)
                            }
                            else{
                                showSNACK(txSignUp,signupResponse.message)
                            } }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                          showSNACK(txSignUp,message) }
                    }

                    is Resource.Loading -> {
                        Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })
    }


    //CLICK handled for views

    fun firstNameSet()
    {
        llyFirstName.setBackgroundResource(R.drawable.bg_et)
        llyLastName.setBackgroundResource(R.drawable.bg_et_grey)
        llyEmail.setBackgroundResource(R.drawable.bg_et_grey)
        llyReferralCode.setBackgroundResource(R.drawable.bg_et_grey)
        llyPassword.setBackgroundResource(R.drawable.bg_et_grey)
    }

    private fun lastNameset() {
        llyFirstName.setBackgroundResource(R.drawable.bg_et_grey)
        llyLastName.setBackgroundResource(R.drawable.bg_et)
        llyEmail.setBackgroundResource(R.drawable.bg_et_grey)
        llyReferralCode.setBackgroundResource(R.drawable.bg_et_grey)
        llyPassword.setBackgroundResource(R.drawable.bg_et_grey)
    }

    private fun emailSet() {
        llyFirstName.setBackgroundResource(R.drawable.bg_et_grey)
        llyLastName.setBackgroundResource(R.drawable.bg_et_grey)
        llyEmail.setBackgroundResource(R.drawable.bg_et)
        llyReferralCode.setBackgroundResource(R.drawable.bg_et_grey)
        llyPassword.setBackgroundResource(R.drawable.bg_et_grey)
    }

    private fun passwordSet() {
        llyFirstName.setBackgroundResource(R.drawable.bg_et_grey)
        llyLastName.setBackgroundResource(R.drawable.bg_et_grey)
        llyEmail.setBackgroundResource(R.drawable.bg_et_grey)
        llyPassword.setBackgroundResource(R.drawable.bg_et)
        llyReferralCode.setBackgroundResource(R.drawable.bg_et_grey)}

    private fun referralCodeSet()
    {
        llyFirstName.setBackgroundResource(R.drawable.bg_et_grey)
        llyLastName.setBackgroundResource(R.drawable.bg_et_grey)
        llyEmail.setBackgroundResource(R.drawable.bg_et_grey)
        llyPassword.setBackgroundResource(R.drawable.bg_et_grey)
        llyReferralCode.setBackgroundResource(R.drawable.bg_et)
    }

    //CheckBox click
    private fun performTermsClick() {
        if (!checkedRemember) {
            checkedRemember=true
            imChecked!!.setImageResource(R.drawable.remember_check_enabled)
        }
        else
        {
            checkedRemember=false
            imChecked!!.setImageResource(R.drawable.remember_check_disabled)

        }
    }



    /*
* Set up validations for Log In fields
* */
    fun isValidate(): Boolean {
        var flag = true
        if (edFirstName.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edFirstName,getString(R.string.please_enter_firstname))
            flag = false
        } else if (edLastName.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edLastName,getString(R.string.please_enter_lastname))
            flag = false
        }  else if (edEmail.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edEmail,getString(R.string.please_enter_your_email))
            flag = false
        } else if (!isValidEmaillId(edEmail.text.toString().trim { it <= ' ' })) {
            showSNACK(edEmail,getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (edPassword.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edPassword,getString(R.string.please_enter_password))
            flag = false
        } else if (edPassword.text.toString().length < 6) {
           showSNACK(edPassword,getString(R.string.please_enter_minimum_6_))
            flag = false
        }
        else if (checkedRemember == false) {
            showSNACK(imChecked,getString(R.string.please_accept))
            flag = false
        }
        return flag
    }

    //Device token
    fun getDeviceTokenUser() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener {
            if (it.isSuccessful) {
                deviceTokenUser = it.result!!
                Log.e("ICMA", "Device token ::"+deviceTokenUser)
            } else {
                Log.e("ICMA", "Device token fetch issue")
                return@OnCompleteListener
            }
        })

    }

}