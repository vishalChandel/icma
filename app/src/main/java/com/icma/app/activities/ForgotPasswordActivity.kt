package com.icma.app.activities

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Resource
import com.icma.app.viewmodel.ForgotPasswordViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import com.icma.app.dataobject.RequestBodies
import kotlinx.android.synthetic.main.activity_forgot_password.editEmailET

class ForgotPasswordActivity : BaseActivity() {

    @BindView(R.id.txSubmitTV)
    lateinit var txSubmitTV:TextView
    @BindView(R.id.llyBack)
    lateinit var llyBack:LinearLayout


    lateinit var forgotPasswordViewModel: ForgotPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        ButterKnife.bind(this)
        init()
    }

    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        forgotPasswordViewModel = ViewModelProvider(this, factory).get(ForgotPasswordViewModel::class.java)
    }


    @OnClick(R.id.txSubmitTV,R.id.llyBack)
    fun onCLick(view: View)
    {
        when(view.id)
        {
            R.id.txSubmitTV -> performSubmitData()
            R.id.llyBack ->finish()
        }
    }

    private fun performSubmitData() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.no_internet_connection))
            } else {
                performSubmit()
            }
    }

    private fun performSubmit() {
        var email = editEmailET.text.toString().trim()

        val body = RequestBodies.ForgotPswdBody(
            email
        )

        forgotPasswordViewModel.forgotPswdUser(body)
        forgotPasswordViewModel.forgotPswdResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { forgotResponse ->
                            if (forgotResponse.status==1||forgotResponse.status==200) {
                                editEmailET.setText("")
                                showDismissDialog(mActivity, forgotResponse.message)
                            }
                            else{
                               showSNACK(txSubmitTV,forgotResponse.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showSNACK(txSubmitTV,message)
                        }
                    }

                    is Resource.Loading -> {
                        Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })

    }


    /*
* Set up validations for Log In fields
* */
    fun isValidate(): Boolean {
        var flag = true
        if (editEmailET.text.toString().trim { it <= ' ' } == "") {
            showSNACK(editEmailET,getString(R.string.please_enter_your_email))
            flag = false
        } else if (!isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' })) {
            showSNACK(editEmailET,getString(R.string.please_enter_valid_email_address))
            flag = false
        }
        return flag
    }

}