package com.icma.app.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.icma.app.R
import kotlinx.android.synthetic.main.activity_profile_details_screen.*

class ProfileDetailsScreen : BaseActivity() {
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV:TextView
    @BindView(R.id.llyBack)
    lateinit var llyBack:LinearLayout
    @BindView(R.id.llyEdit)
    lateinit var llyEdit:LinearLayout
    @BindView(R.id.txStartDate)
    lateinit var txStartDate:TextView
    @BindView(R.id.txExpirationDate)
    lateinit var txExpirationDate:TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_details_screen)
        ButterKnife.bind(this)
        setToolbarData()
        setScreenDetails()
    }

    private fun setScreenDetails() {
        txFirstName.setText(getFirstName())
        txLastName.setText(getLastName())
        txEmail.setText(getEmail())
        txPassword.text=getPassword()
        txExpirationDate.text=getExpiartionDate()
        txStartDate.text=getStartDate()
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_audio_thumb)
            .error(R.drawable.ic_audio_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
        Glide
            .with(this)
            .load(getUserPhoto())
            .centerCrop()
            .placeholder(R.drawable.ic_audio_thumb)
            .apply(options)
            .into(imageIV)
    }

    //toolbar data
    private fun setToolbarData() {
       txHeadingTV.setText(getString(R.string.profile))
       llyEdit.visibility= View.VISIBLE
    }

    //Clck bindview
    @OnClick(R.id.llyBack,R.id.llyEdit)
    fun onClick(view:View)
    {
        when(view.id)
        {
            R.id.llyBack->finish()
            R.id.llyEdit ->switchtoEditProfile()
        }
    }

    override fun onRestart() {
        super.onRestart()
        setScreenDetails()
    }

    private fun switchtoEditProfile() {
        val intent=Intent(mActivity, EditProfileActivity::class.java)
        startActivity(intent)
       // showToast(mActivity,getString(R.string.coming_soon))
    }
}