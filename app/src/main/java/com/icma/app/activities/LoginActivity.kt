package com.icma.app.activities

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.ICMAPrefrences
import com.icma.app.utils.Resource
import com.icma.app.viewmodel.LoginViewModel

import com.icma.app.viewmodel.ViewModelProviderFactory
import com.icma.app.dataobject.RequestBodies
import kotlinx.android.synthetic.main.activity_login.*


import com.icma.app.viewmodel.PaymentStatusViewModel
import java.text.SimpleDateFormat
import java.util.*


class LoginActivity : BaseActivity() {

    @BindView(R.id.txtDontHaveAccountTV)
    lateinit var txtDontHaveAccountTV:TextView
    @BindView(R.id.txtForgotPassTV)
    lateinit var txtForgotPassTV:TextView
    @BindView(R.id.llyRememberMe)
    lateinit var llyRememberMe:LinearLayout
    @BindView(R.id.txtLoginTV)
    lateinit var txtLoginTV:TextView

    @BindView(R.id.password_toggle_si)
    lateinit var passwordToggle: ImageView


    var checkedRemember=false
    var isChecked = true
    var deviceTokenUser:String=""
    lateinit var loginViewModel: LoginViewModel
    lateinit var paymentStatusViewModel: PaymentStatusViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)
        performClicks()
        setEditTextFocused(mActivity, editEmailET)
        getDeviceTokenUser()
        setData()
        init()
    }



    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        loginViewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        paymentStatusViewModel = ViewModelProvider(this, factory).get(PaymentStatusViewModel::class.java)
    }

    @OnClick(R.id.txtDontHaveAccountTV,R.id.txtForgotPassTV,R.id.llyRememberMe,R.id.txtLoginTV,R.id.password_toggle_si)
    fun onClick(view:View)
    {
        when(view.id)
        {
            R.id.txtDontHaveAccountTV ->performDontHaveAccount()
            R.id.txtForgotPassTV -> forgotPswdClick()
            R.id.llyRememberMe ->performRemebermeClick()
            R.id.txtLoginTV ->perfromLoginClick()
            R.id.password_toggle_si ->performPasswordSeenFun()
        }
    }

    //Function clicks
    private fun performPasswordSeenFun() {
        if (!isChecked) {
            // show password
            editPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()
            editPasswordET.setSelection(editPasswordET.text.length)
            passwordToggle!!.setImageResource(R.drawable.ic_eye_close)
            isChecked = true
       } else {
            // hide password
            editPasswordET.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editPasswordET.setSelection(editPasswordET.text.length)
            passwordToggle!!.setImageResource(R.drawable.ic_eye_open)
            isChecked = false
        }
    }


    //Click Handled of views
    private fun performDontHaveAccount() {
        val i = Intent(mActivity, SignUpActivity::class.java)
        startActivity(i)
    }

    private fun forgotPswdClick() {
        val i = Intent(mActivity, ForgotPasswordActivity::class.java)
        startActivity(i)
    }

    private fun perfromLoginClick() {
        if (isValidate())
        {
            if (!isNetworkAvailable(mActivity)) {
                showToast(mActivity, getString(R.string.no_internet_connection))
            } else {
                executeSignInApi()
            }
        }
    }

    //Editext focus listeners
    private fun performClicks() {
        //Email Field Click
        editEmailET.addTextChangedListener {
            performllyemailClick()
        }
        //Password Field Click
        editPasswordET!!.addTextChangedListener {
            performllyPasswordClick()
        }

    }

    //Remember me
    private fun setData() {
        if (IsRememberMe()) {
            checkedRemember=true
            imChecked!!.setImageResource(R.drawable.remember_check_enabled)
            editEmailET.setText(getRemeberEmail())
            getRemeberEmail()?.let { editEmailET.setSelection(it.length) }
            editPasswordET.setText(getRemeberPassword())
            getRemeberPassword()?.let { editPasswordET.setSelection(it.length) }
        } else {
            checkedRemember=false
            imChecked!!.setImageResource(R.drawable.remember_check_disabled)        }
    }


    //API HIT
    private fun executeSignInApi() {
        var email = editEmailET.text.toString().trim()
        val password = editPasswordET.text.toString().trim()
            val body = RequestBodies.LoginBody(
                email,
                password
            )
          loginViewModel.loginUser(mActivity,body)
          loginViewModel.loginResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        response.data?.let { loginResponse ->
                            if (loginResponse.status==1||loginResponse.status==200) {
                                 performPaymentStatusCheck(loginResponse.data.authtoken)
                            }else
                            { Constants.dismissProgressDialog()
                              showSNACK(txtLoginTV, loginResponse.message.toString())
                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showSNACK(txtLoginTV,message)
                        }
                    }

                    is Resource.Loading -> {
                       Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })
    }

    private fun performPaymentStatusCheck(authtoken: String) {
        paymentStatusViewModel.executePaymentStatus(authtoken,mActivity)
        paymentStatusViewModel.resultResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { loginResponse ->
                            Log.e("Check",""+loginResponse.data)
                            if (loginResponse.status==1||loginResponse.status==2||loginResponse.status==200) {
                                var start_date:String=""
                                var expiration_date:String=""
                                if (!loginResponse.data.startDate.equals(""))
                                {
                                    start_date=timeChange(loginResponse.data.startDate.toString().toLong())
                                }
                                if (!loginResponse.data.endDate.equals(""))
                                {
                                    expiration_date=timeChange(loginResponse.data.endDate.toString().toLong())
                                }
                                ICMAPrefrences.writeBoolean(mActivity, ICMAPrefrences.ISLOGIN, true)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.ID, loginResponse.data.userid)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.FIRSTNAME, loginResponse.data.firstname)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.LASTNAME, loginResponse.data.lastname)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.NAME, loginResponse.data.firstname+" "+loginResponse.data.lastname)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.EMAIL, loginResponse.data.email)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.AUTHTOKEN, loginResponse.data.authtoken)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.PHOTO, loginResponse.data.profileimage)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.PASSWORD, editPasswordET.text.toString())
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.DEVICETOKEN, deviceTokenUser)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.DEVICETYPE, "2")
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.REFERRALCODE, loginResponse.data.referralCode)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.REFERRALCOUNT, loginResponse.data.referralCount)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.START_DATE, start_date)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.EXPIRATION_DATE, expiration_date)
                                Intent(this@LoginActivity, HomeActivity::class.java).also { startActivity(it) }
                                finish()
                            }else
                            {
                                Intent(this@LoginActivity, MembershipActiivty::class.java).also { startActivity(it) }
                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showSNACK(txtLoginTV,message)
                        }
                    }

                    is Resource.Loading -> {
                    }
                }
            }
        })
    }


    //Device token
     fun getDeviceTokenUser() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener {
            if (it.isSuccessful) {
                deviceTokenUser = it.result!!
                Log.e("ICMA", "Device token ::"+deviceTokenUser)
            } else {
                Log.e("ICMA", "Device token fetch issue")
                return@OnCompleteListener
            }
        })

    }

    /*
* Set up validations for Log In fields
* */
    fun isValidate(): Boolean {
        var flag = true
        if (editEmailET.text.toString().trim { it <= ' ' } == "") {
            showSNACK(editEmailET,getString(R.string.please_enter_your_email))
            flag = false
        } else if (!isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' })) {
            showSNACK(editEmailET,getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (editPasswordET.text.toString().trim { it <= ' ' } == "") {
            showSNACK(editPasswordET,getString(R.string.please_enter_password))
            flag = false
        }
        return flag
    }

    private fun performRemebermeClick() {
        if (!checkedRemember) {
            checkedRemember=true
            imChecked!!.setImageResource(R.drawable.remember_check_enabled)
            ICMAPrefrences.writeBoolean(mActivity, ICMAPrefrences.REMEMBER_CREDENTIALS, true)
            ICMAPrefrences.writeString(mActivity, ICMAPrefrences.REMEMBER_EMAIL, editEmailET.text.toString().trim { it <= ' ' })
            ICMAPrefrences.writeString(mActivity, ICMAPrefrences.REMEMBER_PASSWORD, editPasswordET.text.toString().trim { it <= ' ' })
        }
        else
        {
            checkedRemember=false
            imChecked!!.setImageResource(R.drawable.remember_check_disabled)
            ICMAPrefrences.writeBoolean(mActivity, ICMAPrefrences.REMEMBER_CREDENTIALS, false)
            ICMAPrefrences.writeString(mActivity, ICMAPrefrences.REMEMBER_EMAIL, "")
            ICMAPrefrences.writeString(mActivity,ICMAPrefrences.REMEMBER_PASSWORD, "")

        }
    }


    private fun performllyPasswordClick() {
        llyPassword!!.setBackgroundResource(R.drawable.bg_et)
        llyEmail.setBackgroundResource(R.drawable.bg_et_grey)
    }

    private fun performllyemailClick() {
        llyPassword!!.setBackgroundResource(R.drawable.bg_et_grey)
        llyEmail.setBackgroundResource(R.drawable.bg_et)

    }



}