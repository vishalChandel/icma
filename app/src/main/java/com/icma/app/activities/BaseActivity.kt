package com.icma.app.activities

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.icma.app.R
import com.icma.app.utils.ICMAPrefrences
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


open class BaseActivity : AppCompatActivity() {

    //  Get Class Name
    var TAG = this@BaseActivity.javaClass.simpleName

    //  Initialize Activity
    var mActivity: Activity = this@BaseActivity

    //  Initialize Objects
    var progressDialog: Dialog? = null

    //  Finishes Activity
    override fun finish() {
        super.finish()
        overridePendingTransitionSlideDownExit()
    }

    //  Start Activity
    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
        overridePendingTransitionSlideUPEnter()
    }

    //  Overrides the pending Activity transition by performing the "Bottom Up" animation.
    fun overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0)
    }

    //  Overrides the pending Activity transition by performing the "Bottom Down" animation.
    fun overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down)
    }

    //  Clear EditText Focus
    fun setEditTextFocused(mActivity: Activity, mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm = v.context
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }



    //  Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        val view: View = LayoutInflater.from(mActivity)
            .inflate(com.icma.app.R.layout.toast_layout, null)
        val tvMessage = view.findViewById<TextView>(com.icma.app.R.id.tvMessage)
        tvMessage.setText(strMessage)
        val toast = Toast(mActivity)
        toast.setView(view);
        toast.show();
    }

    //  Email Address Validation
    fun isValidEmaillId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


    //  To Check Internet Connections
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    //  Get UserId
    fun getLoggedInUserID(): String {
        return ICMAPrefrences.readString(mActivity, ICMAPrefrences.USERID, "")!!
    }

    //    Get UserName
    fun getUserName(): String {
        return ICMAPrefrences.readString(mActivity, ICMAPrefrences.NAME, "")!!
    }


    //  Get Email
    fun getEmail(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.EMAIL, "")!!
    }


    //  Get AuthToken
    fun getAUthToken(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.AUTHTOKEN, "")!!
    }

    //  Get First Name
    fun getFirstName(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.FIRSTNAME, "")!!
    }

    //  Get Last Name
    fun getLastName(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.LASTNAME, "")!!
    }

    //
    fun isUserLogin(): Boolean {
        return ICMAPrefrences.readBoolean(mActivity,  ICMAPrefrences.ISLOGIN,false)!!
    }

    fun getPassword(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.PASSWORD,"")!!
    }

    fun getUserPhoto(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.PHOTO,"")!!
    }

    fun getDeviceToken(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.DEVICETOKEN,"")!!
    }

    fun getDeviceType(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.DEVICETYPE,"")!!
    }

    fun getReferralCode(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.REFERRALCODE,"")!!
    }

    fun getReferralCount(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.REFERRALCODE,"")!!
    }

    fun getStartDate(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.START_DATE,"")!!
    }

    fun getExpiartionDate(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.EXPIRATION_DATE,"")!!
    }

    fun getFreeMembershipText(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.FREE_MEMBERSHIP_TEXT,"")!!
    }

    fun getMemberHipFreeStatus(): String {
        return ICMAPrefrences.readString(mActivity,  ICMAPrefrences.FREE_MEMBERSHIP_STATUS,"")!!
    }
    /*
     * Get Remember Credentials
     * */
    open fun IsRememberMe(): Boolean {
        return ICMAPrefrences.readBoolean(mActivity, ICMAPrefrences.REMEMBER_CREDENTIALS, false)
    }
    /*
     * Get Remember Email
     * */
    open fun getRemeberEmail(): String? {
        return ICMAPrefrences.readString(mActivity, ICMAPrefrences.REMEMBER_EMAIL, "")
    }

    /*
     * Get Remember Password
     * */
    open fun getRemeberPassword(): String? {
        return ICMAPrefrences.readString(mActivity, ICMAPrefrences.REMEMBER_PASSWORD, "")
    }

    //    Switch between fragments
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.container, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }


    //Hide KeyBoard
    fun hideSoftKeyBoard(context: Context, view: View) {
        try {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {
            e.printStackTrace()
        }



    }

    //Hide NavigationBar
    fun hideBottomNavigationbar() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

    }

    //Transparent Status bar
    open fun setTransparentStatusBarOnly(activity: Activity) {
        window.statusBarColor = ContextCompat.getColor(mActivity, com.icma.app.R.color.colorBlack)
        activity.window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        activity.window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.window.statusBarColor = Color.TRANSPARENT
        }
        // this lines ensure only the status-bar to become transparent without affecting the nav-bar
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

      val decorView = window.decorView //set status background black
        decorView.systemUiVisibility =
            decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv() //set status text  light

    }



    //  Show Alert Dialog
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.txOk)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }


    //  Show Alert Dialog
    fun showDismissDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.txOk)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
        mActivity.finish()}
        alertDialog.show()
    }



    //  Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)

        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    //  Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }
    fun showSnackBar(view: View) {
        val snack = Snackbar.make(view.rootView, "Sample snack bar message", Snackbar.LENGTH_INDEFINITE)
        snack.setAction("Click Me") {
           snack.dismiss()
        }
        snack.show()
    }

    fun showSNACK(view: View,message:String){

        var AC:String
        AC = "DISMISS"
        var snackbar:Snackbar?=null
         snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
            .setAction(AC,View.OnClickListener {snackbar!!.dismiss()})

        snackbar.setActionTextColor(Color.WHITE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(resources.getColor(R.color.app_color))
        val textView = snackbarView.findViewById(R.id.snackbar_text) as TextView
        val actionTextView = snackbarView.findViewById(R.id.snackbar_action)as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 15f
        actionTextView.textSize = 13f

        snackbar.show()
    }


    fun showDismissSNACK(view: View,message:String){

        var AC:String
        AC = "DISMISS"
        var snackbar:Snackbar?=null
        snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(AC,View.OnClickListener {snackbar!!.dismiss()
            finish()
            })

        snackbar.setActionTextColor(Color.WHITE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(resources.getColor(R.color.app_color))
        val textView = snackbarView.findViewById(R.id.snackbar_text) as TextView
        val actionTextView = snackbarView.findViewById(R.id.snackbar_action)as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 15f
        actionTextView.textSize = 13f
        snackbar.show()
    }


    fun editTextSignupSelector(mEditText: EditText, rl: LinearLayout, tag: String?) {
        mEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                rl.setBackgroundResource(R.drawable.bg_et_grey)
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (s.length > 0) {
                    rl.setBackgroundResource(R.drawable.bg_et)
                } else {
                    rl.setBackgroundResource(R.drawable.bg_et)
                }
            }
        })
    }



    //close keyboard
    fun closeKeyBoard()
    {
        this.currentFocus?.let { view ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    //card date format
    fun timeChange(coment_time: Long):String {
        var formattedDate:String=""
        val date = Date(coment_time * 1000L)
        val sdf = SimpleDateFormat( "MMMM d, yyyy")
        sdf.timeZone = TimeZone.getDefault()
        formattedDate = sdf.format(date)
        return  formattedDate
    }

    //Button Timer
    fun buttonTimer(viewC:View)
    {
        viewC.isEnabled = false;
        Handler(Looper.getMainLooper()).postDelayed({ viewC!!.isEnabled = true }, 1500)
    }
}