package com.icma.app.activities

import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.exoplayer2.offline.DownloadCursor
import com.google.android.exoplayer2.util.Util
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Resource
import com.icma.app.adapters.SearchScreenAdapter
import com.icma.app.app.MyApplication
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.DataS
import com.icma.app.viewmodel.SearchViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import org.json.JSONObject

class SearchScreenActivity : BaseActivity() {
    @BindView(R.id.edSearch)
    lateinit var edSearch:EditText
    @BindView(R.id.llyCancel)
    lateinit var llyCancel:LinearLayout
    @BindView(R.id.recyclerSearch)
    lateinit var recyclerSearch:RecyclerView
    @BindView(R.id.txCancelClear)
    lateinit var txCancelClear:TextView
    @BindView(R.id.shimmerViewContainer)
    lateinit var shimmerViewContainer:ShimmerFrameLayout
    @BindView(R.id.txNoData)
    lateinit var txNoData:TextView
    @BindView(R.id.txNoDataFound)
    lateinit var txNoDataFound:TextView
    @BindView(R.id.rlyMain)
    lateinit var rlyMain:RelativeLayout

    var canExit=true
    lateinit var searchViewModel: SearchViewModel
    var data_list = mutableListOf<DataS>()
    lateinit var adapter:SearchScreenAdapter
    var finalExit=false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_screen)
        ButterKnife.bind(this)
        sesrchTextFunc()
        init()
        executeApi()

    }

    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        searchViewModel = ViewModelProvider(this, factory).get(SearchViewModel::class.java)
    }


    //Click handled
    @OnClick(R.id.llyCancel)
    fun onClick(view: View)
    {
       when(view.id) {
           R.id.llyCancel ->cancelExitScreen()
       }


    }

    //Exit screen
    private fun cancelExitScreen() {
        if (canExit)
        {
            finalExit=true
            finish()

        }
        else
        {
            executeApi()
            canExit=true
            closeKeyBoard()

        }


    }


    //text write check
    private fun sesrchTextFunc() {
        edSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                txNoData.visibility=View.GONE
                recyclerSearch.visibility=View.GONE
            }
            override fun afterTextChanged(editable: Editable) {
                val string = editable.trim().toString()
                if (string.length>0)
                {
                    finalExit=false
                    txCancelClear.setText(getString(R.string.search))
                    canExit=false
                    if (data_list.size > 1) {
                        adapter.filter!!.filter(string)
                        recyclerSearch.visibility=View.VISIBLE
                    }
                }

                else
                {
                    finalExit=false
                    txCancelClear.setText(getString(R.string.cancel))
                    canExit=true
                    recyclerSearch.visibility=View.VISIBLE
                    executeApi()
                }
            }
        })
    }

    private fun executeApi() {
        txNoData.visibility=View.GONE
            if (!isNetworkAvailable(mActivity)) {
                showSNACK(txCancelClear, getString(R.string.no_internet_connection))
            } else {
                executeSearchApi()
            }
    }

    private fun executeSearchApi() {
        var title = edSearch.text.toString().trim()
        var perPage = "100"
        var pageNo = "1"
        val body = RequestBodies.SearchBody(
            title,
            perPage,
            pageNo
        )
        searchViewModel.searchData(getAUthToken(),body,mActivity)
        searchViewModel.resultResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { response ->

                            if (response.status==1||response.status==200) {
                                txNoDataFound.visibility=View.GONE
                                rlyMain.visibility=View.VISIBLE
                               setAdapter(response.data)
                            }
                            else{
                                rlyMain.visibility=View.GONE
                                if (data_list!=null&&data_list.size>0)
                                {
                                data_list.clear()
                                }
                                txNoDataFound.visibility=View.VISIBLE
                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showSNACK(txCancelClear,message)

                        }
                    }

                    is Resource.Loading -> {
                       // Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })
    }

    private fun setAdapter(data: List<DataS>) {
       data_list.clear()
        for (i in 0..data.size - 1) {
            if (data.get(i)!!.type!!.equals(1)) {
                data.get(i)!!.viewType = Constants.VIDEO_VIEW_HOLDER
            }
            else if (data.get(i)!!.type!!.equals(2)) {
                data.get(i)!!.viewType = Constants.AUDIO_VIEW_HOLDER
            }
        }
        data_list.addAll(data)
        startDownloadedItems()
        recyclerSearch.layoutManager = LinearLayoutManager(mActivity)
        adapter = SearchScreenAdapter(mActivity,data_list,shimmerViewContainer,txNoData)
        recyclerSearch.adapter = adapter

    }

    private fun startDownloadedItems(){  //to resume download
    //    mActivity?.let { it1 -> DownloadService.sendResumeDownloads(it1, MyDownloadService::class.java, true) }

        val downloadedTracks = mutableListOf<String>()
        for (i in 0..data_list.size - 1) {
            Constants.downloadMedia(mActivity, Uri.parse(data_list.get(i).link))
           // Log.e("Check","))))uri::"+data_list.get(i).link)
            Uri.parse(data_list.get(i).link)
           val downloadCursor: DownloadCursor =
                (mActivity?.applicationContext as MyApplication).appContainer.downloadManager.downloadIndex.getDownloads()
            if (downloadCursor.moveToFirst()) {
                do {
                    val jsonString = Util.fromUtf8Bytes(downloadCursor.download.request.data)
                    val jsonObject = JSONObject(jsonString)
                    val uri = downloadCursor.download.request.uri
                    Log.e("Check","uri::"+uri)
                    downloadedTracks.add(
                        uri.toString()
                        )
                } while (downloadCursor.moveToNext())
            }
      }

    }
}