package com.icma.app.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Pair
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.exoplayer2.*
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick

import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.ads.AdsLoader
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.*
import com.icma.app.R
import com.icma.app.app.CoroutineDispatchers
import com.icma.app.app.MyApplication

import kotlinx.android.synthetic.main.activity_video.*
import kotlinx.android.synthetic.main.custom_layout_video_timebar.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import java.util.ArrayList
import kotlin.coroutines.CoroutineContext

interface Action

class VideoActivity : BaseActivity(), Player.EventListener
{

    var privious_player: ExoPlayer? = null
    var link:String=""
    @BindView(R.id.imVidClose)
    lateinit var imClose: ImageView




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTransparentStatusBarOnly(mActivity)
        setContentView(R.layout.activity_video)
        ButterKnife.bind(this)

        getIntentData()
    }


    @OnClick(R.id.imVidClose)
    fun onClick(view:View)
    {
        when(view.id)
        {
            R.id.imVidClose ->ExitScreen()
        }
    }

    private fun ExitScreen() {
        try {
        if (privious_player != null) {
            privious_player!!.playWhenReady = false
        }
    }
    catch (e: Exception)
    {}
        finish()
    }

    private fun getIntentData() {
        if (intent!=null)
        {
          link=intent.getStringExtra("videoURL")  as String
        }
        setPlayer()
    }

    private fun setPlayer() {
        playerview.visibility= View.VISIBLE
        val player: ExoPlayer
        @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode =
            DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
        val loadControl = DefaultLoadControl.Builder()
            .setBufferDurationsMs(25000, 50000, 100, 300).createDefaultLoadControl()
        val renderersFactory =
            DefaultRenderersFactory(this).setExtensionRendererMode(extensionRendererMode)
        player =
            SimpleExoPlayer.Builder(this, renderersFactory).setLoadControl(loadControl).build()
//        val dataSourceFactory = DefaultDataSourceFactory(
//            applicationContext,
//            Util.getUserAgent(applicationContext!!, "ICMA")
//        )
//        val cacheDataSourceFactory =
//            CacheDataSourceFactory(DownloadUtil.getCache(applicationContext), dataSourceFactory)
//        val videoSource: MediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory)
//            .createMediaSource(Uri.parse(link))
        val dataSourceFactory = DefaultDataSourceFactory(mActivity, Util.getUserAgent(mActivity, getString(R.string.app_name)))
        val cachedDataSourceFactory = CacheDataSourceFactory((application as MyApplication).appContainer.downloadCache, dataSourceFactory)
        val mediaSources = ProgressiveMediaSource.Factory(cachedDataSourceFactory).createMediaSource(
            Uri.parse(link))
        player.prepare(mediaSources)
        player.repeatMode = Player.REPEAT_MODE_ALL
        player.addListener((this as Player.EventListener))

        playerview.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FILL
        //  player.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
        playerview.player = player
        player.playWhenReady = true
        privious_player = player
        player.addListener(object : Player.EventListener {
            override fun onTimelineChanged(timeline: Timeline, reason: Int) {
            }

            override fun onTracksChanged(
                trackGroups: TrackGroupArray,
                trackSelections: TrackSelectionArray
            ) {
            }

            override fun onLoadingChanged(isLoading: Boolean) {}
            override fun onPlayerStateChanged(
                playWhenReady: Boolean,
                playbackState: Int
            ) {
                when (playbackState) {

                    Player.STATE_READY ->
                    {
                        progressBarVid.visibility= View.GONE
                    }
                    else -> {
                    }
                }
            }

            override fun onRepeatModeChanged(repeatMode: Int) {}
            override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}
            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {}
            override fun onSeekProcessed() {}
        })


        //New CODE


    }


    override fun onBackPressed() {
        ExitScreen()
        super.onBackPressed()
    }



}
