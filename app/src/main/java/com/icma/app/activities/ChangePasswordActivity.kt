package com.icma.app.activities

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.ICMAPrefrences
import com.icma.app.utils.Resource
import com.icma.app.dataobject.RequestBodies
import com.icma.app.viewmodel.ChangePasswordViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory

class ChangePasswordActivity : BaseActivity() {

    @BindView(R.id.llyBack)
    lateinit var llyBack: LinearLayout
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV: TextView
    @BindView(R.id.editPasswordOldET)
    lateinit var editPasswordOldET: EditText
    @BindView(R.id.llyPasswordOld)
    lateinit var llyPasswordOld:LinearLayout
    @BindView(R.id.editPasswordNewET)
    lateinit var editPasswordNewET: EditText
    @BindView(R.id.llyPasswordNew)
    lateinit var llyPasswordNew:LinearLayout
    @BindView(R.id.editPasswordConfirmET)
    lateinit var editPasswordConfirmET: EditText
    @BindView(R.id.llyPasswordConfirm)
    lateinit var llyPasswordConfirm:LinearLayout
    @BindView(R.id.txSubmitTV)
    lateinit var txSubmitTV:TextView
    @BindView(R.id.password_toggle_new)
    lateinit var password_toggle_new: ImageView
    @BindView(R.id.password_toggle_old)
    lateinit var password_toggle_old: ImageView
    @BindView(R.id.password_toggle_confirm)
    lateinit var password_toggle_confirm: ImageView

    var passwordChekOld=true
    var passwordChekNew=true
    var passwordChekConfirm=true

    lateinit var changePasswordViewModel: ChangePasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        ButterKnife.bind(this)
        txHeadingTV.setText(getString(R.string.change_password))
        performClicks()
        init()

    }

    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        changePasswordViewModel = ViewModelProvider(this, factory).get(ChangePasswordViewModel::class.java)
    }

    //Click Handled
    @OnClick(R.id.llyBack,R.id.txSubmitTV,R.id.password_toggle_new,R.id.password_toggle_old,R.id.password_toggle_confirm)
    fun onCLick(view: View)
    {
        when(view.id)
        {
            R.id.llyBack->finish()
            R.id.txSubmitTV ->executeApi()
            R.id.password_toggle_new -> performPasswordSeenFunNew()
            R.id.password_toggle_old -> performPasswordSeenFunOld()
            R.id.password_toggle_confirm -> performPasswordSeenConfirm()
        }
    }


    private fun executeApi() {
        closeKeyBoard()
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showSNACK(txSubmitTV, getString(R.string.no_internet_connection))
            } else {
                exceuteChnagePswdApi()
            }
    }

    private fun exceuteChnagePswdApi() {
        var old_password = editPasswordOldET.text.toString().trim()
        var new_password = editPasswordConfirmET.text.toString().trim()
        val body = RequestBodies.ChangePswdBody(old_password, new_password)
        changePasswordViewModel.changePswd(getAUthToken(),body,mActivity)
        changePasswordViewModel.resultResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { response ->

                            if (response.status==1||response.status==200) {
                                showToast(mActivity,response.message)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.REMEMBER_PASSWORD, new_password)
                                finish()
                            }
                            else{
                                showSNACK(txSubmitTV,response.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showSNACK(txSubmitTV,message)

                        }
                    }

                    is Resource.Loading -> {
                        Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })
    }


    //Clicks listeners
    private fun performClicks() {

        editPasswordOldET.addTextChangedListener {
            oldPasswordSet()
        }
        editPasswordNewET.addTextChangedListener {
            newPasswordSet()
        }
        editPasswordConfirmET.addTextChangedListener {
            confirmPasswordSet()
        }
    }




    private fun oldPasswordSet() {
        llyPasswordOld.setBackgroundResource(R.drawable.bg_et)
        llyPasswordNew.setBackgroundResource(R.drawable.bg_et_grey)
        llyPasswordConfirm.setBackgroundResource(R.drawable.bg_et_grey)
    }
    private fun newPasswordSet() {
        llyPasswordOld.setBackgroundResource(R.drawable.bg_et_grey)
        llyPasswordNew.setBackgroundResource(R.drawable.bg_et)
        llyPasswordConfirm.setBackgroundResource(R.drawable.bg_et_grey)
    }
    private fun confirmPasswordSet() {
        llyPasswordOld.setBackgroundResource(R.drawable.bg_et_grey)
        llyPasswordNew.setBackgroundResource(R.drawable.bg_et_grey)
        llyPasswordConfirm.setBackgroundResource(R.drawable.bg_et)
    }



    /*
set up validations
 */
    fun isValidate(): Boolean {
        var flag = true
        if (editPasswordOldET.getText().toString().trim { it <= ' ' } == "") {
            showSNACK(txSubmitTV, getString(R.string.please_enter_old_password))
            flag = false
        } else if (editPasswordNewET.getText().toString().trim { it <= ' ' } == "") {
            showSNACK(txSubmitTV, getString(R.string.please_enter_new_password))
            flag = false
        } else if (editPasswordConfirmET.getText().toString().trim { it <= ' ' } == "") {
            showSNACK(txSubmitTV, getString(R.string.please_enter_confirm_password))
            flag = false
        }
        else if (editPasswordNewET.getText().toString()
                .trim { it <= ' ' } != editPasswordConfirmET.getText().toString().trim { it <= ' ' }
        ) {
            showSNACK(txSubmitTV, getString(R.string.password_not_matched))
            flag = false
        } else if (editPasswordOldET.getText().toString().trim { it <= ' ' }.length < 6) {
            showSNACK(txSubmitTV, getString(R.string.please_enter6_digit_current_password))
            flag = false
        } else if (editPasswordNewET.getText().toString().trim { it <= ' ' }.length < 6) {
            showSNACK(txSubmitTV, getString(R.string.please_enter6_digit_new_password))
            flag = false
        } else if (editPasswordConfirmET.getText().toString().trim { it <= ' ' }.length < 6) {
            showSNACK(txSubmitTV, getString(R.string.please_enter6_digit_confirm_password))
            flag = false
        } else if (editPasswordOldET.getText().toString()
                .trim { it <= ' ' } == editPasswordNewET.getText().toString().trim { it <= ' ' }
        ) {
            showSNACK(txSubmitTV, getString(R.string.new_pass_and_old_pasword_different))
            flag = false
        }
        return flag
    }


/*TOGGLE
* \CHECKS*/
    private fun performPasswordSeenFunNew() {
        if (!passwordChekNew) {
            // show password
            passwordChekNew = true
            editPasswordNewET.transformationMethod = PasswordTransformationMethod.getInstance()
            editPasswordNewET.setSelection(editPasswordNewET.text.length)
            password_toggle_new!!.setImageResource(R.drawable.ic_eye_close)

        } else {
            // hide password
            editPasswordNewET.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editPasswordNewET.setSelection(editPasswordNewET.text.length)
            password_toggle_new!!.setImageResource(R.drawable.ic_eye_open)
            passwordChekNew = false
        }    }

    private fun performPasswordSeenFunOld() {
        if (!passwordChekOld) {
            // show password
            passwordChekOld = true
            editPasswordOldET.transformationMethod = PasswordTransformationMethod.getInstance()
            editPasswordOldET.setSelection(editPasswordOldET.text.length)
            password_toggle_old!!.setImageResource(R.drawable.ic_eye_close)

        } else {
            // hide password
            editPasswordOldET.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editPasswordOldET.setSelection(editPasswordOldET.text.length)
            password_toggle_old!!.setImageResource(R.drawable.ic_eye_open)
            passwordChekOld = false
        }
    }

    private fun performPasswordSeenConfirm() {
        if (!passwordChekConfirm) {
            // show password
            passwordChekConfirm = true
            editPasswordConfirmET.transformationMethod = PasswordTransformationMethod.getInstance()
            editPasswordConfirmET.setSelection(editPasswordConfirmET.text.length)
            password_toggle_confirm!!.setImageResource(R.drawable.ic_eye_close)

        } else {
            // hide password
            editPasswordConfirmET.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editPasswordConfirmET.setSelection(editPasswordConfirmET.text.length)
            password_toggle_confirm!!.setImageResource(R.drawable.ic_eye_open)
            passwordChekConfirm = false
        }
    }


}