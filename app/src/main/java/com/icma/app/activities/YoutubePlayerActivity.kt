package com.icma.app.activities

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.DEVELOPER_KEY
import java.lang.Exception

class YoutubePlayerActivity : YouTubeBaseActivity(), YouTubePlayer.OnInitializedListener {
    @BindView(R.id.youtube_view)
    lateinit var youTubePlayerView: YouTubePlayerView

    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV: TextView

    @BindView(R.id.llyBack)
    lateinit var llyBack: LinearLayout

    @BindView(R.id.imPlay)
    lateinit var imPlay: ImageView

    var youTubePlayer2: YouTubePlayer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_youtube_player)
        ButterKnife.bind(this)
        setData()
        youTubePlayerView.initialize(DEVELOPER_KEY, this)
    }

    private fun setData() {
        txHeadingTV.text = Constants.VIDEONAME
    }

    @OnClick(R.id.llyBack, R.id.imPlay)
    fun onClick(view: View) {
        when (view.id) {
            R.id.llyBack -> finish()
            R.id.imPlay -> play()
        }
    }

    private fun play() {
        showToast(this@YoutubePlayerActivity, "Clicked")
        youTubePlayer2!!.play()
    }

    override fun onInitializationSuccess(
        provider: YouTubePlayer.Provider,
        player: YouTubePlayer,
        wasRestored: Boolean
    ) {
        if (null == player) return
        youTubePlayer2 = player
        // Start buffering
        if (!wasRestored) {
            player.loadVideo(Constants.VIDEOID)
        }
        player.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL)
        player.setFullscreen(false)

    }

    override fun onInitializationFailure(
        provider: YouTubePlayer.Provider,
        youTubeInitializationResult: YouTubeInitializationResult
    ) {
        showToast(this, youTubeInitializationResult.toString())
    }

    override fun onPause() {
        super.onPause()
        try {
            if (youTubePlayer2 != null) {
                youTubePlayer2!!.pause()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        // Toast.makeText(mActivity, "Resume", Toast.LENGTH_SHORT).show();
        try {
            // youTubePlayer2.release();
            if (youTubePlayer2 != null) {

                youTubePlayer2!!.setFullscreen(true)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        finish()
    }


    //  Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        val view: View = LayoutInflater.from(mActivity)
            .inflate(com.icma.app.R.layout.toast_layout, null)
        val tvMessage = view.findViewById<TextView>(com.icma.app.R.id.tvMessage)
        tvMessage.setText(strMessage)
        val toast = Toast(mActivity)
        toast.setView(view);
        toast.show();
    }
}
