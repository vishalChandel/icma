package com.icma.app.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.icma.app.R
import com.icma.app.fragments.*
import kotlinx.android.synthetic.main.activity_login.*

class HomeActivity : AppCompatActivity() {
    @BindView(R.id.img1)
    lateinit var img1:ImageView
    @BindView(R.id.img2)
    lateinit var img2:ImageView
    @BindView(R.id.img3)
    lateinit var img3:ImageView
    @BindView(R.id.img4)
    lateinit var img4:ImageView
    @BindView(R.id.img5)
    lateinit var img5:ImageView
    @BindView(R.id.llyImg1)
    lateinit var llyImg1:LinearLayout
    @BindView(R.id.llyImg2)
    lateinit var llyImg2:LinearLayout
    @BindView(R.id.llyImg3)
    lateinit var llyImg3:LinearLayout
    @BindView(R.id.llyImg4)
    lateinit var llyImg4:LinearLayout
    @BindView(R.id.llyImg5)
    lateinit var llyImg5:LinearLayout
    var openedTab=-1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(this)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, Home1Fragment.newInstance())
        transaction.commit()
        openedTab=1
    }


    @OnClick(R.id.llyImg1,R.id.llyImg2,R.id.llyImg3,R.id.llyImg4,R.id.llyImg5)
    fun onClick(view:View)
    {
        when(view.id)
        {
            R.id.llyImg1->if(openedTab!=1){performImg1Click()}
            R.id.llyImg2->if(openedTab!=2)performImg2Click()
            R.id.llyImg3->if(openedTab!=3)performImg3Click()
            R.id.llyImg4->if(openedTab!=4)performImg4Click()
            R.id.llyImg5->if(openedTab!=5)performImg5Click()

        }
    }



    private fun performImg1Click() {
        openedTab=1
        img1!!.setBackgroundResource(R.drawable.home_icon1)
        img2!!.setBackgroundResource(R.drawable.home_icon2_grey)
        img3!!.setBackgroundResource(R.drawable.home_icon3_grey)
        img4!!.setBackgroundResource(R.drawable.home_icon4_grey)
        img5!!.setBackgroundResource(R.drawable.home_icon5_grey)
        var selectedFragment: Fragment? = null
        selectedFragment = Home1Fragment.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, selectedFragment!!)
        transaction.commit()
    }


    private fun performImg2Click() {
        openedTab=2
        img1!!.setBackgroundResource(R.drawable.home_icon1_grey)
        img2!!.setBackgroundResource(R.drawable.home_icon2)
        img3!!.setBackgroundResource(R.drawable.home_icon3_grey)
        img4!!.setBackgroundResource(R.drawable.home_icon4_grey)
        img5!!.setBackgroundResource(R.drawable.home_icon5_grey)
        var selectedFragment: Fragment? = null
        selectedFragment = Home2Fragment.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, selectedFragment!!)
        transaction.commit()

    }

    private fun performImg3Click() {
        openedTab=3
        img1!!.setBackgroundResource(R.drawable.home_icon1_grey)
        img2!!.setBackgroundResource(R.drawable.home_icon2_grey)
        img3!!.setBackgroundResource(R.drawable.home_icon3)
        img4!!.setBackgroundResource(R.drawable.home_icon4_grey)
        img5!!.setBackgroundResource(R.drawable.home_icon5_grey)
        var selectedFragment: Fragment? = null
        selectedFragment = Home3Fragment.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, selectedFragment!!)
        transaction.commit()

    }

    private fun performImg4Click() {
        openedTab=4
        img1!!.setBackgroundResource(R.drawable.home_icon1_grey)
        img2!!.setBackgroundResource(R.drawable.home_icon2_grey)
        img3!!.setBackgroundResource(R.drawable.home_icon3_grey)
        img4!!.setBackgroundResource(R.drawable.home_icon4)
        img5!!.setBackgroundResource(R.drawable.home_icon5_grey)

        var selectedFragment: Fragment? = null
        selectedFragment = Home4Fragment.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, selectedFragment!!)
        transaction.commit()
    }

    private fun performImg5Click() {
        openedTab=5
        img1!!.setBackgroundResource(R.drawable.home_icon1_grey)
        img2!!.setBackgroundResource(R.drawable.home_icon2_grey)
        img3!!.setBackgroundResource(R.drawable.home_icon3_grey)
        img4!!.setBackgroundResource(R.drawable.home_icon4_grey)
        img5!!.setBackgroundResource(R.drawable.home_icon5)
        var selectedFragment: Fragment? = null
        selectedFragment = Home5Fragment.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, selectedFragment!!)
        transaction.commit()

    }



}