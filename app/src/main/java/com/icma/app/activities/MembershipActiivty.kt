package com.icma.app.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.android.billingclient.api.*
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.dataobject.RequestBodies
import com.icma.app.utils.Constants
import com.icma.app.utils.Constants.Companion.LICENCE_KEY
import com.icma.app.utils.Constants.Companion.MERCHANT_ID
import com.icma.app.utils.Constants.Companion.SUBSCRIPTION_PLAN_KEY
import com.icma.app.utils.ICMAPrefrences
import com.icma.app.utils.Resource
import com.icma.app.viewmodel.MemberShipViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import com.paypal.checkout.approve.OnApprove
import com.paypal.checkout.cancel.OnCancel
import com.paypal.checkout.createorder.CreateOrder
import com.paypal.checkout.createorder.CurrencyCode
import com.paypal.checkout.createorder.OrderIntent
import com.paypal.checkout.createorder.UserAction
import com.paypal.checkout.error.OnError
import com.paypal.checkout.order.*
import com.paypal.checkout.paymentbutton.PayPalButton
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*


class MembershipActiivty : BaseActivity(), PurchasesUpdatedListener,
    BillingProcessor.IBillingHandler {
    @BindView(R.id.llyManageSubscription)
    lateinit var llyManageSubscription:LinearLayout
    @BindView(R.id.txTermsOfUse)
    lateinit var txTermsOfUse:TextView
    @BindView(R.id.txPrivacyPolicy)
    lateinit var txPrivacyPolicy:TextView
    @BindView(R.id.payPalButton)
    lateinit var payPalButton: PayPalButton
    @BindView(R.id.llyBuyNow)
    lateinit var llyBuyNow:LinearLayout
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV:TextView
    @BindView(R.id.llyBack)
    lateinit var llyBack:LinearLayout


    var deviceTokenUser:String=""
    var tag="PayPal::::"
    var mBillingProcessor: BillingProcessor? = null
    var transactionDetails: TransactionDetails? = null

    //Subscription
    var product: String? = null
    private var billingClient0: BillingClient? = null
    val skuList = ArrayList<String>()
    var check=true

    lateinit var addPrayerViewModel: MemberShipViewModel

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_membership_actiivty)
        ButterKnife.bind(this)
        initializeSubscription()
        setHeaderText()
        getDeviceTokenUser()
       // payPalButton.onEligibilityStatusChanged = { buttonEligibilityStatus: PaymentButtonEligibilityStatus ->
//            Log.e(tag, "OnEligibilityStatusChanged")
//            Log.e(tag, "Button eligibility status: $buttonEligibilityStatus")
//        }
    //    setupPaymentButton()
        init()
    }

    private fun setHeaderText() {
        txHeadingTV.setText(getString(R.string.membership))
    }

    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        addPrayerViewModel = ViewModelProvider(this, factory).get(MemberShipViewModel::class.java)
    }

    //Clicks
    @OnClick(R.id.llyManageSubscription,R.id.txTermsOfUse,R.id.txPrivacyPolicy,R.id.llyBuyNow, R.id.llyBack)
    fun onClick(view: View)
    {
        when(view.id)
        {
            R.id.llyManageSubscription->finish()
            R.id.txTermsOfUse->switchToWebView(getString(R.string.terms_of_use),getString(R.string.terms_type))
            R.id.txPrivacyPolicy->switchToWebView(getString(R.string.privacy_policy), getString(R.string.policy_type))
            R.id.llyBuyNow->subscriptionCallBack()
            R.id.llyBack ->  finish()

        }
    }

    private fun payPal() {
        showToast(mActivity,"Somet")
        // ...

    }

    private fun switchToWebView(headerText: String, valType: String) {
        val intent= Intent(mActivity,WebViewActivity::class.java)
        intent.putExtra("type",headerText)
        intent.putExtra("valType",valType)
        startActivity(intent)
    }


    //SUBSCRIPTION
    private fun buyInapp() {
        buttonTimer(llyBuyNow)
        product=Constants.SUBSCRIPTION_KEY
        skuList.add(product!!)
        billingClient0 = BillingClient.newBuilder(mActivity).enablePendingPurchases().setListener(this).build()
        billingClient0!!.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    loadAllSKUs()
                    Log.e("Billing","Success")
                }
                else
                    {
                        showSNACK(llyBuyNow,""+getString(R.string.billing_unavailable))
                    }
            }
            override fun onBillingServiceDisconnected() {
                Log.e("Billing","disconnect")
            }
        })

    }

    private fun loadAllSKUs() {
        if (billingClient0!!.isReady) {
            val params = SkuDetailsParams.newBuilder().setSkusList(skuList).setType(BillingClient.SkuType.SUBS).build()
            billingClient0!!.querySkuDetailsAsync(params
            ) { billingResult, skuDetailsList ->
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && !skuDetailsList!!.isEmpty()) {
                    for (skuDetailsObject in skuDetailsList) {
                        val skuDetails = skuDetailsObject as SkuDetails
                        if (skuDetails.sku == product) {
                            val billingFlowParams = BillingFlowParams.newBuilder().setSkuDetails(skuDetails).build()
                            if (check) {
                                billingClient0!!.launchBillingFlow(this@MembershipActiivty, billingFlowParams)
                                check = false
                            }
                        }
                    }
                }
                else {
                    showSNACK(llyBuyNow,""+getString(R.string.billing_unavailable))
                }
            }
        } else {
            Log.e("Billing", "Billing client not ready")
        }
    }
    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: List<Purchase?>?) {
        val responseCode = billingResult.responseCode
        if (responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
            Log.e("Billing", "Already bought")
        } else if (responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
            for (purchase in purchases) {
                purchase?.let { handlePurchase(it) }
            }
        } else if (responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
            check = true
        } else {
            check = true
        }
    }


    private fun handlePurchase(purchase: Purchase) {
        executePrayerApi(purchase)
    }


    //Paypal buy
    @RequiresApi(Build.VERSION_CODES.M)
    private fun setupPaymentButton() {
        payPalButton.setup(
            createOrder = CreateOrder { createOrderActions ->
                Log.e(tag, "CreateOrder")
                createOrderActions.create(Order.Builder().appContext(
                    AppContext(userAction = UserAction.PAY_NOW)
                )
                    .intent(OrderIntent.CAPTURE)
                    .purchaseUnitList(listOf(PurchaseUnit.Builder().amount(Amount.Builder()
                        .value("10.00")
                        .currencyCode(CurrencyCode.USD)
                        .build()).build())).build()
                    .also { Log.e(tag, "Order: $it") }
                )
            },

            onApprove = OnApprove { approval ->
                Log.e(tag, "OnApprove")
                Log.e(tag, "Approval details: $approval")
                approval.orderActions.capture { captureOrderResult ->
                    showToast(mActivity,getString(R.string.payment_success))
                    Log.e(tag, "Capture Order id "+approval.data.orderId)
                    Log.e(tag, "Capture Order  "+approval.data.paymentId)
                    Log.e(tag, "Capture order result: $captureOrderResult")
                }

            },


            onCancel = OnCancel {
                Log.e(tag, "OnCancel")
                Log.e(tag, "Buyer cancelled the checkout experience.")
            },
            onError = OnError { errorInfo ->
                Log.e(tag, "OnError")
                Log.e(tag, "Error details: $errorInfo")
            }
        )
    }

    //Api Hit
    private fun executePrayerApi(purchase: Purchase) {
        val date: Calendar = Calendar.getInstance()
        date.add(Calendar.YEAR, +1)
        date.getTimeInMillis()
        var timestampExpire:String= (date.timeInMillis/1000L).toString()
        var userid = getLoggedInUserID()
        var title = "yearly"
        var end_date = timestampExpire
        var referralby = getReferralCode()
        var devicetype ="2"
        val devicemodel = Build.MANUFACTURER + " " + Build.MODEL
        val body = RequestBodies.InappPuchaseBody(
            userid,
            title,
            end_date,
            referralby,
            devicetype,
            devicemodel,
            purchase.orderId
        )
        Log.e("Check",body.toString())
        addPrayerViewModel.executeInapp(body,mActivity)
        addPrayerViewModel.resultResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { prayerResponseF ->

                            if (prayerResponseF.status==1||prayerResponseF.status==200) {
                                showToast(mActivity,getString(R.string.payment_successfull))
                                var start_date:String=""
                                var expiration_date:String=""
                                if (!prayerResponseF.data.startDate.equals(""))
                                {
                                    start_date=timeChange(prayerResponseF.data.startDate.toString().toLong())
                                }
                                if (!prayerResponseF.data.endDate.equals(""))
                                {
                                    expiration_date=timeChange(prayerResponseF.data.endDate.toString().toLong())
                                }
                                ICMAPrefrences.writeBoolean(mActivity, ICMAPrefrences.ISLOGIN, true)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.ID, prayerResponseF.data.userid)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.FIRSTNAME, prayerResponseF.data.firstname)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.LASTNAME, prayerResponseF.data.lastname)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.NAME, prayerResponseF.data.firstname+" "+prayerResponseF.data.lastname)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.EMAIL, prayerResponseF.data.email)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.AUTHTOKEN, prayerResponseF.data.authtoken)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.PHOTO, prayerResponseF.data.profileimage)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.DEVICETOKEN, deviceTokenUser)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.DEVICETYPE, "2")
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.REFERRALCODE, prayerResponseF.data.referralCode)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.REFERRALCOUNT, prayerResponseF.data.referralCount)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.START_DATE, start_date)
                                ICMAPrefrences.writeString(mActivity, ICMAPrefrences.EXPIRATION_DATE, expiration_date)
                                val intent = Intent(this@MembershipActiivty, HomeActivity::class.java)
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                startActivity(intent)
                            }
                            else{
                                showSNACK(llyBuyNow,getString(R.string.server_error))

                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showSNACK(llyBuyNow,message)

                        }
                    }

                    is Resource.Loading -> {
                        Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })
    }


    //Device token
    fun getDeviceTokenUser() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener {
            if (it.isSuccessful) {
                deviceTokenUser = it.result!!
                Log.e("ICMA", "Device token ::"+deviceTokenUser)
            } else {
                Log.e("ICMA", "Device token fetch issue")
                return@OnCompleteListener
            }
        })

    }

    /*
        * In App  Subscriptions
        * */
    open fun initializeSubscription() {
        mBillingProcessor = BillingProcessor(mActivity, LICENCE_KEY, this)
        mBillingProcessor!!.initialize()
    }

    private fun subscriptionCallBack() {
        // With Testing  PayLoads for Product Purchase & Subscription
        //mBillingProcessor.purchase(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
        //mBillingProcessor.subscribe(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
        if (isNetworkAvailable(mActivity)) {
            val extraParams = Bundle()
            extraParams.putString("accountId", MERCHANT_ID)
            mBillingProcessor!!.subscribe(mActivity, SUBSCRIPTION_PLAN_KEY, null , extraParams)
        } else {
            showToast(mActivity, getString(R.string.no_internet_connection))
        }
    }

    override fun onProductPurchased(productId: String, mTransactionDetails: TransactionDetails?) {
        Log.e(TAG, "Purchase DATA :- " + mTransactionDetails!!.purchaseInfo.purchaseData)
        /*
         * Called when requested PRODUCT ID was successfully purchased
         */
        val mPurchaseData = mTransactionDetails!!.purchaseInfo.purchaseData
        var mSubscriptionPlanKey = mTransactionDetails!!.purchaseInfo.purchaseData.productId

        Log.e(TAG, "onProduct Purchased_Data: " + mTransactionDetails!!.purchaseInfo.purchaseData.toString())
        Log.e(TAG, "onProduct Purchased_OrderID: " + mPurchaseData!!.orderId)
        Log.e(TAG, "onProduct Purchased_Token: " + mPurchaseData!!.purchaseToken)
        Log.e(TAG, "onProduct Purchased_Time: " + mPurchaseData!!.purchaseTime)
        Log.e(TAG, "Subscriptions Plan Key: $mSubscriptionPlanKey")


//        if (mTransactionDetails!!.purchaseInfo == null) {
//            AppPrefrences().writeBoolean(mActivity, IS_SUBSCRIPTION, false)
//            showToast(mActivity,getString(R.string.some_things))
//        } else {
//            AppPrefrences().writeBoolean(mActivity, IS_SUBSCRIPTION, true)
//            showToast(mActivity,getString(R.string.purchase_subscription_))
//        }

        finish()
    }

    override fun onPurchaseHistoryRestored() {

    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {
        /*
        * Called when some error occurred. See Constants class for more details
        *
        * Note - this includes handling the case where the user canceled the buy dialog:
        * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
        */
        Log.e(TAG, "onBillingError: ")
        when (errorCode) {
            0 -> Log.e("onBillingError", "> Success - BILLING_RESPONSE_RESULT_OK")
            1 -> Log.e(
                "onBillingError",
                "> User pressed back or canceled a dialog - BILLING_RESPONSE_RESULT_USER_CANCELED"
            )
            3 -> Log.e(
                "onBillingError",
                "> Billing API version is not supported for the type requested - BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE"
            )
            4 -> Log.e(
                "onBillingError",
                "> Requested product is not available for purchase - BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE"
            )
            5 -> Log.e(
                "onBillingError",
                "> Invalid arguments provided to the API. This error can also indicate that the application was not correctly signed or properly set up for In-app Billing in Google Play, or does not have the necessary permissions in its manifest - BILLING_RESPONSE_RESULT_DEVELOPER_ERROR"
            )
            6 -> Log.e(
                "onBillingError",
                "> Fatal error during the API action - BILLING_RESPONSE_RESULT_ERROR"
            )
            7 -> Log.e(
                "onBillingError",
                "> Failure to purchase since item is already owned - BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED"
            )
            8 -> Log.e(
                "onBillingError",
                "> Failure to consume since item is not owned - BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED"
            )
        }
    }

    override fun onBillingInitialized() {
        /*
        * Called when BillingProcessor was initialized and it's ready to purchase
        */
        Log.e(TAG, "onBillingInitialized: ");
    }

    override fun onDestroy() {
        if (mBillingProcessor != null) mBillingProcessor!!.release()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!mBillingProcessor!!.handleActivityResult(requestCode, resultCode, data)) super.onActivityResult(
            requestCode,
            resultCode,
            data
        )
    }

}