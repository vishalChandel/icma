package com.icma.app.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Resource
import com.icma.app.dataobject.RequestBodies
import com.icma.app.viewmodel.ContactUsViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory

class ContactUsActivity : BaseActivity() {

    @BindView(R.id.llyBack)
    lateinit var llyBack: LinearLayout
    @BindView(R.id.txHeadingTV)
    lateinit var txHeadingTV: TextView
    @BindView(R.id.llyName)
    lateinit var llyName: LinearLayout
    @BindView(R.id.edName)
    lateinit var edName: EditText
    @BindView(R.id.llyEmail)
    lateinit var llyEmail: LinearLayout
    @BindView(R.id.llyMessage)
    lateinit var llyMessage: LinearLayout
    @BindView(R.id.edMessage)
    lateinit var edMessage: EditText
    @BindView(R.id.edEmail)
    lateinit var edEmail: EditText
    @BindView(R.id.txSubmitTV)
    lateinit var txSubmitTV: TextView

    lateinit var contactUsViewModel: ContactUsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        ButterKnife.bind(this)
        setData()
        performClicks()
        init()

    }

    private fun setData() {
        txHeadingTV.text=getString(R.string.contact_us)
        edName.setText(getUserName())
        edEmail.setText(getEmail())
    }

    //Repository setup
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(application, repository)
        contactUsViewModel = ViewModelProvider(this, factory).get(ContactUsViewModel::class.java)
    }


    //Click Listeners
    @OnClick(R.id.llyBack,R.id.txSubmitTV)
    fun onClick(view: View)
    {
        when(view.id)
        {
            R.id.llyBack   ->finish()
            R.id.txSubmitTV->executeApi()
        }
    }

    //API Execution
    private fun executeApi() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showSNACK(txSubmitTV, getString(R.string.no_internet_connection))
            } else {
              executeContactUsApi()

            }
    }


    private fun executeContactUsApi() {
        var name = edName.text.toString().trim()
        var email = edEmail.text.toString().trim()
        var message = edMessage.text.toString().trim()
        val body = RequestBodies.ContactUsBody(
            name,
            email,
            message
        )
        Log.e("Check",body.toString())
        contactUsViewModel.contactUs(getAUthToken(),body,mActivity)
        contactUsViewModel.resultResponse.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { prayerResponseF ->

                            if (prayerResponseF.status==1||prayerResponseF.status==200) {
                                showToast(mActivity,prayerResponseF.message)
                                finish()
                            }
                            else{
                                showSNACK(txSubmitTV,prayerResponseF.message)

                            }
                        }
                    }

                    is Resource.Error -> {
                        Constants.dismissProgressDialog()
                        response.message?.let { message ->
                            showSNACK(txSubmitTV,message)

                        }
                    }

                    is Resource.Loading -> {
                        Constants.showProgressDialog(mActivity)
                    }
                }
            }
        })
    }




    //Clicks listeners
    private fun performClicks() {

        edName.addTextChangedListener {
            nameSet()
        }
        edMessage.addTextChangedListener {
            messageSet()
        }
        edEmail.addTextChangedListener {
            emailSet()
        }
    }

    //functions for view  chnage
    private fun nameSet() {
        llyName.setBackgroundResource(R.drawable.bg_et)
        llyMessage.setBackgroundResource(R.drawable.bg_et_grey)
        llyEmail.setBackgroundResource(R.drawable.bg_et_grey)
    }

    private fun messageSet() {
        llyName.setBackgroundResource(R.drawable.bg_et_grey)
        llyMessage.setBackgroundResource(R.drawable.bg_et)
        llyEmail.setBackgroundResource(R.drawable.bg_et_grey)   }

    private fun emailSet() {
        llyName.setBackgroundResource(R.drawable.bg_et_grey)
        llyMessage.setBackgroundResource(R.drawable.bg_et_grey)
        llyEmail.setBackgroundResource(R.drawable.bg_et)   }


    /*
* Set up validations
* */
    fun isValidate(): Boolean {
        var flag = true
        if (edName.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edName,getString(R.string.enter_name))
            flag = false
        }  else if (edEmail.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edEmail,getString(R.string.please_enter_your_email))
            flag = false
        } else if (!isValidEmaillId(edEmail.text.toString().trim { it <= ' ' })) {
            showSNACK(edEmail,getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (edMessage.text.toString().trim { it <= ' ' } == "") {
            showSNACK(edMessage,getString(R.string.enter_message))
            flag = false
        }
        return flag
    }

}