package com.icma.app.retrofit


import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.icma.app.utils.Constants

import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*
import okhttp3.OkHttpClient
import java.lang.Exception
import java.lang.RuntimeException


class RetrofitInstance {

    companion object {
        //Staging
        // private const val BASE_URL = "https://www.dharmani.com/ICMA/webservices/"

        //Live
        private const val BASE_URL = "https://icma.net/webservices/"

        private val retrofitLogin by lazy {
            val gson = GsonBuilder()
                .setLenient()
                .create()

            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()
            httpsTrustManager2.allowAllSSL()
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
              //  .client(client)
                .client(getUnsafeOkHttpClient11()!!.build())
                .build()


        }


        val appApi by lazy {
            retrofitLogin.create(API::class.java)
        }


        //Auth issue of domain
        private fun getUnsafeOkHttpClient11(): OkHttpClient.Builder? {
            return try {

                val trustAllCerts = arrayOf<TrustManager>(
                    object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate?>?,
                            authType: String?
                        ) {
                        }

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate?>?,
                            authType: String?
                        ) {
                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate?>? {
                            return arrayOf()
                        }
                    }
                )

                // Install the all-trusting trust manager
                val sslContext: SSLContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())

                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory: SSLSocketFactory = sslContext.socketFactory
                val interceptor = HttpLoggingInterceptor()
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
                val builder: OkHttpClient.Builder =
                    OkHttpClient.Builder().addInterceptor(interceptor)
                        .connectTimeout(1, TimeUnit.MINUTES)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                builder.hostnameVerifier { hostname, session -> true }
                builder
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }


    }





}