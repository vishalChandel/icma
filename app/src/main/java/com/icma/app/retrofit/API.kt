package com.icma.app.retrofit

import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface API {

    @POST("login.php")
    suspend fun loginUser(@Body body: RequestBodies.LoginBody): Response<LoginModel>

    @POST("signup.php")
    suspend fun signUpUser(@Body body: RequestBodies.SignUpBody): Response<LoginModel>

    @POST("forgotpassword.php")
    suspend fun forgotPswd(@Body body: RequestBodies.ForgotPswdBody): Response<ForgotPasswordModel>

    @POST("audiovideoListing.php")
    suspend fun homeVideoAudioData(@Header("token") mAuthorization: String?,@Body body: RequestBodies.Home2Body): Response<VideoAudioModel>

    @POST("videoListing.php")
    suspend fun home1Data( @Header("token") mAuthorization: String?, @Body body: RequestBodies.Home5Body): Response<Home1Model>


    @POST("hymns.php")
    suspend fun home4Data( @Header("token") mAuthorization: String?, @Body body: RequestBodies.Home5Body): Response<HymnModel>


    @POST("getprayers.php")
    suspend fun homePrayers( @Header("token") mAuthorization: String?, @Body body: RequestBodies.Home5Body): Response<PrayerModel>

    @POST("addprayer.php")
    suspend fun addPrayer( @Header("token") mAuthorization: String?, @Body body: RequestBodies.PrayerBody): Response<AddPrayerModel>

    @POST("contactus.php")
    suspend fun contactUs( @Header("token") mAuthorization: String?, @Body body: RequestBodies.ContactUsBody): Response<StatusModel>

    @POST("logout.php")
    suspend fun logOut( @Header("token") mAuthorization: String?): Response<StatusModel>

    @POST("notificationListing.php")
    suspend fun getNotifications( @Header("token") mAuthorization: String?,@Body body: RequestBodies.Home5Body): Response<NotificationModel>

    @POST("changepassword.php")
    suspend fun changePswd( @Header("token") mAuthorization: String?,@Body body: RequestBodies.ChangePswdBody): Response<StatusModel>


    @POST("search.php")
    suspend fun searchData(@Header("token") authToken: String, @Body body: RequestBodies.SearchBody): Response<SearchModel>

    @POST("inapp_purchase.php")
    suspend fun inappPurchase( @Body body: RequestBodies.InappPuchaseBody): Response<InAppPurchaseModel>

    @POST("payment_status.php")
    suspend fun getPaymentStatus( @Header("token") authToken: String,): Response<PaymentStatusModel>

    @POST("free_membership.php")
    suspend fun freeMembershipCheck( @Header("token") authToken: String,): Response<StatusModel>

    @Multipart
    @POST("editprofile.php")
     fun profileSaveDataRequest(
        @Header("token") token: String?,
        @Part profileimage: MultipartBody.Part?,
        @Part("firstname") firstname: RequestBody?,
        @Part("lastname") lastname: RequestBody?,
        @Part("phone") phone: RequestBody?,
        @Part("password") password: RequestBody?,

    ): Call<EditProfileModel>



}