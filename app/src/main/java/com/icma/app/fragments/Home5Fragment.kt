package com.icma.app.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.facebook.shimmer.ShimmerFrameLayout
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Resource
import com.icma.app.activities.AddPrayerActivity
import com.icma.app.adapters.Home5Adapter
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.DataPrayer
import com.icma.app.viewmodel.Home5ViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import java.util.*


class Home5Fragment : BaseFragment() {
    companion object {
        @JvmStatic
        fun newInstance() = Home5Fragment()
    }

    @BindView(R.id.recyclerHome5)
    lateinit var recyclerHome2:RecyclerView
    @BindView(R.id.txVideoHeadingMain)
    lateinit var txVideoHeadingMain: TextView
    @BindView(R.id.shimmerViewContainer)
    lateinit var mShimmerViewContainer: ShimmerFrameLayout
    @BindView(R.id.llyGoTo)
    lateinit var llyGoTo:LinearLayout
    @BindView(R.id.imBannerImage)
    lateinit var imBannerImage: ImageView

    lateinit var home5ViewModel: Home5ViewModel
    var data_list = mutableListOf<DataPrayer>()
    lateinit var adapter:Home5Adapter
    var dataPrayer:DataPrayer?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view:View=inflater.inflate(R.layout.fragment_home5, container, false)
        ButterKnife.bind(this,view)
        setHeadLayoutData()
        init()
        executeApi()
        return  view
    }

    @OnClick(R.id.llyGoTo)
    fun onClick(view:View)
    {
        when(view.id)
        {
            R.id.llyGoTo->goToAddPrayerScreen()
        }
    }

    //get data from Prayer activity and add to current list
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
            val data: Intent? = result.data
            dataPrayer= data!!.getParcelableExtra(Constants.MODEL)
            if(data_list!=null&&data_list.size>1) {
                Collections.reverse(data_list)
            }
            data_list.add(dataPrayer!!)
            Collections.reverse(data_list)
            if (adapter!=null) {
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun goToAddPrayerScreen() {
        val intent= Intent(context, AddPrayerActivity::class.java)
        resultLauncher.launch(intent)
    }

    private fun setHeadLayoutData() {
        txVideoHeadingMain.setText(getString(R.string.fifth_screen_heading))
    }


    //REPOSIORY SETUP
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(requireActivity().application, repository)
        home5ViewModel = ViewModelProvider(this, factory).get(Home5ViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()

    }

    //Execute api
    private fun executeApi() {
        var perPage = "100"
        var pageNo="1"
        val body = RequestBodies.Home5Body(
            perPage,
            pageNo
        )
        home5ViewModel.homeGetData(getAUthToken(),body,context as Activity)
        home5ViewModel.homeResponse.observe(viewLifecycleOwner, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { homeResponse ->
                            if (homeResponse.status==1||homeResponse.status==200) {
                                setAdapter(homeResponse)
                            }
                            else if (homeResponse.status == 401)
                            {
                                Constants.showAuthDismissDialog(context as Activity?,getString(R.string.authorization_failure))
                            }
                            else{
                               homeResponse.message?.let { showSNACK(txVideoHeadingMain, it) }
                            }
                        }
                    }

                    is Resource.Error -> {
                        response.message?.let { message -> showSNACK(txVideoHeadingMain,message)
                        }
                    }

                    is Resource.Loading -> {
                      //  mShimmerViewContainer!!.visibility=View.VISIBLE
                    }
                }
            }
        })
    }

    private fun setAdapter(homeResponse: com.icma.app.model.PrayerModel) {
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_video_thumb)
            .error(R.drawable.ic_video_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
        context?.let { Glide.with(it).load(homeResponse.banner!!.bannerImage).apply(options).into(imBannerImage) }
        data_list.clear()
        data_list.addAll(homeResponse.data)
        recyclerHome2.layoutManager = LinearLayoutManager(context)
        adapter = Home5Adapter(data_list,mShimmerViewContainer)
        Log.e("TEXT",data_list.toString())
        recyclerHome2.adapter = adapter
    }


}