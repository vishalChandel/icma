package com.icma.app.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.icma.app.R
import com.icma.app.utils.ICMAPrefrences
import java.util.*
import java.util.regex.Pattern

open class BaseFragment : Fragment() {

    /*
     * Get Class Name
     * */
    var TAG = this@BaseFragment.javaClass.simpleName
    //  Initialize Objects
    var progressDialog: Dialog? = null



    //  Clear EditText Focus
    fun setEditTextFocused(mActivity: Activity, mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm = v.context
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }



    //  Show Toast Message
    //  Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        val view: View = LayoutInflater.from(mActivity)
            .inflate(com.icma.app.R.layout.toast_layout, null)
        val tvMessage = view.findViewById<TextView>(com.icma.app.R.id.tvMessage)
        tvMessage.setText(strMessage)
        val toast = Toast(mActivity)
        toast.setView(view);
        toast.show();
    }

    //  Email Address Validation
    fun isValidEmaillId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


    //  To Check Internet Connections
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    //  Get UserId
    fun getLoggedInUserID(): String {
        return ICMAPrefrences.readString(requireContext(), ICMAPrefrences.USERID, "")!!
    }

    //    Get UserName
    fun getUserName(): String {
        return ICMAPrefrences.readString(requireContext(), ICMAPrefrences.NAME, "")!!
    }


    //  Get Email
    fun getEmail(): String {
        return ICMAPrefrences.readString(requireContext(),  ICMAPrefrences.EMAIL, "")!!
    }


    //  Get AuthToken
    fun getAUthToken(): String {
        return ICMAPrefrences.readString(requireContext(),  ICMAPrefrences.AUTHTOKEN, "")!!
    }

    //  Get First Name
    fun getFirstName(): String {
        return ICMAPrefrences.readString(requireContext(),  ICMAPrefrences.FIRSTNAME, "")!!
    }

    //  Get Last Name
    fun getLastName(): String {
        return ICMAPrefrences.readString(requireContext(),  ICMAPrefrences.LASTNAME, "")!!
    }

    //
    fun isUserLogin(): Boolean {
        return ICMAPrefrences.readBoolean(requireContext(),  ICMAPrefrences.ISLOGIN,false)!!
    }



    //Hide KeyBoard
    fun hideSoftKeyBoard(context: Context, view: View) {
        try {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        } catch (e: Exception) {
            e.printStackTrace()
        }



    }




    //  Show Alert Dialog
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.txOk)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }


    //  Show Alert Dialog
    fun showDismissDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.txOk)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
            mActivity.finish()}
        alertDialog.show()
    }



    //  Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)

        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    //  Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }
    fun showSnackBar(view: View) {
        val snack = Snackbar.make(view.rootView, "Sample snack bar message", Snackbar.LENGTH_INDEFINITE)
        snack.setAction("Click Me") {
            snack.dismiss()
        }
        snack.show()
    }

    fun showSNACK(view: View, message:String){

        var AC:String
        AC = "DISMISS"
        var snackbar: Snackbar?=null
        snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
            .setAction(AC, View.OnClickListener {snackbar!!.dismiss()})

        snackbar.setActionTextColor(Color.WHITE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(resources.getColor(R.color.app_color))
        val textView = snackbarView.findViewById(R.id.snackbar_text) as TextView
        val actionTextView = snackbarView.findViewById(R.id.snackbar_action)as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 15f
        actionTextView.textSize = 13f

        snackbar.show()
    }

    //Button Timer
    fun buttonTimer(viewC:View)
    {
        viewC.isEnabled = false;
        Handler(Looper.getMainLooper()).postDelayed({ viewC!!.isEnabled = true }, 1500)
    }
}