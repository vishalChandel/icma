package com.icma.app.fragments

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.HandlerCompat.postDelayed
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.exoplayer2.offline.DownloadService
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.Resource
import com.icma.app.app.MyDownloadService
import com.icma.app.activities.NotificationActivity
import com.icma.app.activities.ProfileActivity
import com.icma.app.activities.SearchScreenActivity
import com.icma.app.activities.WebViewActivity
import com.icma.app.adapters.Home1Adapter
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.Home1Model
import com.icma.app.viewmodel.Home1ViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import kotlinx.android.synthetic.main.activity_web_view.*
import kotlinx.android.synthetic.main.activity_web_view.webView
import kotlinx.android.synthetic.main.banner_pop_dialog.*

class Home1Fragment : BaseFragment() {
    companion object {
        @JvmStatic
        fun newInstance() = Home1Fragment()
    }

    @BindView(R.id.recyclerviewHome1)
    lateinit var recyclerviewHome1:RecyclerView
    @BindView(R.id.imProfile)
    lateinit var imProfile:ImageView
    @BindView(R.id.shimmerViewContainer)
    lateinit var mShimmerViewContainer: ShimmerFrameLayout
    @BindView(R.id.mainLly)
    lateinit var mainlly: LinearLayout
    @BindView(R.id.imBannerImage)
    lateinit var imBannerImage:ImageView
    @BindView(R.id.imNotification)
    lateinit var imNotification:ImageView
    @BindView(R.id.imSearch)
    lateinit var imSearch:ImageView

    lateinit var home1ViewModel: Home1ViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view:View=inflater.inflate(R.layout.fragment_home1, container, false)
        ButterKnife.bind(this,view)
        init()
        executeApi()
        return  view
    }



    //REPOSIORY SETUP
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(requireActivity().application, repository)
        home1ViewModel = ViewModelProvider(this, factory).get(Home1ViewModel::class.java)
    }

    @OnClick(R.id.imProfile,R.id.imNotification,R.id.imSearch)
    fun onCLick(view:View)
    {
       when(view.id)
       {
           R.id.imProfile->gotoProfile()
           R.id.imNotification ->switchToNotificationFrag()
           R.id.imSearch ->switchToSearchScreen()
       }
    }

    private fun switchToSearchScreen() {
        Intent(context,SearchScreenActivity::class.java).also {
            startActivity(it)
        }
    }

    private fun switchToNotificationFrag() {
        Intent(context,NotificationActivity::class.java).also {
            startActivity(it)
        }
    }

    private fun gotoProfile() {
        buttonTimer(imProfile)
        Intent(context,ProfileActivity::class.java).also {
            startActivity(it)
        }

    }

    private fun executeApi() {
        var perPage = "500"
        var pageNo="1"
        val body = RequestBodies.Home5Body(perPage, pageNo)
        home1ViewModel.homeGetData(getAUthToken(),body,context as Activity)
        home1ViewModel.homeResponse.observe(viewLifecycleOwner, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { homeResponse ->
                            if (homeResponse.status == 401)
                            {
                            Constants.showAuthDismissDialog(context as Activity?,getString(R.string.authorization_failure))
                            }
                            else {
                                setAdapter(homeResponse)
                            }
                        }
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            showSNACK(recyclerviewHome1, message)
                        }
                    }

                    is Resource.Loading -> {
                    }
                }
            }
        })
    }


    private fun setAdapter(homeResponse: Home1Model) {
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_video_thumb)
            .error(R.drawable.ic_video_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
        context?.let { Glide.with(it).load(homeResponse.banner.bannerImage).apply(options).into(imBannerImage) }
        recyclerviewHome1.layoutManager = LinearLayoutManager(context)
        val adapter = Home1Adapter(context,homeResponse.video,mShimmerViewContainer,mainlly)
        recyclerviewHome1.adapter = adapter
    }

    //Bottom sheet dialog for devotional link
    fun showBottomSheet()
    {
        val bottomSheetDialog = context?.let { BottomSheetDialog(it) }
        val view: View = LayoutInflater.from(context).inflate(R.layout.banner_pop_dialog, null)
        bottomSheetDialog!!.setContentView(view)
        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)
        bottomSheetDialog.show()
        
        //Id fetch
        var txLink:TextView=view.findViewById(R.id.txLink)

        //CLick Handlers
        txLink.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(getString(R.string.zoom_link))
            )
            try {
                if (context?.let { it1 -> intent.resolveActivity(it1.packageManager) } != null) {
                    startActivity(intent)
                }
                else{
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=us.zoom.videomeetings")))
                }
            }
            catch (e: Exception)
            {
                e.printStackTrace()
            }


        }
    }


    fun StaticDOwnload()
    {
        //to resume download
        context?.let { it1 -> DownloadService.sendResumeDownloads(it1, MyDownloadService::class.java, true) }

        //to pause download
        context?.let { it1 -> DownloadService.sendPauseDownloads(it1, MyDownloadService::class.java, true) }
    }
}