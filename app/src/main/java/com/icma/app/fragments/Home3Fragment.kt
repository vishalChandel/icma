package com.icma.app.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.hadi.retrofitmvvm.repository.AppRepository
import com.icma.app.R
import com.icma.app.utils.Constants
import com.icma.app.utils.DownloadUtil
import com.icma.app.utils.Resource
import com.icma.app.activities.VideoActivity
import com.icma.app.adapters.Home2Adapter
import com.icma.app.dataobject.RequestBodies
import com.icma.app.model.AudioItem
import com.icma.app.model.VideoAudioModel
import com.icma.app.viewmodel.Home2ViewModel
import com.icma.app.viewmodel.ViewModelProviderFactory
import kotlinx.android.synthetic.main.item_video.*

class Home3Fragment : BaseFragment() {
    companion object {
        @JvmStatic
        fun newInstance() = Home3Fragment()
    }

    @BindView(R.id.recyclerHome2)
    lateinit var recyclerHome2: RecyclerView
    @BindView(R.id.txVideoHeadingMain)
    lateinit var txVideoHeadingMain:TextView
    @BindView(R.id.txPlayTime)
    lateinit var txPlayTime:TextView
    @BindView(R.id.vidThumbnail)
    lateinit var vidThumbnail:ImageView
    @BindView(R.id.shimmerViewContainer)
    lateinit var mShimmerViewContainer: ShimmerFrameLayout
    @BindView(R.id.mainLly)
    lateinit var mainlly: LinearLayout

    lateinit var home2ViewModel: Home2ViewModel
    var data_list = mutableListOf<AudioItem>()
    var vidLink:String=""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view:View=inflater.inflate(R.layout.fragment_home2, container, false)
        ButterKnife.bind(this,view)
        setHeadLayoutData()
        init()
        executeApi();
        return  view
    }

    //HEADER DATA
    private fun setHeadLayoutData() {
        txVideoHeadingMain.setText(getString(R.string.third_screen_heading))

    }

    //CLICK LISTENER
    @OnClick(R.id.vidThumbnail)
    fun onClick(view:View)
    {
        when(view.id)
        {
            R.id.vidThumbnail ->switchToVideoScreen()
        }
    }

    //switch to video screen
    private fun switchToVideoScreen() {
        if (!vidLink.equals(""))
        {
            val intent = Intent(context, VideoActivity::class.java)
            intent.putExtra("videoURL", vidLink)
            startActivity(intent)
        }
        else{
            showSNACK(vidThumbnail,getString(R.string.check_in_sometime))
        }
    }


    //REPOSIORY SETUP
    private fun init() {
        val repository = AppRepository()
        val factory = ViewModelProviderFactory(requireActivity().application, repository)
        home2ViewModel = ViewModelProvider(this, factory).get(Home2ViewModel::class.java)
    }


    //Execute api
    private fun executeApi() {
        txPlayTime.visibility=View.GONE
        var categoryid = "2"

        val body = RequestBodies.Home2Body(
            categoryid
        )
        home2ViewModel.homeGetData(getAUthToken(),body,context as Activity)
        home2ViewModel.homeResponse.observe(viewLifecycleOwner, Observer { event ->
            event.getContentIfNotHandled()?.let { response ->
                when (response) {
                    is Resource.Success -> {
                        Constants.dismissProgressDialog()
                        response.data?.let { homeResponse ->
                            if (homeResponse.status==1||homeResponse.status==200) {
                                setAdapter(homeResponse)
                            }
                            else if (homeResponse.status == 401)
                            {
                                Constants.showAuthDismissDialog(context as Activity?,getString(R.string.authorization_failure))
                            }
                            else{
                                homeResponse.message?.let { showSNACK(txVideoHeadingMain, it) }
                            }
                        }
                    }

                    is Resource.Error -> {
                        response.message?.let { message ->
                            showSNACK(txVideoHeadingMain,message)
                        }
                    }

                    is Resource.Loading -> {
                        mShimmerViewContainer!!.visibility=View.VISIBLE
                    }
                }
            }
        })
    }

    //ADAPTER SET
    private fun setAdapter(homeResponse: VideoAudioModel) {
        val options: RequestOptions = RequestOptions()
            .placeholder(R.drawable.ic_video_thumb)
            .error(R.drawable.ic_video_thumb)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
            .dontAnimate()
            .dontTransform()
        Glide
            .with(this)
            .load(homeResponse.video!!.videoThumbnail)
            .apply(options)
            .into(vidThumbnail)
        vidLink=homeResponse.video!!.categoryVideo
        txVideoHeading.text=homeResponse.video!!.videoTitle
        txPlayTime.text=homeResponse.video!!.endTime
        txPlayTime.visibility=View.VISIBLE
        data_list.clear()
        for (item in homeResponse.audio!!)
        {
            item?.let { data_list.add(it) }
        }
        recyclerHome2.layoutManager = LinearLayoutManager(context)
        val adapter = Home2Adapter(context,data_list,mShimmerViewContainer,mainlly)
        recyclerHome2.adapter = adapter
        perform(vidLink)
    }



    override fun onPause() {
        super.onPause()
        //  mShimmerViewContainer!!.visibility=View.GONE
    }

    private fun perform(vidLink: String) {
        val player: SimpleExoPlayer
        @DefaultRenderersFactory.ExtensionRendererMode val extensionRendererMode =
            DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
        val loadControl = DefaultLoadControl.Builder()
            .setBufferDurationsMs(25000, 50000, 1500, 2000).createDefaultLoadControl()
        val renderersFactory =
            context?.let { DefaultRenderersFactory(it).setExtensionRendererMode(extensionRendererMode) }
        player =
            context?.let { renderersFactory?.let { it1 -> SimpleExoPlayer.Builder(it, it1).setLoadControl(loadControl).build() } }!!
        val dataSourceFactory = DefaultDataSourceFactory(
            context,
            Util.getUserAgent(requireContext(), "ICMA")
        )
        val cacheDataSourceFactory =
            CacheDataSourceFactory(DownloadUtil.getCache(requireContext()), dataSourceFactory)
        val videoSource: MediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory)
            .createMediaSource(Uri.parse(vidLink))

        player.prepare(videoSource,true,true)
        player.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    Player.STATE_READY -> player.release()
                    else -> {
                    }
                }
            }
        })

    }

}