package com.icma.app.app

import android.app.Notification
import android.content.Context
import com.google.android.exoplayer2.offline.Download
import com.google.android.exoplayer2.offline.DownloadManager
import com.google.android.exoplayer2.offline.DownloadService
import com.google.android.exoplayer2.scheduler.Scheduler
import com.google.android.exoplayer2.ui.DownloadNotificationHelper
import com.icma.app.R
import java.lang.Exception

class MyDownloadService : DownloadService(1, DEFAULT_FOREGROUND_NOTIFICATION_UPDATE_INTERVAL, "my app", R.string.app_name, R.string.app_name){

    private lateinit var notificationHelper: DownloadNotificationHelper
    private lateinit var context: Context

    override fun onCreate() {
        super.onCreate()
        context = this
        notificationHelper= DownloadNotificationHelper(this, "my app")

    }
    override fun getDownloadManager(): DownloadManager {
        val manager = (application as MyApplication).appContainer.downloadManager
        //Set the maximum number of parallel downloads
        manager.maxParallelDownloads = 5
        manager.addListener(object : DownloadManager.Listener {
            override fun onDownloadRemoved(downloadManager: DownloadManager, download: Download) {
                // Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show()
            }

            override fun onDownloadsPausedChanged(downloadManager: DownloadManager, downloadsPaused: Boolean) {
                if (downloadsPaused){
                   // Toast.makeText(this@MyDownloadService, "paused", Toast.LENGTH_SHORT).show()
                } else{
                 //   Toast.makeText(this@MyDownloadService, "resumed", Toast.LENGTH_SHORT).show()
                }

            }

            override fun onDownloadChanged(downloadManager: DownloadManager, download: Download) {

            }

        })
        // return (application as App).appContainer.downloadManager
        return manager
    }



    //If you want to restart the download when it failed, you the can override this method, it uses Jobscheduler.
    override fun getScheduler(): Scheduler? {
        return null
    }

    override fun getForegroundNotification(downloads: MutableList<Download>): Notification {
        return notificationHelper.buildProgressNotification(R.drawable.app_logo,null, null, downloads)
    }


}