package com.icma.app.app

import kotlin.coroutines.CoroutineContext

interface CoroutineDispatchers {
    val main: CoroutineContext
    val background: CoroutineContext
}