package com.icma.app.app

import android.content.Context
import com.google.android.exoplayer2.database.DatabaseProvider
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.offline.DownloadManager
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.Cache
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.google.android.exoplayer2.util.Util
import com.icma.app.R
import java.io.File


class AppContainer(val context: Context) {

    private var dataSource = DefaultHttpDataSourceFactory(
        Util.getUserAgent(context, context.resources.getString(R.string.app_name)),null  /*listener*/ ,30 * 1000,30 * 1000, true )

    private  var dataBase: DatabaseProvider
    private var downloadContentDirectory: File
    var downloadCache: Cache
    var downloadManager: DownloadManager

    init {
        dataBase = ExoDatabaseProvider(context)
        downloadContentDirectory = File(context.getExternalFilesDir(null), "my app")
        downloadCache = SimpleCache(downloadContentDirectory, NoOpCacheEvictor(), dataBase)
        downloadManager = DownloadManager(context, dataBase, downloadCache, dataSource)
    }

}
